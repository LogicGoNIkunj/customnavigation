package customnotification.quick.controlsettings;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.util.Base64;


import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import customnotification.quick.controlsettings.model.ApiInterface;
import customnotification.quick.controlsettings.model.Model_Playdata;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LauncherApp extends Application {
    static LauncherApp mInstance;
    final SharedPreferences.OnSharedPreferenceChangeListener listener = (sharedPreferences, str) -> {
        if (str.equalsIgnoreCase("dataWarning")) {
            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("dataWarning", "");
        }
    };
    SharedPreferences preferences;
    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;
    public static AppOpenManager appOpenManager;
    public static Retrofit retrofit;
    public static Context context;
    public static String qurekaimage = "", querekatext = "", url = "";
    public static boolean checkqureka = false;
    public static String MYSECRET;

    public static synchronized LauncherApp getInstance() {
        LauncherApp launcherApp;
        synchronized (LauncherApp.class) {
            launcherApp = mInstance;
        }
        return launcherApp;
    }

    public void onCreate() {
        super.onCreate();
        mInstance = this;
        context = this;
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences.registerOnSharedPreferenceChangeListener(listener);
        sharedPreferences = getSharedPreferences("ps", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        appOpenManager = new AppOpenManager(this);
        GetData();


        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "customnotification.quick.controlsettings", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String key = Base64.encodeToString(md.digest(), Base64.NO_WRAP);
                key = key.replaceAll("[^a-zA-Z0-9]", "");
//                MYSECRET = key;
                MYSECRET = "abcdefg";
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ignored) {

        }
    }

    public void unregisterListener() {
        if (preferences != null) {
            preferences.unregisterOnSharedPreferenceChangeListener(listener);
        }
    }

    //    MainService ONOFF
    public static void set_ServiceONOFF(boolean ServiceONOFF) {
        editor.putBoolean("ServiceONOFF", ServiceONOFF).commit();
    }

    public static boolean get_ServiceONOFF() {
        return sharedPreferences.getBoolean("ServiceONOFF", false);
    }

    //first time
    public static void set_firsthowto(boolean firsthowto) {
        editor.putBoolean("firsthowto", firsthowto).commit();
    }

    public static boolean get_firsthowto() {
        return sharedPreferences.getBoolean("firsthowto", true);
    }


    //    Timer with second
    public static void set_ClockSecondshow(boolean ClockSecondshow) {
        editor.putBoolean("ClockSecondshow", ClockSecondshow).commit();
    }

    public static boolean get_ClockSecondshow() {
        return sharedPreferences.getBoolean("ClockSecondshow", false);
    }

    //    brightness hader slider
    public static void set_brigtnesshader(boolean brigtnesshader) {
        editor.putBoolean("brigtnesshader", brigtnesshader).commit();
    }

    public static boolean get_brigtnesshader() {
        return sharedPreferences.getBoolean("brigtnesshader", false);
    }

    //    topviewcorner
    public static void set_SmallCorner(boolean SmallCorner) {
        editor.putBoolean("SmallCorner", SmallCorner).commit();
    }

    public static boolean get_SmallCorner() {
        return sharedPreferences.getBoolean("SmallCorner", false);
    }


    //BigClockSize
    public static void set_Clocksize(int Clocksize) {
        editor.putInt("Clocksize", Clocksize).commit();
    }

    public static int get_Clocksize() {
        return sharedPreferences.getInt("Clocksize", 45);
    }

    //    powermenushownot
    public static void set_powerbtn(boolean powerbtn) {
        editor.putBoolean("powerbtn", powerbtn).commit();
    }

    public static boolean get_powerbtn() {
        return sharedPreferences.getBoolean("powerbtn", false);
    }

    //iconshape
    public static void set_qsIconShapeName(String qsIconShapeName) {
        editor.putString("qsIconShapeName", qsIconShapeName).commit();
    }

    public static String get_qsIconShapeName() {
        return sharedPreferences.getString("qsIconShapeName", "Circle");
    }

    public static void set_IconShapethumb(int IconShapethumb) {
        editor.putInt("IconShapethumb", IconShapethumb).commit();
    }

    public static int get_IconShapethumb() {
        return sharedPreferences.getInt("IconShapethumb", R.drawable.ic_qs_shape_circle);
    }

    //Gridtiles number
    public static void set_GridTiles(String GridTiles) {
        editor.putString("GridTiles", GridTiles).commit();
    }

    public static String get_GridTiles() {
        return sharedPreferences.getString("GridTiles", "4 * 4");
    }

    //headertiles number
    public static void set_headertiles(int headertiles) {
        editor.putInt("headertiles", headertiles).commit();
    }

    public static int get_headertiles() {
        return sharedPreferences.getInt("headertiles", 6);
    }

//    Color Modual

    //    bgcolor
    public static void set_MainBgColor(int MainBgColor) {
        editor.putInt("MainBgColor", MainBgColor).commit();
    }

    public static int get_MainBgColor() {
        return sharedPreferences.getInt("MainBgColor", Color.parseColor("#f0f2f5"));
    }

    //    bg_alpha
    public static void set_MainBgalpha(int MainBgalpha) {
        editor.putInt("MainBgalpha", MainBgalpha).commit();
    }

    public static int get_MainBgalpha() {
        return sharedPreferences.getInt("MainBgalpha", 255);
    }

    //    textcolor
    public static void set_TxtColor(int TxtColor) {
        editor.putInt("TxtColor", TxtColor).commit();
    }

    public static int get_TxtColor() {
        return sharedPreferences.getInt("TxtColor", Color.parseColor("#000000"));
    }

    //    textcoloralpha
    public static void set_TxtColoralpha(int TxtColoralpha) {
        editor.putInt("TxtColoralpha", TxtColoralpha).commit();
    }

    public static int get_TxtColoralpha() {
        return sharedPreferences.getInt("TxtColoralpha", 255);
    }

    //    ActiveTilecolor
    public static void set_ActiveTileColor(int ActiveTileColor) {
        editor.putInt("ActiveTileColor", ActiveTileColor).commit();
    }

    public static int get_ActiveTileColor() {
        return sharedPreferences.getInt("ActiveTileColor", Color.parseColor("#8B7BFF"));
    }

    //    ActiveTilealpha
    public static void set_ActiveTilealpha(int ActiveTilealpha) {
        editor.putInt("ActiveTilealpha", ActiveTilealpha).commit();
    }

    public static int get_ActiveTilealpha() {
        return sharedPreferences.getInt("ActiveTilealpha", 255);
    }

    //    brightnessslidercolor
    public static void set_brightnesscolor(int brightnesscolor) {
        editor.putInt("brightnesscolor", brightnesscolor).commit();
    }

    public static int get_brightnesscolor() {
        return sharedPreferences.getInt("brightnesscolor", Color.parseColor("#8B7BFF"));
    }

    //    brightnessslidercoloralpha
    public static void set_brightcoloralpha(int brightcoloralpha) {
        editor.putInt("brightcoloralpha", brightcoloralpha).commit();
    }

    public static int get_brightcoloralpha() {
        return sharedPreferences.getInt("brightcoloralpha", 255);
    }


    public static int getbtmBarColorAlpha() {
        int barColor = LauncherApp.get_btmtrigerColor();
        return Color.argb(LauncherApp.get_BarAlpha(), Color.red(barColor), Color.green(barColor), Color.blue(barColor));
    }

    public static int getLeftBarColorAlpha() {
        int barColor = LauncherApp.get_LefttrigerColor();
        return Color.argb(LauncherApp.get_LeftBarAlpha(), Color.red(barColor), Color.green(barColor), Color.blue(barColor));
    }

    public static int getRightBarColorAlpha() {
        int barColor = LauncherApp.get_RighttrigerColor();
        return Color.argb(LauncherApp.get_RightBarAlpha(), Color.red(barColor), Color.green(barColor), Color.blue(barColor));
    }
    //    triger

    //    Vibrate
    public static void set_Vibrateonoff(boolean Vibrateonoff) {
        editor.putBoolean("Vibrateonoff", Vibrateonoff).commit();
    }

    public static boolean get_Vibrateonoff() {
        return sharedPreferences.getBoolean("Vibrateonoff", false);
    }


    //    Edgetrigercolor
    public static void set_TrigerVal(int TrigerVal) {
        editor.putInt("TrigerVal", TrigerVal).commit();
    }

    public static int get_TrigerVal() {
        return sharedPreferences.getInt("TrigerVal", 0);
    }


    //    btmServicebool
    public static void set_btmService(boolean btmService) {
        editor.putBoolean("btmService", btmService).commit();
    }

    public static boolean get_btmService() {
        return sharedPreferences.getBoolean("btmService", false);
    }

    //    Edgetrigercolor
    public static void set_btmtrigerColor(int btmtrigerColor) {
        editor.putInt("btmtrigerColor", btmtrigerColor).commit();
    }

    public static int get_btmtrigerColor() {
        return sharedPreferences.getInt("btmtrigerColor", Color.parseColor("#FF1100"));
    }

    //btmalphaa
    public static void set_BarAlpha(int BarAlpha) {
        editor.putInt("BarAlpha", BarAlpha).commit();
    }

    public static int get_BarAlpha() {
        return sharedPreferences.getInt("BarAlpha", 188);
    }

//    btmbarwith

    public static int get_BarWith() {
        return sharedPreferences.getInt("BarWith", 200);
    }

    //btmbarheight
    public static void set_BarHeight(int BarHeight) {
        editor.putInt("BarHeight", BarHeight).commit();
    }

    public static int get_BarHeight() {
        return sharedPreferences.getInt("BarHeight", 25);
    }

    //btmbarposi
    public static void set_BarPosition(int BarPosition) {
        editor.putInt("BarPosition", BarPosition).commit();
    }

    public static int get_BarPosition() {
        return sharedPreferences.getInt("BarPosition", 720);
    }

    //btmbarposi
    public static void set_BarMinvat(int BarMinvat) {
        editor.putInt("BarMinvat", BarMinvat).commit();
    }

    public static int get_BarMinvat() {
        return sharedPreferences.getInt("BarMinvat", Resources.getSystem().getDisplayMetrics().widthPixels);
    }


//    LeftTriger


    //    LeftServicebool
    public static void set_LeftService(boolean LeftService) {
        editor.putBoolean("LeftService", LeftService).commit();
    }

    public static boolean get_LeftService() {
        return sharedPreferences.getBoolean("LeftService", false);
    }


    //    LeftEdgetrigercolor
    public static void set_LefttrigerColor(int LefttrigerColor) {
        editor.putInt("LefttrigerColor", LefttrigerColor).commit();
    }

    public static int get_LefttrigerColor() {
        return sharedPreferences.getInt("LefttrigerColor", Color.parseColor("#FF1100"));
    }

    //Leftalphaa
    public static void set_LeftBarAlpha(int LeftBarAlpha) {
        editor.putInt("LeftBarAlpha", LeftBarAlpha).commit();
    }

    public static int get_LeftBarAlpha() {
        return sharedPreferences.getInt("LeftBarAlpha", 187);
    }

    //Leftbarwith
    public static void set_LeftBarWith(int LeftBarWith) {
        editor.putInt("LeftBarWith", LeftBarWith).commit();
    }

    public static int get_LeftBarWith() {
        return sharedPreferences.getInt("LeftBarWith", 20);
    }

    //Leftbarposi
    public static void set_LeftBarPosition(int LeftBarPosition) {
        editor.putInt("LeftBarPosition", LeftBarPosition).commit();
    }

    public static int get_LeftBarPosition() {
        return sharedPreferences.getInt("LeftBarPosition", -79);
    }


//    RightTriger


    //    RightServicebool
    public static void set_RightService(boolean RightService) {
        editor.putBoolean("RightService", RightService).commit();
    }

    public static boolean get_RightService() {
        return sharedPreferences.getBoolean("RightService", false);
    }


    //    RightEdgetrigercolor
    public static void set_RighttrigerColor(int RighttrigerColor) {
        editor.putInt("RighttrigerColor", RighttrigerColor).commit();
    }

    public static int get_RighttrigerColor() {
        return sharedPreferences.getInt("RighttrigerColor", Color.parseColor("#FF1100"));
    }

    //Rightalphaa
    public static void set_RightBarAlpha(int RightBarAlpha) {
        editor.putInt("RightBarAlpha", RightBarAlpha).commit();
    }

    public static int get_RightBarAlpha() {
        return sharedPreferences.getInt("RightBarAlpha", 141);
    }

    //Rightbarwith
    public static void set_RightBarWith(int RightBarWith) {
        editor.putInt("RightBarWith", RightBarWith).commit();
    }

    public static int get_RightBarWith() {
        return sharedPreferences.getInt("RightBarWith", 20);
    }

    //Rightbarheight

    //Rightbarposi
    public static void set_RightBarPosition(int RightBarPosition) {
        editor.putInt("RightBarPosition", RightBarPosition).commit();
    }

    public static int get_RightBarPosition() {
        return sharedPreferences.getInt("RightBarPosition", -145);
    }


    //    ads
    public static void set_AdsInt(int adsInt) {
        editor.putInt("adsInt", adsInt).commit();
    }

    public static int get_AdsInt() {
        return sharedPreferences.getInt("adsInt", 3);
    }

    //admob banner
    public static void set_Admob_banner_Id(String Admob_banner_Id) {
        editor.putString("Admob_banner_Id", Admob_banner_Id).commit();
    }

    public static String get_Admob_banner_Id() {
        return sharedPreferences.getString("Admob_banner_Id", "ca-app-pub-3940256099942544/6300978111");
    }

    //admob interstitial
    public static void set_Admob_interstitial_Id(String Admob_interstitial_Id) {
        editor.putString("Admob_interstitial_Id", Admob_interstitial_Id).commit();
    }

    public static String get_Admob_interstitial_Id() {
        return sharedPreferences.getString("Admob_interstitial_Id", "ca-app-pub-3940256099942544/1033173712");
    }

    //admob native
    public static void set_Admob_native_Id(String Admob_native_Id) {
        editor.putString("Admob_native_Id", Admob_native_Id).commit();
    }

    public static String get_Admob_native_Id() {
        return sharedPreferences.getString("Admob_native_Id", "ca-app-pub-3940256099942544/2247696110");
    }

    //admob open
    public static void set_Admob_open_Id(String Admob_open_Id) {
        editor.putString("Admob_open_Id", Admob_open_Id).commit();
    }

    public static String get_Admob_open_Id() {
        return sharedPreferences.getString("Admob_open_Id", "");
    }

    public static void set_Admob_openshowtime(int Admob_openshowtime) {
        editor.putInt("Admob_openshowtime", Admob_openshowtime).commit();
    }

    public static int get_Admob_openshowtime() {
        return sharedPreferences.getInt("Admob_openshowtime", 0);
    }


    //panelheightval
    public static void set_Panelheight(boolean Panelheight) {
        editor.putBoolean("Panelheight", Panelheight).commit();
    }

    public static boolean get_Panelheight() {
        return sharedPreferences.getBoolean("Panelheight", false);
    }


    //    firstview
//panelheightval
    public static void set_OpenFirst(boolean OpenFirst) {
        editor.putBoolean("OpenFirst", OpenFirst).commit();
    }

    public static boolean get_OpenFirst() {
        return sharedPreferences.getBoolean("OpenFirst", false);
    }


    //panelheightval
    public static void set_BigFramehei(int BigFramehei) {
        editor.putInt("BigFramehei", BigFramehei).commit();
    }

    public static int get_BigFramehei() {
        return sharedPreferences.getInt("BigFramehei", 0);
    }


    //theme int
    public static void set_theme(int theme) {
        editor.putInt("theme", theme).commit();
    }

    public static int get_theme() {
        return sharedPreferences.getInt("theme", 9);
    }


    //qureka
    public void GetData() {
        try {
            if (isNetworkAvailable(context)) {
                if (retrofit == null) {
                    retrofit = new Retrofit.Builder().baseUrl(ApiInterface.GameAPI).client(new OkHttpClient.Builder().addInterceptor(chain -> chain.proceed(chain.request().newBuilder().build())).connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build()).addConverterFactory(GsonConverterFactory.create()).build();
                }
                retrofit.create(ApiInterface.class).GetData().enqueue(new Callback<Model_Playdata>() {
                    @Override
                    public void onResponse(@NotNull Call<Model_Playdata> call, @NotNull retrofit2.Response<Model_Playdata> response) {
                        try {
                            if (response.body() != null) {
                                if (response.body().isStatus()) {
                                    if (response.body().getData() != null) {
                                        checkqureka = response.body().getData().isFlage();
                                        if (response.body().getData().isFlage()) {
                                            qurekaimage = response.body().getData().getImage();
                                            querekatext = response.body().getData().getTitle();
                                            url = response.body().getData().getUrl();
                                        }
                                    }
                                }
                            }

                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Model_Playdata> call, @NonNull Throwable t) {
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}