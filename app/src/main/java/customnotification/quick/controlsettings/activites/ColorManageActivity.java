package customnotification.quick.controlsettings.activites;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.material.appbar.AppBarLayout;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.services.MAccessibilityService;

public class ColorManageActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colormanager);
        ThemeChange();
        Clicks();
        findViewById(R.id.btn_customcolors).setOnClickListener(view -> startActivity(new Intent(ColorManageActivity.this, CustomColorActivity.class)));
    }

    public void ThemeChange() {
        findViewById(R.id.t1).setVisibility(View.GONE);
        findViewById(R.id.t2).setVisibility(View.GONE);
        findViewById(R.id.t3).setVisibility(View.GONE);
        findViewById(R.id.t4).setVisibility(View.GONE);
        findViewById(R.id.t5).setVisibility(View.GONE);
        findViewById(R.id.t6).setVisibility(View.GONE);
        findViewById(R.id.t7).setVisibility(View.GONE);
        findViewById(R.id.t8).setVisibility(View.GONE);
        findViewById(R.id.t9).setVisibility(View.GONE);
        findViewById(R.id.t10).setVisibility(View.GONE);
        findViewById(R.id.t11).setVisibility(View.GONE);
        findViewById(R.id.t12).setVisibility(View.GONE);
        findViewById(R.id.t13).setVisibility(View.GONE);
        findViewById(R.id.t14).setVisibility(View.GONE);
        findViewById(R.id.t15).setVisibility(View.GONE);
        findViewById(R.id.t16).setVisibility(View.GONE);
        if (LauncherApp.get_theme() == 1) {
            findViewById(R.id.t1).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 2) {
            findViewById(R.id.t2).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 3) {
            findViewById(R.id.t3).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 4) {
            findViewById(R.id.t4).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 5) {
            findViewById(R.id.t5).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 6) {
            findViewById(R.id.t6).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 7) {
            findViewById(R.id.t7).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 8) {
            findViewById(R.id.t8).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 9) {
            findViewById(R.id.t9).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 10) {
            findViewById(R.id.t10).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 11) {
            findViewById(R.id.t11).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 12) {
            findViewById(R.id.t12).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 13) {
            findViewById(R.id.t13).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 14) {
            findViewById(R.id.t14).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 15) {
            findViewById(R.id.t15).setVisibility(View.VISIBLE);
        } else if (LauncherApp.get_theme() == 16) {
            findViewById(R.id.t16).setVisibility(View.VISIBLE);
        }
    }

    public void Clicks() {
        ((AppBarLayout) findViewById(R.id.app_bar)).addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            ((TextView) findViewById(R.id.txt_tit)).setTextColor(Color.argb(Math.abs(verticalOffset / 2), 0, 0, 0));
            ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(verticalOffset / 2, 0, 0, 0));
            ((ImageView) findViewById(R.id.img_top)).setAlpha(verticalOffset / 2);
            if (Math.abs(verticalOffset) <= 1) {
                ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(255, 0, 0, 0));
                ((ImageView) findViewById(R.id.img_top)).setAlpha(255);
            }
        });
        findViewById(R.id.btn_back).setOnClickListener(view -> onBackPressed());
        findViewById(R.id.btn_theme1).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t1));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t1));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t1));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t1));
            LauncherApp.set_qsIconShapeName("Rectangle");
            LauncherApp.set_IconShapethumb(R.drawable.ic_qs_shape_rect);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(1);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme2).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t2));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t2));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t2));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t2));
            LauncherApp.set_qsIconShapeName("Pentagon");
            LauncherApp.set_IconShapethumb(R.drawable.ic_qs_shape_pentagon);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(2);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme3).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t3));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t3));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t3));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t3));
            LauncherApp.set_qsIconShapeName("Circle");
            LauncherApp.set_IconShapethumb(R.drawable.default_blue);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(3);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme4).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t4));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t4));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t4));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t4));
            LauncherApp.set_qsIconShapeName("Hexagon");
            LauncherApp.set_IconShapethumb(R.drawable.ic_qs_shape_hexagon);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(4);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme5).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t5));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t5));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t5));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t5));
            LauncherApp.set_qsIconShapeName("Star");
            LauncherApp.set_IconShapethumb(R.drawable.ic_qs_shape_star);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(5);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme6).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t6));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t6));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t6));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t6));
            LauncherApp.set_qsIconShapeName("Diamond");
            LauncherApp.set_IconShapethumb(R.drawable.ic_qs_shape_diamond);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(6);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme7).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t7));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t7));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t7));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t7));
            LauncherApp.set_qsIconShapeName("Circle");
            LauncherApp.set_IconShapethumb(R.drawable.default_blue);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(7);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme8).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t8));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t8));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t8));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t8));
            LauncherApp.set_qsIconShapeName("Circle");
            LauncherApp.set_IconShapethumb(R.drawable.default_blue);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(8);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme9).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t9));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t9));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t9));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t9));
            LauncherApp.set_qsIconShapeName("Circle");
            LauncherApp.set_IconShapethumb(R.drawable.default_blue);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(9);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme10).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t10));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t10));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t10));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t10));
            LauncherApp.set_qsIconShapeName("Circle");
            LauncherApp.set_IconShapethumb(R.drawable.default_blue);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(10);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme11).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t11));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t11));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t11));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t11));
            LauncherApp.set_qsIconShapeName("Squircle");
            LauncherApp.set_IconShapethumb(R.drawable.ic_qs_shape_squircle);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(11);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme12).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t12));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t12));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t12));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t12));
            LauncherApp.set_qsIconShapeName("Circle");
            LauncherApp.set_IconShapethumb(R.drawable.default_blue);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(12);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme13).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t13));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t13));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t13));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t13));
            LauncherApp.set_qsIconShapeName("Sun");
            LauncherApp.set_IconShapethumb(R.drawable.ic_qs_shape_sun);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(13);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme14).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t14));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t14));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t14));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t14));
            LauncherApp.set_qsIconShapeName("Circle");
            LauncherApp.set_IconShapethumb(R.drawable.default_blue);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(14);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme15).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t15));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t15));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t15));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t15));
            LauncherApp.set_qsIconShapeName("Circle");
            LauncherApp.set_IconShapethumb(R.drawable.default_blue);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(15);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_theme16).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(getResources().getColor(R.color.bg_t16));
            LauncherApp.set_TxtColor(getResources().getColor(R.color.txt_t16));
            LauncherApp.set_ActiveTileColor(getResources().getColor(R.color.Active_tile_t16));
            LauncherApp.set_brightnesscolor(getResources().getColor(R.color.brightness_t16));
            LauncherApp.set_qsIconShapeName("Teardrop");
            LauncherApp.set_IconShapethumb(R.drawable.ic_qs_shape_teardrop);
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            LauncherApp.set_theme(16);
            ThemeChange();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(ColorManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
    }
}