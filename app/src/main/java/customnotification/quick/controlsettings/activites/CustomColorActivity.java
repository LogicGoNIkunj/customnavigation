package customnotification.quick.controlsettings.activites;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.google.android.material.appbar.AppBarLayout;
import com.rtugeek.android.colorseekbar.ColorSeekBar;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.services.MAccessibilityService;
import customnotification.quick.controlsettings.utils.Utils;

public class CustomColorActivity extends Activity {
    AppCompatImageView iv_bg_color, iv_activetile_color, iv_txt_color, iv_brigt_color;
    ColorSeekBar seek_bg_color, seek_actitile_color, seek_txt_color, seek_bright_color;
    SeekBar sb_bg_alpha, sb_actitile_alpha, sb_txt_alpha, sb_bright_alpha;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_color);
        Initmeth();
        Clicks();
    }

    public void Initmeth() {
        iv_bg_color = findViewById(R.id.iv_bg_color);
        iv_activetile_color = findViewById(R.id.iv_activetile_color);
        iv_txt_color = findViewById(R.id.iv_txt_color);
        iv_brigt_color = findViewById(R.id.iv_brigt_color);
        seek_bg_color = findViewById(R.id.seek_bg_color);
        seek_actitile_color = findViewById(R.id.seek_actitile_color);
        seek_txt_color = findViewById(R.id.seek_txt_color);
        seek_bright_color = findViewById(R.id.seek_bright_color);
        sb_bg_alpha = findViewById(R.id.sb_bg_alpha);
        sb_actitile_alpha = findViewById(R.id.sb_actitile_alpha);
        sb_txt_alpha = findViewById(R.id.sb_txt_alpha);
        sb_bright_alpha = findViewById(R.id.sb_bright_alpha);
        sb_bg_alpha.setMax(255);
        sb_actitile_alpha.setMax(255);
        sb_txt_alpha.setMax(255);
        sb_bright_alpha.setMax(255);
        sb_bg_alpha.setProgress(LauncherApp.get_MainBgalpha());
        sb_actitile_alpha.setProgress(LauncherApp.get_ActiveTilealpha());
        sb_txt_alpha.setProgress(LauncherApp.get_TxtColoralpha());
        sb_bright_alpha.setProgress(LauncherApp.get_brightcoloralpha());
        seek_bg_color.setColor(LauncherApp.get_MainBgColor());
        seek_actitile_color.setColor(LauncherApp.get_ActiveTileColor());
        seek_txt_color.setColor(LauncherApp.get_TxtColor());
        seek_bright_color.setColor(LauncherApp.get_brightnesscolor());
        iv_bg_color.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_MainBgColor(), LauncherApp.get_MainBgalpha()));
        iv_activetile_color.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_ActiveTileColor(), LauncherApp.get_ActiveTilealpha()));
        iv_txt_color.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_TxtColor(), LauncherApp.get_TxtColoralpha()));
        iv_brigt_color.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_brightnesscolor(), LauncherApp.get_brightcoloralpha()));
    }

    public void Clicks() {
        ((AppBarLayout) findViewById(R.id.app_bar)).addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            ((TextView) findViewById(R.id.txt_tit)).setTextColor(Color.argb(Math.abs(verticalOffset / 2), 0, 0, 0));
            ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(verticalOffset / 2, 0, 0, 0));
            ((ImageView) findViewById(R.id.img_top)).setAlpha(verticalOffset / 2);
            if (Math.abs(verticalOffset) <= 1) {
                ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(255, 0, 0, 0));
                ((ImageView) findViewById(R.id.img_top)).setAlpha(255);
            }
        });
        findViewById(R.id.btn_back).setOnClickListener(view -> onBackPressed());
        seek_bg_color.setOnColorChangeListener((progress, color) -> {
            LauncherApp.set_MainBgColor(color);
            iv_bg_color.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_MainBgColor(), LauncherApp.get_MainBgalpha()));
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(CustomColorActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        sb_bg_alpha.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (i <= 100) {
                    sb_bg_alpha.setProgress(100);
                } else {
                    LauncherApp.set_MainBgalpha(seekBar.getProgress());
                    iv_bg_color.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_MainBgColor(), LauncherApp.get_MainBgalpha()));
                    if (LauncherApp.get_ServiceONOFF()) {
                        Intent intent = new Intent(CustomColorActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            startForegroundService(intent);
                        } else {
                            startService(intent);
                        }
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        seek_actitile_color.setOnColorChangeListener((progress, color) -> {
            LauncherApp.set_ActiveTileColor(color);
            iv_activetile_color.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_ActiveTileColor(), LauncherApp.get_ActiveTilealpha()));
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(CustomColorActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        sb_actitile_alpha.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (i <= 100) {
                    sb_actitile_alpha.setProgress(100);
                } else {
                    LauncherApp.set_ActiveTilealpha(seekBar.getProgress());
                    iv_activetile_color.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_ActiveTileColor(), LauncherApp.get_ActiveTilealpha()));
                    if (LauncherApp.get_ServiceONOFF()) {
                        Intent intent = new Intent(CustomColorActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            startForegroundService(intent);
                        } else {
                            startService(intent);
                        }
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        seek_txt_color.setOnColorChangeListener((progress, color) -> {
            LauncherApp.set_TxtColor(color);
            iv_txt_color.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_TxtColor(), LauncherApp.get_TxtColoralpha()));
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(CustomColorActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        sb_txt_alpha.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (i <= 100) {
                    sb_txt_alpha.setProgress(100);
                } else {
                    LauncherApp.set_TxtColoralpha(seekBar.getProgress());
                    iv_txt_color.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_TxtColor(), LauncherApp.get_TxtColoralpha()));
                    if (LauncherApp.get_ServiceONOFF()) {
                        Intent intent = new Intent(CustomColorActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            startForegroundService(intent);
                        } else {
                            startService(intent);
                        }
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        seek_bright_color.setOnColorChangeListener((progress, color) -> {
            LauncherApp.set_brightnesscolor(color);
            iv_brigt_color.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_brightnesscolor(), LauncherApp.get_brightcoloralpha()));
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(CustomColorActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        sb_bright_alpha.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (i <= 100) {
                    sb_bright_alpha.setProgress(100);
                } else {
                    LauncherApp.set_brightcoloralpha(seekBar.getProgress());
                    iv_brigt_color.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_brightnesscolor(), LauncherApp.get_brightcoloralpha()));
                    if (LauncherApp.get_ServiceONOFF()) {
                        Intent intent = new Intent(CustomColorActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            startForegroundService(intent);
                        } else {
                            startService(intent);
                        }
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        findViewById(R.id.btn_resetall).setOnClickListener(view -> {
            LauncherApp.set_MainBgColor(Color.parseColor("#f0f2f5"));
            LauncherApp.set_TxtColor(Color.parseColor("#000000"));
            LauncherApp.set_ActiveTileColor(Color.parseColor("#8B7BFF"));
            LauncherApp.set_brightnesscolor(Color.parseColor("#8B7BFF"));
            LauncherApp.set_MainBgalpha(255);
            LauncherApp.set_ActiveTilealpha(255);
            LauncherApp.set_TxtColoralpha(255);
            LauncherApp.set_brightcoloralpha(255);
            sb_bg_alpha.setProgress(LauncherApp.get_MainBgalpha());
            sb_actitile_alpha.setProgress(LauncherApp.get_ActiveTilealpha());
            sb_txt_alpha.setProgress(LauncherApp.get_TxtColoralpha());
            sb_bright_alpha.setProgress(LauncherApp.get_brightcoloralpha());
            iv_bg_color.getDrawable().setTint(LauncherApp.get_MainBgColor());
            iv_activetile_color.getDrawable().setTint(LauncherApp.get_ActiveTileColor());
            iv_brigt_color.getDrawable().setTint(LauncherApp.get_brightnesscolor());
            iv_txt_color.getDrawable().setTint(LauncherApp.get_TxtColor());
            seek_bg_color.setProgress(0);
            seek_actitile_color.setProgress(0);
            seek_txt_color.setProgress(0);
            seek_bright_color.setProgress(0);
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(CustomColorActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 4);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
    }
}