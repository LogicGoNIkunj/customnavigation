package customnotification.quick.controlsettings.activites;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.android.material.appbar.AppBarLayout;
import com.rtugeek.android.colorseekbar.ColorSeekBar;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.utils.DHANVINE_Utils;
import customnotification.quick.controlsettings.utils.D_PermiReqDialog;
import customnotification.quick.controlsettings.utils.D_PermiReqListener;

public class EDGETrigActivity extends Activity implements SeekBar.OnSeekBarChangeListener {
    ToggleButton switch_enable;
    ImageView iv_edgecolor;
    BroadcastReceiver mReceiver;
    static final String[] PERMISSIONS = {"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"};
    SeekBar sbAlpha, sbHeight, sbWidth, sb_position;
    private final int width = Resources.getSystem().getDisplayMetrics().widthPixels;
    private final int height = Resources.getSystem().getDisplayMetrics().heightPixels;
    ColorSeekBar seek_trigcolor;
    NativeAd nativeAds;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edgetrig);
        switch_enable = findViewById(R.id.switch_enable);
        iv_edgecolor = findViewById(R.id.iv_edgecolor);
        sbAlpha = findViewById(R.id.sb_alpha);
        sbWidth = findViewById(R.id.sb_width);
        sbHeight = findViewById(R.id.sb_height);
        sb_position = findViewById(R.id.sb_position);
        seek_trigcolor = findViewById(R.id.seek_trigcolor);
        sbAlpha.setOnSeekBarChangeListener(this);
        sbWidth.setOnSeekBarChangeListener(this);
        sbHeight.setOnSeekBarChangeListener(this);
        sb_position.setOnSeekBarChangeListener(this);
        ((ToggleButton) findViewById(R.id.switch_Vibrate)).setChecked(LauncherApp.get_Vibrateonoff());
        IntentFilter intentFilter = new IntentFilter("com.action.ACTION_ADMIN_DISABLED");
        intentFilter.addAction("com.action.ACTION_UPDATE_MAIN_ACTIVITY");
        intentFilter.addAction("com.action.ACTION_FINISH_APP");
        mReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                int hashCode = action.hashCode();
                if (hashCode == -1202526179) {
                    action.equals("com.action.ACTION_UPDATE_MAIN_ACTIVITY");
                } else if (hashCode == -1163623862) {
                    action.equals("com.action.ACTION_ADMIN_DISABLED");
                } else if (hashCode == -260093933 && action.equals("com.action.ACTION_FINISH_APP")) {
                    finish();
                }
            }
        };
        RefreshAd();
        registerReceiver(mReceiver, intentFilter);
        LauncherApp.set_TrigerVal(0);
        SetMethod();
        Clicks();
    }

    public void Clicks() {
        ((AppBarLayout) findViewById(R.id.app_bar)).addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            ((TextView) findViewById(R.id.txt_tit)).setTextColor(Color.argb(Math.abs(verticalOffset / 2), 0, 0, 0));
            ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(verticalOffset / 2, 0, 0, 0));
            ((ImageView) findViewById(R.id.img_top)).setAlpha(verticalOffset / 2);
            if (Math.abs(verticalOffset) <= 1) {
                ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(255, 0, 0, 0));
                ((ImageView) findViewById(R.id.img_top)).setAlpha(255);
            }
        });
        findViewById(R.id.btn_back).setOnClickListener(view -> onBackPressed());
        findViewById(R.id.btn_vibrate).setOnClickListener(view -> {
            if (LauncherApp.get_Vibrateonoff()) {
                LauncherApp.set_Vibrateonoff(false);
                ((ToggleButton) findViewById(R.id.switch_Vibrate)).setChecked(false);
            } else {
                LauncherApp.set_Vibrateonoff(true);
                ((ToggleButton) findViewById(R.id.switch_Vibrate)).setChecked(true);
            }
        });
        findViewById(R.id.btn_lefttrig).setOnClickListener(view -> {
            LauncherApp.set_TrigerVal(1);
            SetMethod();
        });
        findViewById(R.id.btn_bottomtrig).setOnClickListener(view -> {
            LauncherApp.set_TrigerVal(0);
            SetMethod();
        });
        findViewById(R.id.btn_righttrig).setOnClickListener(view -> {
            LauncherApp.set_TrigerVal(2);
            SetMethod();
        });
        findViewById(R.id.llswitch).setOnClickListener(view -> {
            if (switch_enable.isChecked()) {
                switch_enable.setChecked(false);
                sendBroadcast(new Intent("com.action.ACTION_HIDE_NAVIGATION_BAR"));
            } else {
                if (!Settings.canDrawOverlays(EDGETrigActivity.this)) {
                    startActivityForResult(new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION", Uri.parse("package:" + getPackageName())), 1);
                } else if (DHANVINE_Utils.isAccessibilityServiceEnabled(EDGETrigActivity.this)) {
                    if (LauncherApp.get_TrigerVal() == 0) {
                        if (!LauncherApp.get_btmService()) {
                            switch_enable.setChecked(true);
                            LauncherApp.set_btmService(true);
                        }
                    } else if (LauncherApp.get_TrigerVal() == 1) {
                        if (!LauncherApp.get_LeftService()) {
                            switch_enable.setChecked(true);
                            LauncherApp.set_LeftService(true);
                        }
                    } else if (LauncherApp.get_TrigerVal() == 2) {
                        if (!LauncherApp.get_RightService()) {
                            switch_enable.setChecked(true);
                            LauncherApp.set_RightService(true);
                        }
                    }
                    sendBroadcast(new Intent("com.action.ACTION_SHOW_NAVIGATION_BAR"));
                } else {
                    startActivityForResult(new Intent("android.settings.ACCESSIBILITY_SETTINGS"), 3);
                }
            }
        });
        seek_trigcolor.setOnColorChangeListener((progress, color) -> {
            if (LauncherApp.get_TrigerVal() == 0) {
                LauncherApp.set_btmtrigerColor(color);
                iv_edgecolor.getDrawable().setTint(LauncherApp.getbtmBarColorAlpha());
            } else if (LauncherApp.get_TrigerVal() == 1) {
                LauncherApp.set_LefttrigerColor(color);
                iv_edgecolor.getDrawable().setTint(LauncherApp.getLeftBarColorAlpha());
            } else if (LauncherApp.get_TrigerVal() == 2) {
                LauncherApp.set_RighttrigerColor(color);
                iv_edgecolor.getDrawable().setTint(LauncherApp.getRightBarColorAlpha());
            }
            updateNavigationBarColor();
        });
    }

    public void SetMethod() {
        declareUI();
        if (LauncherApp.get_TrigerVal() == 0) {
            iv_edgecolor.getDrawable().setTint(LauncherApp.getbtmBarColorAlpha());
            switch_enable.setChecked(LauncherApp.get_btmService());
            findViewById(R.id.btn_bottomtrig).setBackgroundColor(getResources().getColor(R.color.selected));
            ((TextView) findViewById(R.id.btn_bottomtrig)).setTextColor(getResources().getColor(R.color.white));
            ((TextView) findViewById(R.id.btn_bottomtrig)).getCompoundDrawables()[0].setTint(getResources().getColor(R.color.white));
            findViewById(R.id.btn_lefttrig).setBackgroundColor(getResources().getColor(R.color.simpbg));
            ((TextView) findViewById(R.id.btn_lefttrig)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) findViewById(R.id.btn_lefttrig)).getCompoundDrawables()[0].setTint(getResources().getColor(R.color.black));
            findViewById(R.id.btn_righttrig).setBackgroundColor(getResources().getColor(R.color.simpbg));
            ((TextView) findViewById(R.id.btn_righttrig)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) findViewById(R.id.btn_righttrig)).getCompoundDrawables()[0].setTint(getResources().getColor(R.color.black));
        } else if (LauncherApp.get_TrigerVal() == 1) {
            iv_edgecolor.getDrawable().setTint(LauncherApp.getLeftBarColorAlpha());
            switch_enable.setChecked(LauncherApp.get_LeftService());
            findViewById(R.id.btn_bottomtrig).setBackgroundColor(getResources().getColor(R.color.simpbg));
            ((TextView) findViewById(R.id.btn_bottomtrig)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) findViewById(R.id.btn_bottomtrig)).getCompoundDrawables()[0].setTint(getResources().getColor(R.color.black));
            findViewById(R.id.btn_lefttrig).setBackgroundColor(getResources().getColor(R.color.selected));
            ((TextView) findViewById(R.id.btn_lefttrig)).setTextColor(getResources().getColor(R.color.white));
            ((TextView) findViewById(R.id.btn_lefttrig)).getCompoundDrawables()[0].setTint(getResources().getColor(R.color.white));
            findViewById(R.id.btn_righttrig).setBackgroundColor(getResources().getColor(R.color.simpbg));
            ((TextView) findViewById(R.id.btn_righttrig)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) findViewById(R.id.btn_righttrig)).getCompoundDrawables()[0].setTint(getResources().getColor(R.color.black));
        } else if (LauncherApp.get_TrigerVal() == 2) {
            iv_edgecolor.getDrawable().setTint(LauncherApp.getRightBarColorAlpha());
            switch_enable.setChecked(LauncherApp.get_RightService());
            findViewById(R.id.btn_bottomtrig).setBackgroundColor(getResources().getColor(R.color.simpbg));
            ((TextView) findViewById(R.id.btn_bottomtrig)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) findViewById(R.id.btn_bottomtrig)).getCompoundDrawables()[0].setTint(getResources().getColor(R.color.black));
            findViewById(R.id.btn_lefttrig).setBackgroundColor(getResources().getColor(R.color.simpbg));
            ((TextView) findViewById(R.id.btn_lefttrig)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) findViewById(R.id.btn_lefttrig)).getCompoundDrawables()[0].setTint(getResources().getColor(R.color.black));
            findViewById(R.id.btn_righttrig).setBackgroundColor(getResources().getColor(R.color.selected));
            ((TextView) findViewById(R.id.btn_righttrig)).setTextColor(getResources().getColor(R.color.white));
            ((TextView) findViewById(R.id.btn_righttrig)).getCompoundDrawables()[0].setTint(getResources().getColor(R.color.white));
        }
    }

    public void updateNavigationBarColor() {
        sendBroadcast(new Intent("com.action.ACTION_UPDATE_NAVIGATION_COLOR"));
    }

    private void updateNavigationBarSize() {
        sendBroadcast(new Intent("com.action.ACTION_UPDATE_NAVIGATION_SIZE"));
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        if (LauncherApp.get_TrigerVal() == 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                sb_position.setMin(LauncherApp.get_BarMinvat());
            }
            if (z) {
                int id = seekBar.getId();
                if (id == R.id.sb_alpha) {
                    LauncherApp.set_BarAlpha(this.sbAlpha.getProgress());
                    iv_edgecolor.getDrawable().setTint(LauncherApp.getbtmBarColorAlpha());
                    updateNavigationBarColor();
                } else if (id == R.id.sb_height) {

                    LauncherApp.set_BarHeight(sbHeight.getProgress() + 25);
                    updateNavigationBarSize();
                } else if (id == R.id.sb_position) {
                    LauncherApp.set_BarPosition(sb_position.getProgress());
                    updateNavigationBarSize();
                }
            }
        } else if (LauncherApp.get_TrigerVal() == 1) {
            if (z) {
                int id = seekBar.getId();
                if (id == R.id.sb_alpha) {
                    LauncherApp.set_LeftBarAlpha(this.sbAlpha.getProgress());
                    iv_edgecolor.getDrawable().setTint(LauncherApp.getLeftBarColorAlpha());
                    updateNavigationBarColor();
                } else if (id == R.id.sb_width) {
                    LauncherApp.set_LeftBarWith(sbWidth.getProgress());
                    updateNavigationBarSize();
                } else if (id == R.id.sb_position) {
                    LauncherApp.set_LeftBarPosition(sb_position.getProgress() - 30);
                    updateNavigationBarSize();
                }
            }
        } else if (LauncherApp.get_TrigerVal() == 2) {
            if (z) {
                int id = seekBar.getId();
                if (id == R.id.sb_alpha) {
                    LauncherApp.set_RightBarAlpha(this.sbAlpha.getProgress());
                    iv_edgecolor.getDrawable().setTint(LauncherApp.getRightBarColorAlpha());
                    updateNavigationBarColor();
                } else if (id == R.id.sb_width) {
                    LauncherApp.set_RightBarWith(sbWidth.getProgress());
                    updateNavigationBarSize();
                } else if (id == R.id.sb_position) {
                    LauncherApp.set_RightBarPosition(sb_position.getProgress() - 30);
                    updateNavigationBarSize();
                }
            }
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    public void declareUI() {
        if (LauncherApp.get_TrigerVal() == 0) {
            findViewById(R.id.ll_width).setVisibility(View.GONE);
            findViewById(R.id.ll_height).setVisibility(View.VISIBLE);
            sbAlpha.setMax(255);
            sbAlpha.setProgress(LauncherApp.get_BarAlpha());
            sbHeight.setMax(20);
            sbHeight.setProgress(LauncherApp.get_BarHeight() - 25);
            int maxwith = width + width;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                sb_position.setMin(LauncherApp.get_BarMinvat());
            }
            sb_position.setMax(maxwith - LauncherApp.get_BarWith());
            sb_position.setProgress(LauncherApp.get_BarPosition());
            updateNavigationBarSize();
            updateNavigationBarColor();
        } else if (LauncherApp.get_TrigerVal() == 1) {
            findViewById(R.id.ll_width).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_height).setVisibility(View.GONE);
            sbAlpha.setMax(255);
            sbAlpha.setProgress(LauncherApp.get_LeftBarAlpha());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                sbWidth.setMin(20);
            }
            sbWidth.setMax(40);
            sbWidth.setProgress(LauncherApp.get_LeftBarWith());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                sb_position.setMin(-height - 200);
            }
            sb_position.setMax(height - 200);
            sb_position.setProgress(LauncherApp.get_LeftBarPosition());
            updateNavigationBarSize();
            updateNavigationBarColor();
        } else if (LauncherApp.get_TrigerVal() == 2) {
            findViewById(R.id.ll_width).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_height).setVisibility(View.GONE);
            sbAlpha.setMax(255);
            sbAlpha.setProgress(LauncherApp.get_RightBarAlpha());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                sbWidth.setMin(20);
            }
            sbWidth.setMax(40);
            sbWidth.setProgress(LauncherApp.get_RightBarWith());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                sb_position.setMin(-height - 200);
            }
            sb_position.setMax(height - 200);
            sb_position.setProgress(LauncherApp.get_RightBarPosition());
            updateNavigationBarSize();
            updateNavigationBarColor();
        }
    }

    @SuppressLint({"NewApi"})
    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        if (i != 2) {
            super.onRequestPermissionsResult(i, strArr, iArr);
        } else if (DHANVINE_Utils.checkPermission(PERMISSIONS, this) != 0) {
            if (!shouldShowRequestPermissionRationale("android.permission.WRITE_EXTERNAL_STORAGE")) {
                D_PermiReqDialog d_permiReqDialog = new D_PermiReqDialog(this, new D_PermiReqListener() {
                    public void goToInfo() {
                        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
                        intent.setData(Uri.fromParts("package", getPackageName(), null));
                        startActivityForResult(intent, 4);
                    }

                    public void sureClicked() {
                        finish();
                    }
                });
                d_permiReqDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                d_permiReqDialog.getWindow().setWindowAnimations(R.style.DialogAnim);
                d_permiReqDialog.requestWindowFeature(1);
                d_permiReqDialog.setCancelable(false);
                d_permiReqDialog.setCanceledOnTouchOutside(false);
                d_permiReqDialog.show();
            } else if (DHANVINE_Utils.checkPermission(PERMISSIONS, this) != 0) {
                requestPermissions(PERMISSIONS, 2);
            }
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 1) {
            switch (i) {
                case 3:
                    if (DHANVINE_Utils.isAccessibilityServiceEnabled(this)) {
                        sendBroadcast(new Intent("com.action.ACTION_SHOW_NAVIGATION_BAR"));
                        return;
                    }
                    if (LauncherApp.get_TrigerVal() == 0) {
                        LauncherApp.set_btmService(false);
                    } else if (LauncherApp.get_TrigerVal() == 1) {
                        LauncherApp.set_LeftService(false);
                    } else if (LauncherApp.get_TrigerVal() == 2) {
                        LauncherApp.set_RightService(false);
                    }
                    switch_enable.setChecked(false);
                    return;
                case 4:
                    if (DHANVINE_Utils.checkPermission(PERMISSIONS, this) == 0) {
                        declareUI();
                        return;
                    }
                    D_PermiReqDialog d_permiReqDialog = new D_PermiReqDialog(this, new D_PermiReqListener() {
                        public void goToInfo() {
                            Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
                            intent.setData(Uri.fromParts("package", getPackageName(), null));
                            startActivityForResult(intent, 4);
                        }

                        public void sureClicked() {
                            finish();
                        }
                    });
                    d_permiReqDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                    d_permiReqDialog.getWindow().setWindowAnimations(R.style.DialogAnim);
                    d_permiReqDialog.requestWindowFeature(1);
                    d_permiReqDialog.setCancelable(false);
                    d_permiReqDialog.setCanceledOnTouchOutside(false);
                    d_permiReqDialog.show();
                    return;
                default:
                    super.onActivityResult(i, i2, intent);
                    return;
            }
        } else if (!Settings.canDrawOverlays(this)) {
            switch_enable.setChecked(false);
        }
    }

    private void RefreshAd() {
        AdLoader.Builder builder = new AdLoader.Builder(this, LauncherApp.get_Admob_native_Id());
        builder.forNativeAd(unifiedNativeAd -> {
            if (nativeAds != null) {
                nativeAds.destroy();
            }
            nativeAds = unifiedNativeAd;
            NativeAdView adView = (NativeAdView) getLayoutInflater().inflate(R.layout.small_native, null);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            ((FrameLayout) findViewById(R.id.bottom_adsContainer)).removeAllViews();
            ((FrameLayout) findViewById(R.id.bottom_adsContainer)).addView(adView);
            findViewById(R.id.text_ads_gallry).setVisibility(View.GONE);
        });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                findViewById(R.id.text_ads_gallry).setVisibility(View.GONE);
            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    public static void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (nativeAds != null) {
            nativeAds.destroy();
        }
    }
}