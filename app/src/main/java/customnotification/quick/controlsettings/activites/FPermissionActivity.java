package customnotification.quick.controlsettings.activites;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;

import com.google.android.material.appbar.AppBarLayout;

import java.util.List;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.services.NotificationService;
import customnotification.quick.controlsettings.utils.Constants;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class FPermissionActivity extends Activity implements EasyPermissions.PermissionCallbacks, EasyPermissions.RationaleCallbacks {
    NotificationManager notificationManager;
    LocationManager locationManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Constants.setNotif(FPermissionActivity.this, checkNotificationEnabled(FPermissionActivity.this));
        Clicks();

    }

    public void Clicks() {
        findViewById(R.id.btn_next).setVisibility(View.GONE);
        ((AppBarLayout) findViewById(R.id.app_bar)).addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            ((TextView) findViewById(R.id.txt_tit)).setTextColor(Color.argb(Math.abs(verticalOffset / 2), 0, 0, 0));
            ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(verticalOffset / 2, 0, 0, 0));
            ((TextView) findViewById(R.id.titletxt)).setTextColor(Color.argb(verticalOffset / 2, 0, 0, 0));
            if (Math.abs(verticalOffset) <= 1) {
                ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(255, 0, 0, 0));
                ((TextView) findViewById(R.id.titletxt)).setTextColor(Color.argb(255, 0, 0, 0));
            }
        });
        ((ToggleButton) findViewById(R.id.toggle_2)).setOnCheckedChangeListener((compoundButton, b) -> {
            if (!Constants.checkSystemWritePermission(FPermissionActivity.this)) {
                try {
                    Intent intent = new Intent("android.settings.action.MANAGE_WRITE_SETTINGS");
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivityForResult(intent, 11);
                } catch (ActivityNotFoundException unused) {
                    Toast.makeText(FPermissionActivity.this, "Setting not found", Toast.LENGTH_LONG).show();
                }
            }
        });
        ((ToggleButton) findViewById(R.id.toggle_3)).setOnCheckedChangeListener((compoundButton, b) -> {
            if (!checkNotificationEnabled(FPermissionActivity.this)) {
                try {
                    Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                    Bundle bundle1 = new Bundle();
                    String str = getPackageName() + "/" + NotificationService.class.getName();
                    bundle1.putString(":settings:fragment_args_key", str);
                    intent.putExtra(":settings:fragment_args_key", str);
                    intent.putExtra(":settings:show_fragment_args", bundle1);
                    startActivity(intent, ActivityOptions.makeCustomAnimation(FPermissionActivity.this, R.anim.fade_in, R.anim.fade_out).toBundle());
                } catch (Exception unused) {
                    Toast.makeText(FPermissionActivity.this, "Notification service activity not found.\nPlease grant permission manually", Toast.LENGTH_LONG).show();
                }
            }
            Constants.setNotif(FPermissionActivity.this, checkNotificationEnabled(FPermissionActivity.this));
        });
        findViewById(R.id.btn_next).setOnClickListener(view -> {
            startActivity(new Intent(FPermissionActivity.this, HomeActivity.class));
            finish();
        });
    }

    public void onResume() {
        LauncherApp.set_ServiceONOFF(Constants.checkAccessibilityEnabled(FPermissionActivity.this));
        Constants.setNotif(FPermissionActivity.this, checkNotificationEnabled(FPermissionActivity.this));
        ((ToggleButton) findViewById(R.id.toggle_2)).setChecked(Constants.checkSystemWritePermission(FPermissionActivity.this));
        ((ToggleButton) findViewById(R.id.toggle_3)).setChecked(checkNotificationEnabled(FPermissionActivity.this));
        if (Constants.checkSystemWritePermission(FPermissionActivity.this) && checkNotificationEnabled(FPermissionActivity.this)) {
            findViewById(R.id.btn_next).setVisibility(View.VISIBLE);
            startActivity(new Intent(FPermissionActivity.this, HomeActivity.class));
            finish();
        } else {
            findViewById(R.id.btn_next).setVisibility(View.GONE);
        }
        super.onResume();
    }

    public void onPermissionsGranted(int i, List<String> list) {
    }

    public void onRationaleAccepted(int i) {
    }

    public void onRationaleDenied(int i) {
    }

    @Override
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        EasyPermissions.onRequestPermissionsResult(i, strArr, iArr, this);
    }

    public void onPermissionsDenied(int i, List<String> list) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
            return;
        }
        Toast.makeText(FPermissionActivity.this, "Required permission is not granted.!", Toast.LENGTH_LONG).show();
        if (i == 15) {
            LauncherApp.set_ServiceONOFF(false);
            LauncherApp.set_Panelheight(false);
            LauncherApp.set_OpenFirst(false);
            enableLockToggle();
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 9) {
            if (Settings.canDrawOverlays(FPermissionActivity.this)) {
                LauncherApp.set_ServiceONOFF(true);
                enableLockToggle();
                return;
            }
            LauncherApp.set_ServiceONOFF(false);
            disableLockToggle();
        }
    }

    public boolean checkNotificationEnabled(Context context2) {
        try {
            return Settings.Secure.getString(context2.getContentResolver(), "enabled_notification_listeners").contains(context2.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void disableLockToggle() {
        LauncherApp.set_ServiceONOFF(false);
    }

    private void enableLockToggle() {
        LauncherApp.set_ServiceONOFF(true);
    }
}