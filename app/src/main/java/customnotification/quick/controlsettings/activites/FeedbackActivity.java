package customnotification.quick.controlsettings.activites;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.model.ApiInterface;
import customnotification.quick.controlsettings.model.FeedbackModel;
import customnotification.quick.controlsettings.model.VideoStatusClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends Activity {
    EditText TextDescription;
    EditText TextTitle;
    boolean isFatching = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        initUI();
        clickListener();
        getWindow().setSoftInputMode(3);
        closeKeyboard();
    }

    private void initUI() {
        TextTitle = findViewById(R.id.TextTitle);
        TextDescription = findViewById(R.id.TextDescription);
    }

    private void clickListener() {
        findViewById(R.id.imageViewBack).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.SendEmail).setOnClickListener(v -> {
            if (TextTitle.getText().toString().trim().equalsIgnoreCase("")) {
                Toast.makeText(FeedbackActivity.this, "Please enter title", Toast.LENGTH_SHORT).show();
            } else if (TextDescription.getText().toString().trim().equalsIgnoreCase("")) {
                Toast.makeText(FeedbackActivity.this, "Please enter description", Toast.LENGTH_SHORT).show();
            } else {
                PackageInfo packageInfo = null;
                try {
                    packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception ignored) {
                }
                PostFeedback(packageInfo.versionName, getResources().getString(R.string.app_name), String.valueOf(Build.VERSION.SDK_INT), Build.MODEL, getPackageName(), TextTitle.getText().toString(), TextDescription.getText().toString());
            }
        });
    }

    private void PostFeedback(String appverson, String appname, String appversoncode, String mobilemodel, String packagename, String title, String description) {
        if (!isFatching) {
            findViewById(R.id.dialog_ll).setVisibility(View.VISIBLE);
            isFatching = true;
            VideoStatusClient.getClientfeedback().create(ApiInterface.class).sendfeedback(appname, packagename, title, description, mobilemodel, appversoncode, appverson).enqueue(new Callback<FeedbackModel>() {
                @Override
                public void onResponse(@NotNull Call<FeedbackModel> call, @NotNull Response<FeedbackModel> response) {
                    if (response.code() == 200) {
                        isFatching = false;
                        assert response.body() != null;
                        if (response.body().getStatus()) {
                            Toast.makeText(FeedbackActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            TextDescription.setText("");
                            TextTitle.setText("");
                            findViewById(R.id.dialog_ll).setVisibility(View.GONE);
                        }
                    } else {
                        findViewById(R.id.dialog_ll).setVisibility(View.GONE);
                        Toast.makeText(FeedbackActivity.this, "Something went wrong...", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NotNull Call<FeedbackModel> call, Throwable t) {
                    findViewById(R.id.dialog_ll).setVisibility(View.GONE);
                    Toast.makeText(FeedbackActivity.this, "Please send feedback in playstore", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void closeKeyboard() {
        try {
            View view = getCurrentFocus();
            if (view != null) {
                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onBackPressed() {
        finish();
    }
}