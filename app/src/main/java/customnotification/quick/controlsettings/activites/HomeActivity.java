package customnotification.quick.controlsettings.activites;

import static customnotification.quick.controlsettings.LauncherApp.checkqureka;
import static customnotification.quick.controlsettings.LauncherApp.querekatext;
import static customnotification.quick.controlsettings.LauncherApp.qurekaimage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.browser.customtabs.CustomTabsIntent;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.onesignal.OneSignal;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.services.MAccessibilityService;
import customnotification.quick.controlsettings.utils.Constants;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class HomeActivity extends Activity implements EasyPermissions.PermissionCallbacks, EasyPermissions.RationaleCallbacks {
    ToggleButton toggle_enable;
    NativeAd nativeAd, nativeAds;
    InterstitialAd mInterstitialAd;
    LottieAnimationView Howto;
    BottomSheetDialog exitdialog;
    Handler handler = new Handler();
    Runnable runnable;
    boolean changeval;

    @SuppressLint("ResourceType")
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_home);
        LauncherApp.appOpenManager.isAdShow = true;
        toggle_enable = findViewById(R.id.toggle_enable);
        findViewById(R.id.btn_service).setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        firebaseAnalytics();
        InitView();
    }

    public void firebaseAnalytics() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        FirebaseAnalytics.getInstance(this);
        FirebaseCrashlytics.getInstance();
        OneSignal.initWithContext(this);
        OneSignal.setAppId(getString(R.string.one_signal));
    }
    public void InitView(){
        if (Constants.getCameraPkg(this).equals("")) {
            loadApps();
        }
        getrGames();
        LoadInterstitial();
        RefreshAd();
        ExitDiloge();
        Clicks();
        LauncherApp.set_ServiceONOFF(Constants.checkAccessibilityEnabled(HomeActivity.this));
        if (Constants.checkAccessibilityEnabled(HomeActivity.this)) {
            findViewById(R.id.servicecolor).setBackgroundColor(getResources().getColor(R.color.serviceon));
            ((TextView) findViewById(R.id.setvicetxt)).setTextColor(getResources().getColor(R.color.white));
        } else {
            findViewById(R.id.servicecolor).setBackgroundColor(getResources().getColor(R.color.white));
            ((TextView) findViewById(R.id.setvicetxt)).setTextColor(getResources().getColor(R.color.black));
            LauncherApp.set_Panelheight(false);
            LauncherApp.set_OpenFirst(false);
        }
    }

    public void Clicks() {
        Howto = findViewById(R.id.howto);
        Howto.setImageAssetsFolder("lottie/img_how/images");
        Howto.setAnimation("lottie/img_how/data.json");
        Howto.playAnimation();
        ((AppBarLayout) findViewById(R.id.app_bar)).addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            ((TextView) findViewById(R.id.txt_tit)).setTextColor(Color.argb(Math.abs(verticalOffset / 2), 0, 0, 0));
            ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(verticalOffset / 2, 0, 0, 0));
            ((ImageView) findViewById(R.id.img_top)).setAlpha(verticalOffset / 2);
            if (Math.abs(verticalOffset) <= 1) {
                ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(255, 0, 0, 0));
                ((ImageView) findViewById(R.id.img_top)).setAlpha(255);
            }
        });
        toggle_enable.setChecked(LauncherApp.get_ServiceONOFF());
        if (!LauncherApp.get_ServiceONOFF()) {
            LauncherApp.set_Panelheight(false);
            LauncherApp.set_OpenFirst(false);
        }
        toggle_enable.setChecked(Constants.checkAccessibilityEnabled(HomeActivity.this));
        toggle_enable.setOnCheckedChangeListener((compoundButton, z) -> {
            if (z) {
                accessPhoneStatePermision();
            } else {
                findViewById(R.id.servicecolor).setBackgroundColor(getResources().getColor(R.color.white));
                ((TextView) findViewById(R.id.setvicetxt)).setTextColor(getResources().getColor(R.color.black));
                LauncherApp.set_Panelheight(false);
                LauncherApp.set_OpenFirst(false);
                stopService(HomeActivity.this, 0);
            }
        });
        findViewById(R.id.btn_service).setOnClickListener(view -> {
            if (!toggle_enable.isChecked()) {
                accessPhoneStatePermision();
            } else {
                findViewById(R.id.servicecolor).setBackgroundColor(getResources().getColor(R.color.white));
                ((TextView) findViewById(R.id.setvicetxt)).setTextColor(getResources().getColor(R.color.black));
                toggle_enable.setChecked(false);
                LauncherApp.set_Panelheight(false);
                LauncherApp.set_OpenFirst(false);
                stopService(HomeActivity.this, 0);
            }
        });
        findViewById(R.id.btn_colors).setOnClickListener(view -> startActivity(new Intent(HomeActivity.this, ColorManageActivity.class)));
        findViewById(R.id.btn_layoutmanage).setOnClickListener(view -> {
            startActivity(new Intent(HomeActivity.this, LayoutrManageActivity.class));
            try {
                if (mInterstitialAd != null) {
                    mInterstitialAd.show(HomeActivity.this);
                }
                LauncherApp.appOpenManager.isAdShow = true;
            } catch (Exception ignored) {
            }
        });
        findViewById(R.id.btn_Edgetrig).setOnClickListener(view -> startActivity(new Intent(HomeActivity.this, EDGETrigActivity.class)));
        findViewById(R.id.btn_bg).setOnClickListener(view -> {
            startActivity(new Intent(HomeActivity.this, W_WallpaperActivity.class));
            try {
                if (mInterstitialAd != null) {
                    mInterstitialAd.show(HomeActivity.this);
                }
                LauncherApp.appOpenManager.isAdShow = true;
            } catch (Exception ignored) {
            }
        });
        Howto.setOnClickListener(view -> {
            Intent intent = new Intent(HomeActivity.this, HowToActivity.class);
            intent.putExtra("howint", "btn");
            startActivity(intent);
        });
        findViewById(R.id.btn_setting).setOnClickListener(v -> startActivity(new Intent(HomeActivity.this, SettingActivity.class)));
    }

    public boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void onPermissionsGranted(int i, List<String> list) {
    }

    public void onRationaleAccepted(int i) {
    }

    public void onRationaleDenied(int i) {
    }

    private void loadApps() {
        new Thread(() -> {
            try {
                PackageManager packageManager = getPackageManager();
                Intent intent = new Intent("android.intent.action.MAIN", null);
                intent.addCategory("android.intent.category.LAUNCHER");
                for (ResolveInfo resolveInfo : packageManager.queryIntentActivities(intent, 0)) {
                    String charSequence = resolveInfo.loadLabel(packageManager).toString();
                    if (charSequence.equalsIgnoreCase("camera")) {
                        Constants.setCameraPkg(HomeActivity.this, resolveInfo.activityInfo.packageName);
                    }
                    if (resolveInfo.loadLabel(packageManager).toString().equalsIgnoreCase("phone") || resolveInfo.activityInfo.name.equalsIgnoreCase("dialler")) {
                        Constants.setPhonePkg(HomeActivity.this, resolveInfo.activityInfo.packageName);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void onBackPressed() {
        exitdialog.show();
    }

    public void onResume() {
        if (!Constants.checkAccessibilityEnabled(HomeActivity.this)) {
            handler.postDelayed(runnable = () -> {
                handler.postDelayed(runnable, 500);
                if (changeval) {
                    findViewById(R.id.servicecolor).setBackgroundColor(getResources().getColor(R.color.dscdcd));
                    ((TextView) findViewById(R.id.setvicetxt)).setTextColor(getResources().getColor(R.color.white));
                    changeval = false;
                } else {
                    findViewById(R.id.servicecolor).setBackgroundColor(getResources().getColor(R.color.white));
                    ((TextView) findViewById(R.id.setvicetxt)).setTextColor(getResources().getColor(R.color.black));
                    changeval = true;
                }
            }, 500);
        }
        toggle_enable.setChecked(Constants.checkAccessibilityEnabled(HomeActivity.this));
        LauncherApp.set_ServiceONOFF(Constants.checkAccessibilityEnabled(HomeActivity.this));
        if (Constants.checkAccessibilityEnabled(HomeActivity.this)) {
            handler.removeCallbacks(runnable);
            findViewById(R.id.servicecolor).setBackgroundColor(getResources().getColor(R.color.serviceon));
            ((TextView) findViewById(R.id.setvicetxt)).setTextColor(getResources().getColor(R.color.white));
        } else {
            findViewById(R.id.servicecolor).setBackgroundColor(getResources().getColor(R.color.white));
            ((TextView) findViewById(R.id.setvicetxt)).setTextColor(getResources().getColor(R.color.black));
        }
        super.onResume();
    }

    @AfterPermissionGranted(15)
    public void accessPhoneStatePermision() {
        if (Constants.hasPermissions(HomeActivity.this, Constants.PHONE_STATE_PERMISSION)) {
            enableLock();
        } else {
            requestPermissions(Constants.PHONE_STATE_PERMISSION, 15);
        }
    }

    @Override
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        EasyPermissions.onRequestPermissionsResult(i, strArr, iArr, this);
    }

    public void onPermissionsDenied(int i, List<String> list) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
            return;
        }
        Toast.makeText(HomeActivity.this, "Required permission is not granted.!", Toast.LENGTH_LONG).show();
        if (i == 15) {
            toggle_enable.setChecked(false);
            LauncherApp.set_ServiceONOFF(false);

            LauncherApp.set_Panelheight(false);
            LauncherApp.set_OpenFirst(false);
            findViewById(R.id.servicecolor).setBackgroundColor(getResources().getColor(R.color.white));
            ((TextView) findViewById(R.id.setvicetxt)).setTextColor(getResources().getColor(R.color.black));
        }
    }

    private void enableLock() {
        Intent intent = new Intent("com.samsung.accessibility.installed_service");
        if (intent.resolveActivity(getPackageManager()) == null) {
            intent = new Intent("android.settings.ACCESSIBILITY_SETTINGS");
        }
        Bundle bundle = new Bundle();
        String str = getPackageName() + "/" + MAccessibilityService.class.getName();
        bundle.putString(":settings:fragment_args_key", str);
        intent.putExtra(":settings:fragment_args_key", str);
        intent.putExtra(":settings:show_fragment_args", bundle);
        StartPermissionActivity(HomeActivity.this, intent);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 9) {
            if (Settings.canDrawOverlays(HomeActivity.this)) {
                toggle_enable.setChecked(true);
                LauncherApp.set_ServiceONOFF(true);
                findViewById(R.id.servicecolor).setBackgroundColor(getResources().getColor(R.color.serviceon));
                ((TextView) findViewById(R.id.setvicetxt)).setTextColor(getResources().getColor(R.color.white));
                return;
            }
            toggle_enable.setChecked(false);
            LauncherApp.set_ServiceONOFF(false);
            LauncherApp.set_Panelheight(false);
            LauncherApp.set_OpenFirst(false);
            findViewById(R.id.servicecolor).setBackgroundColor(getResources().getColor(R.color.white));
            ((TextView) findViewById(R.id.setvicetxt)).setTextColor(getResources().getColor(R.color.black));
        }
    }

    public void StartPermissionActivity(Context context2, Intent intent) {
        context2.startActivity(intent, ActivityOptions.makeCustomAnimation(context2, R.anim.fade_in, R.anim.fade_out).toBundle());
    }

    public static void stopService(Context context2, int i) {
        try {
            Intent intent = new Intent(context2, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", i);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context2.startForegroundService(intent);
            } else {
                context2.startService(intent);
            }
        } catch (Throwable ignored) {
        }
    }

    public void ExitDiloge() {
        exitdialog = new BottomSheetDialog(this);
        exitdialog.setContentView(R.layout.dialog_exit);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(exitdialog.findViewById(com.google.android.material.R.id.design_bottom_sheet));
        bottomSheetBehavior.setPeekHeight(Resources.getSystem().getDisplayMetrics().heightPixels);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        FrameLayout frmlay = exitdialog.findViewById(R.id.frmlay);
        AdLoader.Builder builder = new AdLoader.Builder(this, LauncherApp.get_Admob_native_Id());
        builder.forNativeAd(unifiedNativeAd -> {
            if (nativeAd != null) {
                nativeAd.destroy();
            }
            nativeAd = unifiedNativeAd;
            NativeAdView adView = (NativeAdView) getLayoutInflater().inflate(R.layout.unifiednativead, null);
            populateUnifiedNativeAdView(nativeAd, adView);
            frmlay.removeAllViews();
            frmlay.addView(adView);
        });
        builder.build().loadAd(new AdRequest.Builder().build());
        exitdialog.findViewById(R.id.crdads).setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        exitdialog.findViewById(R.id.btn_exit).setOnClickListener(view -> {
            exitdialog.dismiss();
            finish();
        });
        exitdialog.findViewById(R.id.btn_notnoww).setOnClickListener(view -> exitdialog.dismiss());
    }

    private void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        adView.setMediaView(adView.findViewById(R.id.ad_media));
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    public void LoadInterstitial() {
        InterstitialAd.load(this, LauncherApp.get_Admob_interstitial_Id(), new AdRequest.Builder().build(), new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        super.onAdDismissedFullScreenContent();
                        mInterstitialAd = null;
                        LauncherApp.appOpenManager.isAdShow = false;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                mInterstitialAd = null;
                LauncherApp.appOpenManager.isAdShow = false;
            }
        });
    }

    private void RefreshAd() {
        AdLoader.Builder builder = new AdLoader.Builder(this, LauncherApp.get_Admob_native_Id());
        builder.forNativeAd(unifiedNativeAd -> {
            if (nativeAds != null) {
                nativeAds.destroy();
            }
            nativeAds = unifiedNativeAd;
            NativeAdView adView = (NativeAdView) getLayoutInflater().inflate(R.layout.small_native, null);
            populateUnifiedNativeAdViewaa(unifiedNativeAd, adView);
            ((FrameLayout) findViewById(R.id.bottom_adsContainer)).removeAllViews();
            ((FrameLayout) findViewById(R.id.bottom_adsContainer)).addView(adView);
            findViewById(R.id.text_ads_gallry).setVisibility(View.GONE);
        });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                findViewById(R.id.text_ads_gallry).setVisibility(View.GONE);
            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    public static void populateUnifiedNativeAdViewaa(NativeAd nativeAd, NativeAdView adView) {
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    public void onDestroy() {
        LauncherApp.getInstance().unregisterListener();
        if (nativeAd != null) {
            nativeAd.destroy();
        }
        if (nativeAds != null) {
            nativeAds.destroy();
        }
        super.onDestroy();
    }

    public void getrGames() {
        if (isNetworkAvailable()) {
            if (checkqureka) {
                if (!qurekaimage.equals("")) {
                    Glide.with(this).load(qurekaimage).into((ImageView) findViewById(R.id.img_game));
                    ((TextView) findViewById(R.id.txt_game)).setText(querekatext);
                    findViewById(R.id.btn_playgame).setVisibility(View.VISIBLE);
                    findViewById(R.id.btn_playgame).setOnClickListener(view -> {
                        try {
                            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                            CustomTabsIntent customTabsIntent = builder.build();
                            customTabsIntent.intent.setPackage("com.android.chrome");
                            customTabsIntent.launchUrl(HomeActivity.this, Uri.parse(LauncherApp.url));
                        } catch (Exception e) {
                            Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    findViewById(R.id.btn_playgame).setVisibility(View.GONE);
                }
            }
        }
    }
}