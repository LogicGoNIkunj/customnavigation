package customnotification.quick.controlsettings.activites;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import customnotification.quick.controlsettings.R;

public class HowToActivity extends FragmentActivity {
    static TextView btmdone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_howto);
        btmdone = findViewById(R.id.btmdone);
        findViewById(R.id.btn_back).setOnClickListener(view -> onBackPressed());
        if (getIntent().getExtras().getString("howint").equals("start")) {
            findViewById(R.id.topll).setVisibility(View.GONE);
            btmdone.setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.topll).setVisibility(View.VISIBLE);
            btmdone.setVisibility(View.VISIBLE);
        }
        btmdone.setOnClickListener(view -> {
            startActivity(new Intent(HowToActivity.this, FPermissionActivity.class));
            finish();
        });
        ((ViewPager) findViewById(R.id.pager)).setAdapter(new ScreenSlidePagerAdapter(getSupportFragmentManager()));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    static class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return TutorialFragment.create(position);
        }

        @Override
        public int getCount() {
            return 4;
        }
    }
}