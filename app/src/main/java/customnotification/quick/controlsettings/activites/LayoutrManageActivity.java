package customnotification.quick.controlsettings.activites;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.android.material.appbar.AppBarLayout;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import java.util.ArrayList;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.largetile.TilesShape;
import customnotification.quick.controlsettings.largetile.TilesShapeAdapter;
import customnotification.quick.controlsettings.services.MAccessibilityService;

public class LayoutrManageActivity extends Activity {
    AlertDialog Dialog_qkIconShape, Dialog_headerNumber, Dialog_GridTiles;
    String MyVal;
    int Myvals;
    NativeAd nativeAds;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layoutmanager);

        initview();
        RefreshAd();
        Clicks();
    }

    public void initview() {
        ((TextView) findViewById(R.id.txt_qkset)).setText(LauncherApp.get_qsIconShapeName());
        ((TextView) findViewById(R.id.txt_gridtiles)).setText(String.valueOf(LauncherApp.get_GridTiles()));
        ((TextView) findViewById(R.id.txt_clocksize)).setText(String.valueOf(LauncherApp.get_Clocksize()));
        ((IndicatorSeekBar) findViewById(R.id.txtsizeseek)).setProgress(LauncherApp.get_Clocksize());
        changeTiles();
        ((ImageView) findViewById(R.id.img_qkset)).setImageResource(LauncherApp.get_IconShapethumb());
        ((ToggleButton) findViewById(R.id.switch_clocksecond)).setChecked(LauncherApp.get_ClockSecondshow());
        ((ToggleButton) findViewById(R.id.switch_brightnesshadder)).setChecked(LauncherApp.get_brigtnesshader());
        ((ToggleButton) findViewById(R.id.switch_smallcorner)).setChecked(LauncherApp.get_SmallCorner());
        ((ToggleButton) findViewById(R.id.switch_powerbtn)).setChecked(LauncherApp.get_powerbtn());
    }

    public void Clicks() {
        ((AppBarLayout) findViewById(R.id.app_bar)).addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            ((TextView) findViewById(R.id.txt_tit)).setTextColor(Color.argb(Math.abs(verticalOffset / 2), 0, 0, 0));
            ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(verticalOffset / 2, 0, 0, 0));
            ((ImageView) findViewById(R.id.img_top)).setAlpha(verticalOffset / 2);
            if (Math.abs(verticalOffset) <= 1) {
                ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(255, 0, 0, 0));
                ((ImageView) findViewById(R.id.img_top)).setAlpha(255);
            }
        });
        findViewById(R.id.btn_back).setOnClickListener(view -> onBackPressed());
        findViewById(R.id.btn_qksettingbg).setOnClickListener(view -> DialogQuickSetIconShape());
        findViewById(R.id.btn_numberhadertiles).setOnClickListener(view -> DialogNumofHeaderTiles());
        findViewById(R.id.btn_gridTiles).setOnClickListener(view -> DialogGridTiles());
        ((IndicatorSeekBar) findViewById(R.id.txtsizeseek)).setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
                LauncherApp.set_Clocksize(seekBar.getProgress());
                ((TextView) findViewById(R.id.txt_clocksize)).setText(String.valueOf(LauncherApp.get_Clocksize()));
                if (LauncherApp.get_ServiceONOFF()) {
                    Intent intent = new Intent(LayoutrManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 9);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startForegroundService(intent);
                    } else {
                        startService(intent);
                    }
                }
            }
        });
        findViewById(R.id.btn_sec_clock).setOnClickListener(view -> {
            if (((ToggleButton) findViewById(R.id.switch_clocksecond)).isChecked()) {
                LauncherApp.set_ClockSecondshow(false);
                ((ToggleButton) findViewById(R.id.switch_clocksecond)).setChecked(false);
            } else {
                LauncherApp.set_ClockSecondshow(true);
                ((ToggleButton) findViewById(R.id.switch_clocksecond)).setChecked(true);
            }
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(LayoutrManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 2);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_swbright).setOnClickListener(view -> {
            if (((ToggleButton) findViewById(R.id.switch_brightnesshadder)).isChecked()) {
                LauncherApp.set_brigtnesshader(false);
                ((ToggleButton) findViewById(R.id.switch_brightnesshadder)).setChecked(false);
            } else {
                LauncherApp.set_brigtnesshader(true);
                ((ToggleButton) findViewById(R.id.switch_brightnesshadder)).setChecked(true);
            }
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(LayoutrManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 3);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_swpower).setOnClickListener(view -> {
            if (((ToggleButton) findViewById(R.id.switch_powerbtn)).isChecked()) {
                LauncherApp.set_powerbtn(false);
                ((ToggleButton) findViewById(R.id.switch_powerbtn)).setChecked(false);
            } else {
                LauncherApp.set_powerbtn(true);
                ((ToggleButton) findViewById(R.id.switch_powerbtn)).setChecked(true);
            }
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(LayoutrManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 7);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
        findViewById(R.id.btn_swcorner).setOnClickListener(view -> {
            if (((ToggleButton) findViewById(R.id.switch_smallcorner)).isChecked()) {
                LauncherApp.set_SmallCorner(false);
                ((ToggleButton) findViewById(R.id.switch_smallcorner)).setChecked(false);
            } else {
                LauncherApp.set_SmallCorner(true);
                ((ToggleButton) findViewById(R.id.switch_smallcorner)).setChecked(true);
            }
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(LayoutrManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 5);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
        });
    }

    public void DialogQuickSetIconShape() {
        View alertLayout = getLayoutInflater().inflate(R.layout.dialog_qs_iconshape, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        Dialog_qkIconShape = alert.create();
        Dialog_qkIconShape.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        ArrayList<TilesShape> stringArrayList = new ArrayList<>();
        stringArrayList.add(new TilesShape(R.drawable.default_blue, "Circle"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_circle_slice, "Circle Slice"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_circle_holo, "Circle Hollow"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_teardrop, "Teardrop"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_rect, "Rectangle"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_squircle, "Squircle"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_hexagon, "Hexagon"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_pentagon, "Pentagon"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_flower, "Flower"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_vessel, "Vessel"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_tapered_rect, "Tapered Rectangle"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_pebble, "Pebble"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_diamond, "Diamond"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_heart, "Heart"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_paw, "Dog Paw"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_bell, "Bell"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_sun, "Sun"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_star, "Star"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_file, "File"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_folder, "Folder"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_briefcase, "Briefcase"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_tag, "Price tag"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_bulb, "Bulb"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_medal, "Medal"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_emblem, "Emblem"));
        stringArrayList.add(new TilesShape(R.drawable.ic_qs_shape_shield, "Shield"));
        ((RecyclerView) alertLayout.findViewById(R.id.rv_qsicons)).setLayoutManager(new LinearLayoutManager(LayoutrManageActivity.this));
        ((RecyclerView) alertLayout.findViewById(R.id.rv_qsicons)).setAdapter(new TilesShapeAdapter(LayoutrManageActivity.this, stringArrayList, Dialog_qkIconShape));
        alertLayout.findViewById(R.id.btn_cancel).setOnClickListener(v -> Dialog_qkIconShape.dismiss());
        Dialog_qkIconShape.setOnDismissListener(dialogInterface -> {
            ((TextView) findViewById(R.id.txt_qkset)).setText(LauncherApp.get_qsIconShapeName());
            ((ImageView) findViewById(R.id.img_qkset)).setImageResource(LauncherApp.get_IconShapethumb());
        });
        Dialog_qkIconShape.show();
    }

    public void DialogGridTiles() {
        View alertLayout = getLayoutInflater().inflate(R.layout.dialog_gridtiles, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        LinearLayout lm_5 = alertLayout.findViewById(R.id.lm_5);
        LinearLayout lm_6 = alertLayout.findViewById(R.id.lm_6);
        ImageView view_1_4 = alertLayout.findViewById(R.id.view_1_4);
        ImageView view_1_5 = alertLayout.findViewById(R.id.view_1_5);
        ImageView view_2_4 = alertLayout.findViewById(R.id.view_2_4);
        ImageView view_2_5 = alertLayout.findViewById(R.id.view_2_5);
        ImageView view_3_4 = alertLayout.findViewById(R.id.view_3_4);
        ImageView view_3_5 = alertLayout.findViewById(R.id.view_3_5);
        ImageView view_4_4 = alertLayout.findViewById(R.id.view_4_4);
        ImageView view_4_5 = alertLayout.findViewById(R.id.view_4_5);
        ImageView view_5_4 = alertLayout.findViewById(R.id.view_5_4);
        ImageView view_5_5 = alertLayout.findViewById(R.id.view_5_5);
        ImageView view_6_4 = alertLayout.findViewById(R.id.view_6_4);
        ImageView view_6_5 = alertLayout.findViewById(R.id.view_6_5);
        TextView btn_36 = alertLayout.findViewById(R.id.btn_36);
        TextView btn_44 = alertLayout.findViewById(R.id.btn_44);
        TextView btn_54 = alertLayout.findViewById(R.id.btn_54);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        Dialog_GridTiles = alert.create();
        Dialog_GridTiles.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        MyVal = LauncherApp.get_GridTiles();
        manageGridTiles(MyVal, lm_5, lm_6, view_1_4, view_1_5, view_2_4, view_2_5, view_3_4, view_3_5, view_4_4, view_4_5, view_5_4, view_5_5, view_6_4, view_6_5, btn_36, btn_44, btn_54);
        btn_36.setOnClickListener(view -> manageGridTiles("3 * 6", lm_5, lm_6, view_1_4, view_1_5, view_2_4, view_2_5, view_3_4, view_3_5, view_4_4, view_4_5, view_5_4, view_5_5, view_6_4, view_6_5, btn_36, btn_44, btn_54));
        btn_44.setOnClickListener(view -> manageGridTiles("4 * 4", lm_5, lm_6, view_1_4, view_1_5, view_2_4, view_2_5, view_3_4, view_3_5, view_4_4, view_4_5, view_5_4, view_5_5, view_6_4, view_6_5, btn_36, btn_44, btn_54));
        btn_54.setOnClickListener(view -> manageGridTiles("5 * 4", lm_5, lm_6, view_1_4, view_1_5, view_2_4, view_2_5, view_3_4, view_3_5, view_4_4, view_4_5, view_5_4, view_5_5, view_6_4, view_6_5, btn_36, btn_44, btn_54));
        alertLayout.findViewById(R.id.btn_cancel).setOnClickListener(v -> Dialog_GridTiles.dismiss());
        alertLayout.findViewById(R.id.btn_ok).setOnClickListener(v -> {
            LauncherApp.set_GridTiles(MyVal);
            ((TextView) findViewById(R.id.txt_gridtiles)).setText(String.valueOf(LauncherApp.get_GridTiles()));
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(LayoutrManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 10);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
            Dialog_GridTiles.dismiss();
        });
        Dialog_GridTiles.show();
    }

    public void manageGridTiles(String totaltiles, LinearLayout lm_5, LinearLayout lm_6, ImageView view_1_4, ImageView view_1_5, ImageView view_2_4, ImageView view_2_5, ImageView view_3_4, ImageView view_3_5, ImageView view_4_4, ImageView view_4_5, ImageView view_5_4, ImageView view_5_5, ImageView view_6_4, ImageView view_6_5, TextView btn_36, TextView btn_44, TextView btn_54) {
        MyVal = totaltiles;
        switch (totaltiles) {
            case "3 * 6":
                btn_36.setBackground(getResources().getDrawable(R.drawable.btn_selected));
                btn_44.setBackground(null);
                btn_54.setBackground(null);
                lm_5.setVisibility(View.VISIBLE);
                lm_6.setVisibility(View.VISIBLE);
                view_1_4.setVisibility(View.GONE);
                view_1_5.setVisibility(View.GONE);
                view_2_4.setVisibility(View.GONE);
                view_2_5.setVisibility(View.GONE);
                view_3_4.setVisibility(View.GONE);
                view_3_5.setVisibility(View.GONE);
                view_4_4.setVisibility(View.GONE);
                view_4_5.setVisibility(View.GONE);
                view_5_4.setVisibility(View.GONE);
                view_5_5.setVisibility(View.GONE);
                view_6_4.setVisibility(View.GONE);
                view_6_5.setVisibility(View.GONE);
                break;
            case "4 * 4":
                btn_36.setBackground(null);
                btn_44.setBackground(getResources().getDrawable(R.drawable.btn_selected));
                btn_54.setBackground(null);
                lm_5.setVisibility(View.GONE);
                lm_6.setVisibility(View.GONE);
                view_1_4.setVisibility(View.VISIBLE);
                view_1_5.setVisibility(View.GONE);
                view_2_4.setVisibility(View.VISIBLE);
                view_2_5.setVisibility(View.GONE);
                view_3_4.setVisibility(View.VISIBLE);
                view_3_5.setVisibility(View.GONE);
                view_4_4.setVisibility(View.VISIBLE);
                view_4_5.setVisibility(View.GONE);
                break;
            case "5 * 4":
                btn_36.setBackground(null);
                btn_44.setBackground(null);
                btn_54.setBackground(getResources().getDrawable(R.drawable.btn_selected));
                lm_5.setVisibility(View.GONE);
                lm_6.setVisibility(View.GONE);
                view_1_4.setVisibility(View.VISIBLE);
                view_1_5.setVisibility(View.VISIBLE);
                view_2_4.setVisibility(View.VISIBLE);
                view_2_5.setVisibility(View.VISIBLE);
                view_3_4.setVisibility(View.VISIBLE);
                view_3_5.setVisibility(View.VISIBLE);
                view_4_4.setVisibility(View.VISIBLE);
                view_4_5.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void DialogNumofHeaderTiles() {
        View alertLayout = getLayoutInflater().inflate(R.layout.dialog_headertiles, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        ImageView img4 = alertLayout.findViewById(R.id.img4);
        ImageView img5 = alertLayout.findViewById(R.id.img5);
        ImageView img6 = alertLayout.findViewById(R.id.img6);
        TextView btn_txt3 = alertLayout.findViewById(R.id.btn_txt3);
        TextView btn_txt4 = alertLayout.findViewById(R.id.btn_txt4);
        TextView btn_txt5 = alertLayout.findViewById(R.id.btn_txt5);
        TextView btn_txt6 = alertLayout.findViewById(R.id.btn_txt6);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        Dialog_headerNumber = alert.create();
        Dialog_headerNumber.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        Myvals = LauncherApp.get_headertiles();
        manageHeaderTiles(Myvals, img4, img5, img6, btn_txt3, btn_txt4, btn_txt5, btn_txt6);
        btn_txt3.setOnClickListener(view -> manageHeaderTiles(3, img4, img5, img6, btn_txt3, btn_txt4, btn_txt5, btn_txt6));
        btn_txt4.setOnClickListener(view -> manageHeaderTiles(4, img4, img5, img6, btn_txt3, btn_txt4, btn_txt5, btn_txt6));
        btn_txt5.setOnClickListener(view -> manageHeaderTiles(5, img4, img5, img6, btn_txt3, btn_txt4, btn_txt5, btn_txt6));
        btn_txt6.setOnClickListener(view -> manageHeaderTiles(6, img4, img5, img6, btn_txt3, btn_txt4, btn_txt5, btn_txt6));
        alertLayout.findViewById(R.id.btn_cancel).setOnClickListener(v -> Dialog_headerNumber.dismiss());
        alertLayout.findViewById(R.id.btn_ok).setOnClickListener(v -> {
            LauncherApp.set_headertiles(Myvals);
            changeTiles();
            if (LauncherApp.get_ServiceONOFF()) {
                Intent intent = new Intent(LayoutrManageActivity.this, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 6);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            }
            Dialog_headerNumber.dismiss();
        });
        Dialog_headerNumber.show();
    }

    public void manageHeaderTiles(int totaltiles, ImageView img4, ImageView img5, ImageView img6, TextView btn_txt3, TextView btn_txt4, TextView btn_txt5, TextView btn_txt6) {
        Myvals = totaltiles;
        if (totaltiles == 3) {
            btn_txt3.setBackground(getResources().getDrawable(R.drawable.btn_selected));
            btn_txt4.setBackground(null);
            btn_txt5.setBackground(null);
            btn_txt6.setBackground(null);
            img4.setVisibility(View.GONE);
            img5.setVisibility(View.GONE);
            img6.setVisibility(View.GONE);
        } else if (totaltiles == 4) {
            btn_txt3.setBackground(null);
            btn_txt4.setBackground(getResources().getDrawable(R.drawable.btn_selected));
            btn_txt5.setBackground(null);
            btn_txt6.setBackground(null);
            img4.setVisibility(View.VISIBLE);
            img5.setVisibility(View.GONE);
            img6.setVisibility(View.GONE);
        } else if (totaltiles == 5) {
            btn_txt3.setBackground(null);
            btn_txt4.setBackground(null);
            btn_txt5.setBackground(getResources().getDrawable(R.drawable.btn_selected));
            btn_txt6.setBackground(null);
            img4.setVisibility(View.VISIBLE);
            img5.setVisibility(View.VISIBLE);
            img6.setVisibility(View.GONE);
        } else if (totaltiles == 6) {
            btn_txt3.setBackground(null);
            btn_txt4.setBackground(null);
            btn_txt5.setBackground(null);
            btn_txt6.setBackground(getResources().getDrawable(R.drawable.btn_selected));
            img4.setVisibility(View.VISIBLE);
            img5.setVisibility(View.VISIBLE);
            img6.setVisibility(View.VISIBLE);
        }
    }

    public void changeTiles() {
        ((TextView) findViewById(R.id.txt_hadertilesnumber)).setText(String.valueOf(LauncherApp.get_headertiles()));
        if (LauncherApp.get_headertiles() == 3) {
            findViewById(R.id.lm_img_4).setVisibility(View.GONE);
            findViewById(R.id.lm_img_5).setVisibility(View.GONE);
            findViewById(R.id.lm_img_6).setVisibility(View.GONE);
        } else if (LauncherApp.get_headertiles() == 4) {
            findViewById(R.id.lm_img_4).setVisibility(View.VISIBLE);
            findViewById(R.id.lm_img_5).setVisibility(View.GONE);
            findViewById(R.id.lm_img_6).setVisibility(View.GONE);
        } else if (LauncherApp.get_headertiles() == 5) {
            findViewById(R.id.lm_img_4).setVisibility(View.VISIBLE);
            findViewById(R.id.lm_img_5).setVisibility(View.VISIBLE);
            findViewById(R.id.lm_img_6).setVisibility(View.GONE);
        } else if (LauncherApp.get_headertiles() == 6) {
            findViewById(R.id.lm_img_4).setVisibility(View.VISIBLE);
            findViewById(R.id.lm_img_5).setVisibility(View.VISIBLE);
            findViewById(R.id.lm_img_6).setVisibility(View.VISIBLE);
        }
    }

    private void RefreshAd() {
        AdLoader.Builder builder = new AdLoader.Builder(this, LauncherApp.get_Admob_native_Id());
        builder.forNativeAd(unifiedNativeAd -> {
            if (nativeAds != null) {
                nativeAds.destroy();
            }
            nativeAds = unifiedNativeAd;
            NativeAdView adView = (NativeAdView) getLayoutInflater().inflate(R.layout.small_native, null);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            ((FrameLayout) findViewById(R.id.bottom_adsContainer)).removeAllViews();
            ((FrameLayout) findViewById(R.id.bottom_adsContainer)).addView(adView);
            findViewById(R.id.text_ads_gallry).setVisibility(View.GONE);
        });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                findViewById(R.id.text_ads_gallry).setVisibility(View.GONE);
            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    public static void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (nativeAds != null) {
            nativeAds.destroy();
        }
    }
}