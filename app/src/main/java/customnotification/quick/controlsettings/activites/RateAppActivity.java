package customnotification.quick.controlsettings.activites;

import static customnotification.quick.controlsettings.BuildConfig.APPLICATION_ID;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import customnotification.quick.controlsettings.R;

public class RateAppActivity extends Activity {
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rateus);
        initView();
        findViewById(R.id.txt_submit).setOnClickListener(v12 -> {
            if (((RatingBar) findViewById(R.id.ratingbar)).getRating() <= 3) {
                startActivity(new Intent(RateAppActivity.this, FeedbackActivity.class));
            } else {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://play.google.com/store/apps/details?id=" + APPLICATION_ID)));
            }
        });
    }

    private void initView() {
        getWindow().setStatusBarColor(getResources().getColor(R.color.rate_4));
        findViewById(R.id.btn_back).setOnClickListener(view -> onBackPressed());
        findViewById(R.id.txt_notnow).setOnClickListener(v1 -> onBackPressed());
        findViewById(R.id.ratingbar).setOnTouchListener((view, motionEvent) -> {
            int ratepos = (int) ((RatingBar) findViewById(R.id.ratingbar)).getRating();
            if (ratepos == 0 || ratepos == 1) {
                ((ImageView) findViewById(R.id.img_rateemo)).setImageResource(R.drawable.img_rate_01);
                findViewById(R.id.ratebg).setBackgroundColor(getResources().getColor(R.color.rate_1));
                getWindow().setStatusBarColor(getResources().getColor(R.color.rate_1));
                ((TextView) findViewById(R.id.txtfill)).setText("Terrible");
            } else if (ratepos == 2) {
                ((ImageView) findViewById(R.id.img_rateemo)).setImageResource(R.drawable.img_rate_02);
                findViewById(R.id.ratebg).setBackgroundColor(getResources().getColor(R.color.rate_2));
                getWindow().setStatusBarColor(getResources().getColor(R.color.rate_2));
                ((TextView) findViewById(R.id.txtfill)).setText("Bad");
            } else if (ratepos == 3) {
                ((ImageView) findViewById(R.id.img_rateemo)).setImageResource(R.drawable.img_rate_03);
                findViewById(R.id.ratebg).setBackgroundColor(getResources().getColor(R.color.rate_3));
                getWindow().setStatusBarColor(getResources().getColor(R.color.rate_3));
                ((TextView) findViewById(R.id.txtfill)).setText("Okay");
            } else if (ratepos == 4) {
                ((ImageView) findViewById(R.id.img_rateemo)).setImageResource(R.drawable.img_rate_04);
                findViewById(R.id.ratebg).setBackgroundColor(getResources().getColor(R.color.rate_4));
                getWindow().setStatusBarColor(getResources().getColor(R.color.rate_4));
                ((TextView) findViewById(R.id.txtfill)).setText("Good");
            } else if (ratepos == 5) {
                ((ImageView) findViewById(R.id.img_rateemo)).setImageResource(R.drawable.img_rate_05);
                findViewById(R.id.ratebg).setBackgroundColor(getResources().getColor(R.color.rate_5));
                getWindow().setStatusBarColor(getResources().getColor(R.color.rate_5));
                ((TextView) findViewById(R.id.txtfill)).setText("Amazing");
            }
            return false;
        });
    }
}