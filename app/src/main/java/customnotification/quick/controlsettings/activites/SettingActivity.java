package customnotification.quick.controlsettings.activites;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.material.appbar.AppBarLayout;

import customnotification.quick.controlsettings.BuildConfig;
import customnotification.quick.controlsettings.R;

public class SettingActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        InitView();
        Clicks();
    }

    public void InitView() {
        ((AppBarLayout) findViewById(R.id.app_bar)).addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            ((TextView) findViewById(R.id.txt_tit)).setTextColor(Color.argb(Math.abs(verticalOffset / 2), 0, 0, 0));
            ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(verticalOffset / 2, 0, 0, 0));
            ((ImageView) findViewById(R.id.img_top)).setAlpha(verticalOffset / 2);
            if (Math.abs(verticalOffset) <= 1) {
                ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(255, 0, 0, 0));
                ((ImageView) findViewById(R.id.img_top)).setAlpha(255);
            }
        });
    }

    public void Clicks() {
        ((TextView) findViewById(R.id.txt_version)).setText("Version:   " + BuildConfig.VERSION_NAME);
        findViewById(R.id.btn_back).setOnClickListener(view -> onBackPressed());
        findViewById(R.id.btn_howtouse).setOnClickListener(v -> {
            Intent intent = new Intent(SettingActivity.this, HowToActivity.class);
            intent.putExtra("howint", "btn");
            startActivity(intent);
        });
        findViewById(R.id.btn_invitefrn).setOnClickListener(v -> {
            try {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("text/plain");
                intent.putExtra("android.intent.extra.SUBJECT", "Custom Notification App");
                intent.putExtra("android.intent.extra.TEXT", "\nHey, Checkout this new app\nCustom Notification\n" + "https://play.google.com/store/apps/details?id=" + getPackageName());
                startActivity(Intent.createChooser(intent, "choose one"));
            } catch (Exception ignored) {
            }
        });
        findViewById(R.id.btn_rateapp).setOnClickListener(v -> startActivity(new Intent(SettingActivity.this, RateAppActivity.class)));
        findViewById(R.id.btn_feedapp).setOnClickListener(v -> startActivity(new Intent(SettingActivity.this, FeedbackActivity.class)));
        findViewById(R.id.btn_update).setOnClickListener(view -> startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()))));
        findViewById(R.id.btn_ppc).setOnClickListener(view -> {
            try {
                startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.privacyurl))), 15);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}