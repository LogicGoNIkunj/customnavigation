package customnotification.quick.controlsettings.activites;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.model.ApiInterface;
import customnotification.quick.controlsettings.model.AdsModel;
import customnotification.quick.controlsettings.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SplashActivity extends Activity {
    InterstitialAd mInterstitialAd;
    Handler h;
    boolean isActivityLeft;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        runOnUiThread(() -> setUpAnimation());
        LauncherApp.appOpenManager.isAdShow = true;
        isActivityLeft = false;
        MobileAds.initialize(this, initializationStatus -> {
        });
        h = new Handler();
        getAdsData();
        LoadInterstitial();
        initNext();
    }

    public void initNext() {
        h.postDelayed(() -> {
            if (Constants.checkSystemWritePermission(SplashActivity.this) & Constants.getNotif(SplashActivity.this) & LauncherApp.get_ServiceONOFF()) {
                startActivity(new Intent(this, HomeActivity.class));
                try {
                    if (mInterstitialAd != null && !isActivityLeft) {
                        mInterstitialAd.show(this);
                    }
                    LauncherApp.appOpenManager.isAdShow = true;
                } catch (Exception ignored) {
                }
                finish();
            } else {
                if (LauncherApp.get_firsthowto()) {
                    LauncherApp.set_firsthowto(false);
                    Intent intent = new Intent(this, HowToActivity.class);
                    intent.putExtra("howint", "start");
                    startActivity(intent);
                    finish();
                } else {
                    startActivity(new Intent(this, FPermissionActivity.class));
                    finish();
                }
            }
        }, 4500);
    }

    public void onPause() {
        super.onPause();
        this.isActivityLeft = true;
    }

    public void onResume() {
        super.onResume();
        this.isActivityLeft = false;
    }

    protected void onStop() {
        super.onStop();
        this.isActivityLeft = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.isActivityLeft = true;
        try {
            h.removeCallbacksAndMessages(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
    }

    public void LoadInterstitial() {
        InterstitialAd.load(this, LauncherApp.get_Admob_interstitial_Id(), new AdRequest.Builder().build(), new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        super.onAdDismissedFullScreenContent();
                        mInterstitialAd = null;
                        LauncherApp.appOpenManager.isAdShow = false;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                mInterstitialAd = null;
                LauncherApp.appOpenManager.isAdShow = false;
            }
        });
    }

    private void setUpAnimation() {
        RelativeLayout relativeLayout = new RelativeLayout(this);
        ProgressBar progressBar = new ProgressBar(this);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.MULTIPLY);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(100, 100);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        layoutParams.setMargins(0, 0, 0, 50);
        relativeLayout.addView(progressBar, layoutParams);
        setContentView(relativeLayout);
    }

    public void getAdsData() {
        Retrofit.Builder retrofit = new Retrofit.Builder().baseUrl(ApiInterface.adsAPI).addConverterFactory(GsonConverterFactory.create());
        retrofit.build().create(ApiInterface.class).getstorylist(50).enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(Call<AdsModel> call, Response<AdsModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            if (response.body().getData() != null) {
                                LauncherApp.set_AdsInt(response.body().getData().getPublisher_id());
                                if (response.body().getData().getPublishers() != null) {
                                    if (LauncherApp.get_AdsInt() == 1) {
                                        if (response.body().getData().getPublishers().getAdmob() != null) {
                                            if (response.body().getData().getPublishers().getAdmob().getAdmob_interstitial() != null) {
                                                LauncherApp.set_Admob_interstitial_Id(response.body().getData().getPublishers().getAdmob().getAdmob_interstitial().getAdmob_interstitial_id());
                                            } else {
                                                LauncherApp.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial_ad));
                                            }
                                            if (response.body().getData().getPublishers().getAdmob().getAdmob_native() != null) {
                                                LauncherApp.set_Admob_native_Id(response.body().getData().getPublishers().getAdmob().getAdmob_native().getAdmob_Native_id());
                                            } else {
                                                LauncherApp.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                                            }
                                            if (response.body().getData().getPublishers().getAdmob().getAdmob_banner() != null) {
                                                LauncherApp.set_Admob_banner_Id(response.body().getData().getPublishers().getAdmob().getAdmob_banner().getAdmob_banner_id());
                                            } else {
                                                LauncherApp.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                            }
                                            if (response.body().getData().getPublishers().getAdmob().getAdmob_open() != null) {
                                                LauncherApp.set_Admob_open_Id(response.body().getData().getPublishers().getAdmob().getAdmob_open().getAdmob_OpenAd_id());
                                                LauncherApp.set_Admob_openshowtime(response.body().getData().getPublishers().getAdmob().getAdmob_open().getAdmob_OpenAd_show_time());
                                            } else {
                                                LauncherApp.set_Admob_open_Id(getResources().getString(R.string.openad_id));
                                                LauncherApp.set_Admob_openshowtime(0);
                                            }
                                        }
                                    } else {
                                        LauncherApp.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial_ad));
                                        LauncherApp.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                                        LauncherApp.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                        LauncherApp.set_Admob_open_Id(getResources().getString(R.string.openad_id));
                                        LauncherApp.set_Admob_openshowtime(0);
                                    }
                                } else {
                                    LauncherApp.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial_ad));
                                    LauncherApp.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                                    LauncherApp.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                    LauncherApp.set_Admob_open_Id(getResources().getString(R.string.openad_id));
                                    LauncherApp.set_Admob_openshowtime(0);
                                }
                            } else {
                                LauncherApp.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial_ad));
                                LauncherApp.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                                LauncherApp.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                LauncherApp.set_Admob_open_Id(getResources().getString(R.string.openad_id));
                                LauncherApp.set_Admob_openshowtime(0);
                            }
                        } else {
                            LauncherApp.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial_ad));
                            LauncherApp.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                            LauncherApp.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                            LauncherApp.set_Admob_open_Id(getResources().getString(R.string.openad_id));
                            LauncherApp.set_Admob_openshowtime(0);
                        }
                    } else {
                        LauncherApp.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial_ad));
                        LauncherApp.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                        LauncherApp.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                    }
                } else {
                    LauncherApp.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial_ad));
                    LauncherApp.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                    LauncherApp.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                    LauncherApp.set_Admob_open_Id(getResources().getString(R.string.openad_id));
                    LauncherApp.set_Admob_openshowtime(0);
                }
            }

            @Override
            public void onFailure(Call<AdsModel> call, Throwable t) {
                LauncherApp.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial_ad));
                LauncherApp.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                LauncherApp.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                LauncherApp.set_Admob_open_Id(getResources().getString(R.string.openad_id));
                LauncherApp.set_Admob_openshowtime(0);
            }
        });
    }
}