package customnotification.quick.controlsettings.activites;

import static customnotification.quick.controlsettings.activites.HowToActivity.btmdone;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;

import customnotification.quick.controlsettings.R;

public class TutorialFragment extends Fragment {
    private int mPageNumber;

    public static TutorialFragment create(int pageNumber) {
        TutorialFragment fragment = new TutorialFragment();
        Bundle args = new Bundle();
        args.putInt("page", pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public TutorialFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPageNumber = getArguments().getInt("page");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.actihowtofrag, container, false);
        LottieAnimationView lottie = rootView.findViewById(R.id.lottie);
        switch (mPageNumber) {
            case 0:
                btmdone.setVisibility(View.INVISIBLE);
                lottie.setImageAssetsFolder("lottie/howto_01/images");
                lottie.setAnimation("lottie/howto_01/data.json");
                lottie.playAnimation();
                break;
            case 1:
                btmdone.setVisibility(View.INVISIBLE);
                lottie.setImageAssetsFolder("lottie/howto_02/images");
                lottie.setAnimation("lottie/howto_02/data.json");
                lottie.playAnimation();
                break;
            case 2:
                btmdone.setVisibility(View.INVISIBLE);
                lottie.setImageAssetsFolder("lottie/howto_03/images");
                lottie.setAnimation("lottie/howto_03/data.json");
                lottie.playAnimation();
                break;
            case 3:
                btmdone.setVisibility(View.VISIBLE);
                lottie.setImageAssetsFolder("lottie/howto_04/images");
                lottie.setAnimation("lottie/howto_04/data.json");
                lottie.playAnimation();
                break;
            default:
                break;
        }
        return rootView;
    }
}