package customnotification.quick.controlsettings.activites;

import static customnotification.quick.controlsettings.model.ApiInterface.BASE_URL;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.adaptar.ViewPagerAdapter;
import customnotification.quick.controlsettings.utils.HorizontalMarginItemDecoration;
import customnotification.quick.controlsettings.model.ApiInterface;
import customnotification.quick.controlsettings.model.WallpaperModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class View_WallpaperActivity extends AppCompatActivity {
    WallpaperManager wallpaperManager;
    WallpaperModel.Data.Datum stringUrl;
    Bitmap bitmap = null;
    ArrayList<WallpaperModel.Data.Datum> wallpaperArrayList = new ArrayList<>();
    private NativeAd nativeAds;
    ViewPagerAdapter viewPagerAdapter;
    ProgressDialog pd;
    boolean isImage = true;
    private InterstitialAd mInterstitialAd;
    boolean isActivity;
    FrameLayout bottom_adsContainer;
    ViewPager2 viewPagerMain;
    ImageView backgroundImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_wallpaper);
        isActivity = false;
        initView();
        LoadAds();
        refreshAd(this, bottom_adsContainer);
    }

    private void initView() {
        bottom_adsContainer = findViewById(R.id.bottom_adsContainer);
        viewPagerMain = findViewById(R.id.viewPagerMain);
        backgroundImage = findViewById(R.id.backgroundImage);
        String list = getIntent().getStringExtra("AllImages");
        wallpaperArrayList = new Gson().fromJson(list, new TypeToken<ArrayList<WallpaperModel.Data.Datum>>() {
        }.getType());
        int pos = getIntent().getIntExtra("imgUrl", -1);
        WallpaperModel.Data.Datum datum = wallpaperArrayList.get(pos);
        ArrayList<WallpaperModel.Data.Datum> arrayList = new ArrayList<>();
        int module = 4;
        for (int i = 0; i < wallpaperArrayList.size(); i++) {
            if (i != module) {
                arrayList.add(wallpaperArrayList.get(i));
            } else {
                arrayList.add(null);
                module = module + 5;
            }
        }
        wallpaperArrayList = arrayList;
        findViewById(R.id.backBtn).setOnClickListener(v -> onBackPressed());
        if (wallpaperArrayList.get(0).getWallpaperImage().endsWith(".mp4")) {
            isImage = false;
        }
        viewPagerAdapter = new ViewPagerAdapter(this, wallpaperArrayList, isImage, viewPagerMain);
        viewPagerMain.setOffscreenPageLimit(3);
        viewPagerMain.setAdapter(viewPagerAdapter);
        setUpViewPager();
        pos = wallpaperArrayList.indexOf(datum);
        if (pos != -1) {
            viewPagerMain.setCurrentItem(pos);
            stringUrl = viewPagerAdapter.getWallPaperList().get(pos);
        }
        wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
        findViewById(R.id.btn_sharewallpaper).setOnClickListener(view -> {
            pd = new ProgressDialog(this);
            pd.setCancelable(false);
            pd.setMessage("Downloading...");
            pd.show();
            stringUrl = viewPagerAdapter.getWallPaperList().get(viewPagerMain.getCurrentItem());
            new RetrieveFeedTask("share").execute(stringUrl.getWallpaperImage());
        });
        findViewById(R.id.btn_setwallpaper).setOnClickListener(v -> {
            pd = new ProgressDialog(this);
            pd.setCancelable(false);
            pd.setMessage("Wallpaper Set...");
            pd.show();
            new RetrieveFeedTask("wallpaper").execute(stringUrl.getWallpaperImage());
        });
    }

    private void setUpViewPager() {
        viewPagerMain.setOffscreenPageLimit(3);
        float nextItemVisiblePx = getResources().getDimension(R.dimen.next_item_visible);
        float currentItemHorizontalMarginPx = getResources().getDimension(R.dimen.horizontal_margin);
        float pageTranslationX = nextItemVisiblePx + currentItemHorizontalMarginPx;
        viewPagerMain.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                if (position != 0 && viewPagerAdapter.getWallPaperList().size() - 5 <= position) {
                    if (isImage) {
                        loadMore();
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (viewPagerAdapter.getWallPaperList().get(position) == null) {
                    backgroundImage.setImageResource(R.color.grey);
                    bottom_adsContainer.setVisibility(View.INVISIBLE);
                    findViewById(R.id.btn_sharewallpaper).setVisibility(View.GONE);
                    findViewById(R.id.btn_setwallpaper).setVisibility(View.GONE);
                } else {
                    findViewById(R.id.btn_sharewallpaper).setVisibility(View.VISIBLE);
                    findViewById(R.id.btn_setwallpaper).setVisibility(View.VISIBLE);
                    try {
                        viewPagerAdapter.notifyItemChanged(position - 1);
                        viewPagerAdapter.notifyItemChanged(position);
                        viewPagerAdapter.notifyItemChanged(position + 1);
                        stringUrl = viewPagerAdapter.getWallPaperList().get(position);
                        bottom_adsContainer.setVisibility(View.VISIBLE);
                        Animation fadeIn = new AlphaAnimation(0, 1);
                        fadeIn.setInterpolator(new DecelerateInterpolator());
                        fadeIn.setDuration(1000);
                        backgroundImage.startAnimation(fadeIn);
                        Glide.with(View_WallpaperActivity.this).load(viewPagerAdapter.getWallPaperList().get(viewPagerMain.getCurrentItem()).getWallpaperImage()).override(25, 25).into(backgroundImage);
                        ViewPager2.PageTransformer pageTransformer = (page, ddd) -> {
                            page.setTranslationX(-pageTranslationX * ddd);
                            page.setScaleY(1 - (0.10f * Math.abs(ddd)));
                        };
                        viewPagerMain.setPageTransformer(pageTransformer);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });
        viewPagerMain.addItemDecoration(new HorizontalMarginItemDecoration(this, R.dimen.horizontal_margin));
    }

    @SuppressLint("StaticFieldLeak")
    class RetrieveFeedTask extends AsyncTask<String, Void, Void> {
        String type;

        public RetrieveFeedTask(String type) {
            this.type = type;
        }

        @Override
        protected Void doInBackground(String... strings) {
            try {
                URL url = new URL(stringUrl.getWallpaperImage());
                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (IOException ignored) {
            }
            return null;
        }

        @SuppressLint({"ResourceAsColor", "NewApi"})
        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            if (pd.isShowing()) {
                pd.dismiss();
            }
            if (type.equalsIgnoreCase("wallpaper")) {
                try {
                    wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
                    wallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_SYSTEM);
                    Toast.makeText(View_WallpaperActivity.this, "Wallpaper set successfully!", Toast.LENGTH_SHORT).show();
                } catch (Exception ignored) {
                }
            } else if (type.equalsIgnoreCase("share")) {
                takeScreenshot(bitmap, type);
            }
        }
    }

    private void takeScreenshot(Bitmap bitmap, String type) {
        File file = null;
        if (Environment.getExternalStorageState().equals("mounted")) {
            File directory = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "Download" + File.separator + getResources().getString(R.string.app_name));
            if (!directory.exists()) {
                directory.mkdirs();
            }
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String frame = "Wallpaper" + n + ".png";
            file = new File(directory.getAbsolutePath() + "/" + frame);
        }
        try {
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        File finalFile = file;
        runOnUiThread(() -> {
            if (type.equalsIgnoreCase("share")) {
                pd.dismiss();
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("image/*");
                intent.putExtra("android.intent.extra.STREAM", FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".fileprovider", finalFile));
                intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + getPackageName());
                startActivity(Intent.createChooser(intent, "Share"));
            }
        });
    }

    private void loadMore() {
        LoadMoreDataList(W_WallpaperActivity.pageId);
        W_WallpaperActivity.pageId++;
    }

    void LoadMoreDataList(int pageId) {
        ApiInterface service = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build().create(ApiInterface.class);
        service.getWallpaperList(LauncherApp.MYSECRET, W_WallpaperActivity.company_id, pageId).enqueue(new Callback<WallpaperModel>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<WallpaperModel> call, @NonNull Response<WallpaperModel> response) {
                if (response.body() != null && response.body().getStatus() && response.isSuccessful()) {
                    try {
                        wallpaperArrayList = (ArrayList<WallpaperModel.Data.Datum>) response.body().getData().getData();
                        if (wallpaperArrayList != null && wallpaperArrayList.size() > 0) {
                            W_WallpaperActivity.wallpaperListUpdate.addAll(wallpaperArrayList);
                            W_WallpaperActivity.categoryAdapter.notifyDataSetChanged();
                            ArrayList<WallpaperModel.Data.Datum> arrayList = new ArrayList<>();
                            int module = 4;
                            for (int i = 0; i < wallpaperArrayList.size(); i++) {
                                if (i != module) {
                                    arrayList.add(wallpaperArrayList.get(i));
                                } else {
                                    arrayList.add(null);
                                    module = module + 5;
                                }
                            }
                            wallpaperArrayList = arrayList;
                            viewPagerAdapter.setWallPaperList(wallpaperArrayList);
                        }
                        W_WallpaperActivity.isLoading = false;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<WallpaperModel> call, @NonNull Throwable t) {
                Toast.makeText(View_WallpaperActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onPause() {
        super.onPause();
        this.isActivity = true;
    }

    public void onResume() {
        super.onResume();
        this.isActivity = false;
    }

    protected void onStop() {
        super.onStop();
        this.isActivity = true;
    }

    @Override
    protected void onDestroy() {
        if (nativeAds != null) {
            nativeAds.destroy();
        }
        super.onDestroy();
        wallpaperArrayList.clear();
        isActivity = true;
    }

    @Override
    public void onBackPressed() {
        try {
            if (mInterstitialAd != null && !isActivity) {
                mInterstitialAd.show(View_WallpaperActivity.this);
                LauncherApp.appOpenManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    public void LoadAds() {
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(this, LauncherApp.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        LauncherApp.appOpenManager.isAdShow = false;
                        mInterstitialAd = null;
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        LauncherApp.appOpenManager.isAdShow = false;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                        LauncherApp.appOpenManager.isAdShow = true;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                LauncherApp.appOpenManager.isAdShow = false;
            }
        });
    }

    @SuppressLint("MissingPermission")
    public void refreshAd(final Context context, final FrameLayout frameLayout) {
        AdLoader.Builder builder = new AdLoader.Builder(context, LauncherApp.get_Admob_native_Id());
        builder.forNativeAd(unifiedNativeAd -> {
            if (nativeAds != null) {
                nativeAds.destroy();
            }
            nativeAds = unifiedNativeAd;
            NativeAdView adView = (NativeAdView) LayoutInflater.from(context).inflate(R.layout.small_native, null);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    public static void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }
}