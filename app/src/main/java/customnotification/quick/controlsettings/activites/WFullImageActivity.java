package customnotification.quick.controlsettings.activites;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import customnotification.quick.controlsettings.R;

public class WFullImageActivity extends Activity {
    Bitmap bitmap;
    String stringExtra;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_full_screen_view);

        Clicks();
        stringExtra = getIntent().getStringExtra("image_name");
        if (stringExtra != null) {
            InputStream inputstream = null;
            try {
                inputstream = getAssets().open("wallpaper/" + stringExtra);
            } catch (IOException e) {
                e.printStackTrace();
            }
            bitmap = BitmapFactory.decodeStream(inputstream);
            ((ImageView) findViewById(R.id.imgFullscreen)).setImageBitmap(bitmap);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.msg_unknown_error), Toast.LENGTH_SHORT).show();
        }
    }

    public void Clicks() {
        findViewById(R.id.llDownloadWallpaper).setOnClickListener(view -> {
            if (checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == PackageManager.PERMISSION_GRANTED) {
                downloadWallpaer();
            } else {
                requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 100);
            }
        });
        findViewById(R.id.llShare).setOnClickListener(view -> {
            if (checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") == PackageManager.PERMISSION_GRANTED) {
                ShareImage();
            } else {
                requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 300);
            }
        });
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (i == 100) {
            if (iArr[0] == 0) {
                downloadWallpaer();
            } else {
                Toast.makeText(this, "Until you grant the permission, we can't save wallpaper", Toast.LENGTH_SHORT).show();
            }
        }
        if (i != 300) {
            return;
        }
        if (iArr[0] == 0) {
            ShareImage();
        } else {
            Toast.makeText(this, "Until you grant the permission, we can't share wallpaper", Toast.LENGTH_SHORT).show();
        }
    }

    public void ShareImage() {
        ProgressDialog pd = new ProgressDialog(this);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
        pd.show();
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "Wallpaper");
        if (!file.exists()) {
            file.mkdirs();
        }
        final File file2 = new File(file, "Wallpaper-" + new Random().nextInt(10000) + ".jpg");
        if (file2.exists()) {
            file2.delete();
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file2);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri uri = FileProvider.getUriForFile(WFullImageActivity.this, getPackageName() + ".fileprovider", file2);
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType(getMimeType(String.valueOf(uri)));
        share.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(share, "Share Image"));
        pd.dismiss();
    }

    public String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public void downloadWallpaer() {
        ProgressDialog pd = new ProgressDialog(this);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
        pd.show();
        new Thread(() -> {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    WallpaperManager.getInstance(WFullImageActivity.this).setBitmap(bitmap, null, true, WallpaperManager.FLAG_SYSTEM);
                    runOnUiThread(() -> Toast.makeText(this, "Wallpaper Set", Toast.LENGTH_SHORT).show());
                }
            } catch (Exception ignored) {
            }
            runOnUiThread(() -> pd.dismiss());
        }).start();
    }
}