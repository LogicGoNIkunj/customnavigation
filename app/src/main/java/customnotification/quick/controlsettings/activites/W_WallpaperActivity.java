package customnotification.quick.controlsettings.activites;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.adaptar.CategoryAdapter;
import customnotification.quick.controlsettings.adaptar.CategoryListAdapter;
import customnotification.quick.controlsettings.utils.ItemClick;
import customnotification.quick.controlsettings.model.ApiInterface;
import customnotification.quick.controlsettings.model.CategoryListModel;
import customnotification.quick.controlsettings.model.WallpaperModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class W_WallpaperActivity extends Activity implements ItemClick {
    ArrayList<CategoryListModel.Datum> categoryList = new ArrayList<>();
    ProgressDialog pd;
    ArrayList<WallpaperModel.Data.Datum> wallpaperList = new ArrayList<>();
    public static ArrayList<WallpaperModel.Data.Datum> wallpaperListUpdate = new ArrayList<>();
    public static int company_id;
    int totalImage = 1;
    public static int pageId;
    @SuppressLint("StaticFieldLeak")
    public static CategoryAdapter categoryAdapter;
    public static boolean isLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_w_category);
        InitView();
        CategoryList();
        getWallpaperList(35);
    }

    public void InitView(){
        pd = new ProgressDialog(this);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
        findViewById(R.id.btn_back).setOnClickListener(view -> onBackPressed());
        ((AppBarLayout) findViewById(R.id.app_bar)).addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            ((TextView) findViewById(R.id.txt_tit)).setTextColor(Color.argb(Math.abs(verticalOffset / 2), 0, 0, 0));
            ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(verticalOffset / 2, 0, 0, 0));
            ((ImageView) findViewById(R.id.img_top)).setAlpha(verticalOffset / 2);
            if (Math.abs(verticalOffset) <= 1) {
                ((TextView) findViewById(R.id.titletxt_big)).setTextColor(Color.argb(255, 0, 0, 0));
                ((ImageView) findViewById(R.id.img_top)).setAlpha(255);
            }
        });
    }

    public void CategoryList() {
        pd.show();
        new Retrofit.Builder().baseUrl(ApiInterface.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build().create(ApiInterface.class).getCategoryList(LauncherApp.MYSECRET).enqueue(new Callback<CategoryListModel>() {
            @Override
            public void onResponse(@NonNull Call<CategoryListModel> call, @NonNull Response<CategoryListModel> response) {
                if (pd.isShowing() && pd != null)
                    pd.dismiss();
                if (response.body() != null) {
                    if (response.body().getData() != null) {
                        categoryList = response.body().getData();
                        if (categoryList.size() > 0) {
                            ((RecyclerView) findViewById(R.id.recyclerCategories)).setLayoutManager(new LinearLayoutManager(W_WallpaperActivity.this, LinearLayoutManager.HORIZONTAL, true));
                            ((RecyclerView) findViewById(R.id.recyclerCategories)).setAdapter(new CategoryListAdapter(W_WallpaperActivity.this, categoryList, W_WallpaperActivity.this));
                            findViewById(R.id.tv_nodata).setVisibility(View.GONE);
                        } else {
                            findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
                        }
                        findViewById(R.id.tv_nodata).setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
                    }
                } else {
                    findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CategoryListModel> call, @NonNull Throwable t) {
                if (pd.isShowing() && pd != null)
                    pd.dismiss();
                findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
            }
        });
    }

    private void getWallpaperList(int company_id) {
        pd.show();
        ApiInterface service = new Retrofit.Builder().baseUrl(ApiInterface.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build().create(ApiInterface.class);
        service.getWallpaperList(LauncherApp.MYSECRET, company_id, 1).enqueue(new Callback<WallpaperModel>() {
            @Override
            public void onResponse(@NonNull Call<WallpaperModel> call, @NonNull Response<WallpaperModel> response) {
                if (pd.isShowing() && pd != null)
                    pd.dismiss();
                if (response.body() != null) {
                    totalImage = response.body().getData().getTotal();
                    wallpaperList = (ArrayList<WallpaperModel.Data.Datum>) response.body().getData().getData();
                    wallpaperListUpdate = wallpaperList;
                    findViewById(R.id.tv_nodata).setVisibility(View.GONE);
                } else {
                    findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
                    Toast.makeText(W_WallpaperActivity.this, "We're so sorry, something went wrong. Please try again later", Toast.LENGTH_SHORT).show();
                }
                categoryAdapter = new CategoryAdapter(W_WallpaperActivity.this, wallpaperListUpdate);
                ((RecyclerView) findViewById(R.id.recyclerView)).setLayoutManager(new GridLayoutManager(W_WallpaperActivity.this, 2));
                ((RecyclerView) findViewById(R.id.recyclerView)).setAdapter(categoryAdapter);
                pageId = 2;
                initScrollListener();
            }

            @Override
            public void onFailure(@NonNull Call<WallpaperModel> call, @NonNull Throwable t) {
                if (pd.isShowing() && pd != null)
                    pd.dismiss();
                findViewById(R.id.tv_nodata).setVisibility(View.VISIBLE);
            }
        });
    }

    private void initScrollListener() {
        ((RecyclerView) findViewById(R.id.recyclerView)).addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading && totalImage > wallpaperListUpdate.size()) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() > wallpaperListUpdate.size() - 5) {
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore() {
        findViewById(R.id.progress).setVisibility(View.VISIBLE);
        LoadMoreDataList(pageId);
        pageId++;
    }

    void LoadMoreDataList(int pageId) {
        ApiInterface service = new Retrofit.Builder().baseUrl(ApiInterface.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build().create(ApiInterface.class);
        service.getWallpaperList(LauncherApp.MYSECRET, company_id, pageId).enqueue(new Callback<WallpaperModel>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<WallpaperModel> call, @NonNull Response<WallpaperModel> response) {
                if (response.body() != null && response.body().getStatus() && response.isSuccessful()) {
                    try {
                        findViewById(R.id.progress).setVisibility(View.GONE);
                        wallpaperList = (ArrayList<WallpaperModel.Data.Datum>) response.body().getData().getData();
                        if (wallpaperList != null && wallpaperList.size() > 0) {
                            wallpaperListUpdate.addAll(wallpaperList);
                            categoryAdapter.notifyDataSetChanged();
                        }
                        isLoading = false;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<WallpaperModel> call, @NonNull Throwable t) {
                if (pd.isShowing() && pd != null)
                    pd.dismiss();
            }
        });
    }

    @Override
    public void Click(int position) {
        company_id = position;
        getWallpaperList(position);
    }
}