package customnotification.quick.controlsettings.adaptar;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;

import java.util.ArrayList;

import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.activites.View_WallpaperActivity;
import customnotification.quick.controlsettings.model.WallpaperModel;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    final Context context;
    final ArrayList<WallpaperModel.Data.Datum> wallpaperList;

    public CategoryAdapter(Context context, ArrayList<WallpaperModel.Data.Datum> wallpaperList) {
        this.context = context;
        this.wallpaperList = wallpaperList;
    }

    @NonNull
    public CategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.wallpaper_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder holder, int position) {
        Glide.with(context).load(wallpaperList.get(position).getWallpaper_thumb()).addListener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                holder.progressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(holder.wallpaperImage);

        holder.itemView.setOnClickListener(v -> {
            Intent i1 = new Intent(context, View_WallpaperActivity.class);
            i1.putExtra("imgUrl", position);
            i1.putExtra("AllImages", new Gson().toJson(wallpaperList));
            i1.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            context.startActivity(i1);
        });
    }

    @Override
    public int getItemCount() {
        return wallpaperList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return wallpaperList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView wallpaperImage;
        ProgressBar progressBar;

        public ViewHolder(View itemView) {
            super(itemView);
            wallpaperImage = itemView.findViewById(R.id.wallpaperImage);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
}