package customnotification.quick.controlsettings.adaptar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.utils.ItemClick;
import customnotification.quick.controlsettings.model.CategoryListModel;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {
    Context context;
    ArrayList<CategoryListModel.Datum> categoryList;
    ItemClick itemClick;
    public CategoryListAdapter(Context context, ArrayList<CategoryListModel.Datum> category, ItemClick itemClick) {
        this.context = context;
        this.categoryList = category;
        this.itemClick = itemClick;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.category_list_item, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.textView.setText(categoryList.get(position).getCategoryName());
        Glide.with(context).load(categoryList.get(position).getCategoryIcon()).into(holder.imageView);
        holder.itemView.setOnClickListener(v -> itemClick.Click(categoryList.get(position).getId()));
    }
    @Override
    public int getItemCount() {
        return categoryList.size();
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView textView;
        final ImageView imageView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.TVCategory);
            imageView = itemView.findViewById(R.id.Category);
        }
    }
}