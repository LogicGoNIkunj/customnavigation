package customnotification.quick.controlsettings.adaptar;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import java.util.ArrayList;

import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.services.NotificationListener;
import customnotification.quick.controlsettings.utils.Notification;
import de.hdodenhof.circleimageview.CircleImageView;

public class CustomNotificationAdapter extends RecyclerView.Adapter<CustomNotificationAdapter.ViewHolder> {
    private final Context mContext;
    private final NotificationListener notificationListener;
    private final ArrayList<Notification> notifications;

    public CustomNotificationAdapter(Context context, ArrayList<Notification> arrayList, NotificationListener notificationListener2) {
        this.mContext = context;
        this.notifications = arrayList;
        this.notificationListener = notificationListener2;
    }

    public int getItemCount() {
        ArrayList<Notification> arrayList = this.notifications;
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(this.mContext).inflate(R.layout.notification_list_items, viewGroup, false));
    }

    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        viewHolder.itemView.setOnLongClickListener(null);
        viewHolder.itemView.setLongClickable(false);
        Glide.with(mContext)
                .load(getPackageIcon(mContext, notifications.get(i).pack))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transition(new DrawableTransitionOptions().crossFade())
                .into(viewHolder.iv_icon);
//        try {
//            Drawable icon = mContext.getPackageManager().getApplicationIcon(notifications.get(i).pack);
//            if(icon !=null) {
//                viewHolder.iv_icon.setImageBitmap(((BitmapDrawable) icon).getBitmap());
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        viewHolder.tv_title.setText(this.notifications.get(i).tv_title);
        viewHolder.tv_title.setTag(this.notifications.get(i).pack);
        viewHolder.tv_text.setText(this.notifications.get(i).tv_text);
        if (this.notifications.get(i).senderIcon != null) {
            viewHolder.civ_largeIcon.setImageBitmap(this.notifications.get(i).senderIcon);
        } else {
            viewHolder.civ_largeIcon.setImageResource(0);
        }
        viewHolder.itemView.setOnClickListener(view -> {
            try {
                if (notifications.get(viewHolder.getAdapterPosition()).pendingIntent != null) {
                    notifications.get(viewHolder.getAdapterPosition()).pendingIntent.send();
                    notificationListener.onItemClicked(notifications.get(i));
                }
                if (viewHolder.getAdapterPosition() < notifications.size() && !notifications.get(viewHolder.getAdapterPosition()).pack.equals("android") && !notifications.get(viewHolder.getAdapterPosition()).pack.equals("com.android.systemui") && !notifications.get(viewHolder.getAdapterPosition()).pack.equals("com.android.settings")) {
                    notifications.remove(viewHolder.getAdapterPosition());
                    notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public static Drawable getPackageIcon(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        try {
            return manager.getApplicationIcon(packageName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return context.getResources().getDrawable(R.drawable.ic_default_app);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        final CircleImageView civ_largeIcon;
        final ImageView iv_icon;
        final TextView tv_text;
        public final TextView tv_title;

        public ViewHolder(View view) {
            super(view);
            iv_icon = view.findViewById(R.id.iv_icon);
            this.tv_title = view.findViewById(R.id.tv_title);
            this.tv_text = view.findViewById(R.id.tv_text);
            this.civ_largeIcon = view.findViewById(R.id.civ_senderIcon);
        }
    }
}
