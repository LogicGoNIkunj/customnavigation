package customnotification.quick.controlsettings.adaptar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;

import java.util.ArrayList;
import java.util.Objects;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.model.WallpaperModel;

public class ViewPagerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    final Context context;
    final ViewPager2 viewPagerMain;
    public void setWallPaperList(ArrayList<WallpaperModel.Data.Datum> wallPaperList) {
        this.wallPaperList.addAll(wallPaperList);
        notifyItemRangeChanged(0, wallPaperList.size());
    }

    public ArrayList<WallpaperModel.Data.Datum> getWallPaperList() {
        return wallPaperList;
    }

    final ArrayList<WallpaperModel.Data.Datum> wallPaperList;
    Bitmap bitmap;
    final boolean isImage;
    private NativeAd nativeAd;

    public ViewPagerAdapter(Context context, ArrayList<WallpaperModel.Data.Datum> wallPaperList, boolean isImage, ViewPager2 viewPagerMain) {
        this.context = context;
        this.wallPaperList = wallPaperList;
        this.isImage = isImage;
        this.viewPagerMain = viewPagerMain;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return new AdHolder(LayoutInflater.from(context).inflate(R.layout.viewpager_item, parent, false));
        } else {
            return new ImageHolder(LayoutInflater.from(context).inflate(R.layout.viewpager_item, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AdHolder) {
            refreshAd(((AdHolder) holder).frameLayout);
        } else if (holder instanceof ImageHolder) {
            if (isImage) {
                Glide.with(context).asBitmap().load(wallPaperList.get(position).getWallpaperImage()).listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        ((ImageHolder) holder).progressBar.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        ((ImageHolder) holder).progressBar.setVisibility(View.GONE);
                        bitmap = resource;
                        return false;
                    }
                }).into(((ImageHolder) holder).imageViewMain);
            } else {
                Glide.with(context).asBitmap().load(wallPaperList.get(position).getWallpaperImage()).listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        ((ImageHolder) holder).progressBar.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        ((ImageHolder) holder).progressBar.setVisibility(View.GONE);
                        bitmap = resource;
                        return false;
                    }
                }).into(((ImageHolder) holder).imageViewMain);
            }
        }
    }

    @Override
    public int getItemCount() {
        return wallPaperList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (wallPaperList.get(position) == null) {
            return 0;
        } else {
            return 1;
        }
    }

    public static class ImageHolder extends RecyclerView.ViewHolder {
        ImageView imageViewMain;
        ProgressBar progressBar;
        FrameLayout frameLayout;

        public ImageHolder(@NonNull View view) {
            super(view);
            imageViewMain = view.findViewById(R.id.imageViewMain);
            progressBar = view.findViewById(R.id.progressBar);
            frameLayout = view.findViewById(R.id.frameLayout);
        }
    }

    public static class AdHolder extends RecyclerView.ViewHolder {
        ImageView imageViewMain;
        ProgressBar progressBar;
        FrameLayout frameLayout;

        public AdHolder(@NonNull View view) {
            super(view);
            imageViewMain = view.findViewById(R.id.imageViewMain);
            progressBar = view.findViewById(R.id.progressBar);
            frameLayout = view.findViewById(R.id.frameLayout);
        }
    }

    @SuppressLint("MissingPermission")
    private void refreshAd(FrameLayout frameLayout) {
        AdLoader.Builder builder = new AdLoader.Builder(context, LauncherApp.get_Admob_native_Id());
        builder.forNativeAd(unifiedNativeAd -> {
            if (nativeAd != null) {
                nativeAd.destroy();
            }
            nativeAd = unifiedNativeAd;
            @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) LayoutInflater.from(context).inflate(R.layout.new_native, null, false);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
            frameLayout.setVisibility(View.VISIBLE);
        });

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                frameLayout.setVisibility(View.INVISIBLE);
            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }
}