package customnotification.quick.controlsettings.largetile;

public class LongItems {
    int thumb;
    String name;
    boolean activeornot;

    public LongItems(int thumb, String name, boolean activeornot) {
        this.thumb = thumb;
        this.name = name;
        this.activeornot = activeornot;
    }

    public void setThumb(int thumb) {
        this.thumb = thumb;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setActiveornot(boolean activeornot) {
        this.activeornot = activeornot;
    }

    public int getThumb() {
        return this.thumb;
    }

    public String getname() {
        return this.name;
    }

    public boolean getActiveorNot() {
        return this.activeornot;
    }
}