package customnotification.quick.controlsettings.largetile;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import java.util.List;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.services.ClicksInter;
import customnotification.quick.controlsettings.services.MAccessibilityService;
import customnotification.quick.controlsettings.utils.Utils;
import customnotification.quick.controlsettings.views.PanelView;

public class LongItemsAdapter extends Adapter<LongItemsAdapter.ViewHolder> {
    final Context activity;
    final List<LongItems> list;
    final ClicksInter clicksInter;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView img_iconimg;
        final LinearLayout longitem;
        final TextView tv_icontxt;

        public ViewHolder(View itemView) {
            super(itemView);
            img_iconimg = itemView.findViewById(R.id.img_iconimg);
            longitem = itemView.findViewById(R.id.longitem);
            tv_icontxt = itemView.findViewById(R.id.tv_icontxt);
        }
    }

    public LongItemsAdapter(Context context, List<LongItems> list, ClicksInter flashLightInter) {
        this.list = list;
        this.activity = context;
        this.clicksInter = flashLightInter;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(this.activity).inflate(R.layout.item_largemenu, parent, false));
    }

    @SuppressLint("WrongConstant")
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.img_iconimg.setImageResource(list.get(position).getThumb());
        holder.tv_icontxt.setText(list.get(position).getname());
        holder.img_iconimg.setBackground(activity.getDrawable(LauncherApp.get_IconShapethumb()));
        holder.tv_icontxt.setTextColor(Utils.getAlphaColor(LauncherApp.get_TxtColor(), LauncherApp.get_TxtColoralpha()));
        if (list.get(position).getActiveorNot()) {
            try {
                holder.img_iconimg.getDrawable().setTint(activity.getResources().getColor(R.color.on_button));
                holder.img_iconimg.getBackground().setTint(Utils.getAlphaColor(LauncherApp.get_ActiveTileColor(), LauncherApp.get_ActiveTilealpha()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            holder.img_iconimg.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_TxtColor(), LauncherApp.get_TxtColoralpha()));
            holder.img_iconimg.getBackground().setTint(Color.parseColor("#48FFFFFF"));
        }
        holder.longitem.setOnClickListener(v -> {
            switch (list.get(position).getname()) {
                case "Wi-fi":
                    if (Build.VERSION.SDK_INT < 29) {
                        WifiManager wifiManager = (WifiManager) activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                        wifiManager.setWifiEnabled(!wifiManager.isWifiEnabled());
                    } else {
                        Intent intent = new Intent("android.settings.panel.action.WIFI");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intent);
                        PanelView.onPanelItemClickListner.onItemClicked(false, true);
                    }
                    break;
                case "Mobile Data":
                    if (Build.VERSION.SDK_INT < 29) {
                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
                        activity.startActivity(intent);
                        PanelView.onPanelItemClickListner.onItemClicked(false, true);
                        activity.startActivity(new Intent("android.settings.DATA_ROAMING_SETTINGS"));
                        PanelView.onPanelItemClickListner.onItemClicked(false, true);
                        return;
                    }
                    Intent intent2 = new Intent("android.settings.panel.action.INTERNET_CONNECTIVITY");
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent2);
                    PanelView.onPanelItemClickListner.onItemClicked(false, true);
                    break;
                case "Flashlight":
                    clicksInter.FlashClick();
                    break;
                case "Sound":
                case "Mute":
                case "Vibrate":
                    clicksInter.SoundClick();
                    break;
                case "Bluetooth":
                    clicksInter.BluetoothClick();
                    break;
                case "Brightness":
                    clicksInter.BrightnessClick();
                    break;
                case "Auto-rotate":
                    clicksInter.AutoRotateClick();
                    break;
                case "Airplane mode":
                    clicksInter.AirPlanClick();
                    break;
                case "Location":
                    Intent intent = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                    PanelView.onPanelItemClickListner.onItemClicked(false, true);
                    break;
                case "Home":
                    try {
                        Intent intentaa = new Intent(activity, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 13);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            activity.startForegroundService(intentaa);
                        } else {
                            activity.startService(intentaa);
                        }
                    } catch (Throwable ignored) {
                    }
                    PanelView.onPanelItemClickListner.onItemClicked(false, true);
                    break;
                case "Battery Saver":
                    clicksInter.PowerSaverModeClick();
                    break;
                case "Do Not Disturb":
                    clicksInter.DoNotDisturbClick();
                    break;
                case "Hotspot":
                    clicksInter.HotSpotClick();
                    break;
                case "Dark Theme":
                    try {
                        Intent intentaa = new Intent("android.settings.DISPLAY_SETTINGS");
                        intentaa.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intentaa);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                    PanelView.onPanelItemClickListner.onItemClicked(false, true);
                    break;
                case "Lock Screen":
                    try {
                        Intent intentaa = new Intent(activity, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 12);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            activity.startForegroundService(intentaa);
                        } else {
                            activity.startService(intentaa);
                        }
                    } catch (Throwable ignored) {
                    }
                    PanelView.onPanelItemClickListner.onItemClicked(false, true);
                    break;

                case "Open System":
                    try {
                        Intent intentaa = new Intent(activity, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 11);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            activity.startForegroundService(intentaa);
                        } else {
                            activity.startService(intentaa);
                        }
                    } catch (Throwable ignored) {
                    }
                    PanelView.onPanelItemClickListner.onItemClicked(false, true);
                    break;
            }
        });
    }

    public int getItemCount() {
        return this.list.size();
    }
}