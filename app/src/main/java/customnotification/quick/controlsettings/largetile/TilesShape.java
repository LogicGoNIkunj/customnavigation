package customnotification.quick.controlsettings.largetile;

public class TilesShape {
    final int thumb;
    final String name;

    public TilesShape(int thumb, String name) {
        this.thumb = thumb;
        this.name = name;
    }

    public int getThumb() {
        return this.thumb;
    }

    public String getname() {
        return this.name;
    }
}