package customnotification.quick.controlsettings.largetile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.services.MAccessibilityService;

public class TilesShapeAdapter extends Adapter<TilesShapeAdapter.ViewHolder> {
    final Context activity;
    final ArrayList<TilesShape> list;
    AlertDialog Dialog_qkIconShape;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView img_shapeicon;
        final RelativeLayout shapeitem;
        final TextView tv_shapename;
        final RadioButton radiobtn;

        public ViewHolder(View itemView) {
            super(itemView);
            img_shapeicon = itemView.findViewById(R.id.img_shapeicon);
            shapeitem = itemView.findViewById(R.id.shapeitem);
            tv_shapename = itemView.findViewById(R.id.tv_shapename);
            radiobtn = itemView.findViewById(R.id.radiobtn);
        }
    }

    public TilesShapeAdapter(Context context, ArrayList<TilesShape> list, AlertDialog Dialog) {
        this.list = list;
        this.activity = context;
        this.Dialog_qkIconShape = Dialog;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(this.activity).inflate(R.layout.item_tilesshapes, parent, false));
    }

    @SuppressLint("WrongConstant")
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Glide.with(activity).load(list.get(position).getThumb()).into(holder.img_shapeicon);
        holder.tv_shapename.setText(list.get(position).getname());
        holder.radiobtn.setChecked(list.get(position).getname().equals(LauncherApp.get_qsIconShapeName()));
        holder.shapeitem.setOnClickListener(v -> {
            LauncherApp.set_qsIconShapeName(list.get(position).getname());
            LauncherApp.set_IconShapethumb(list.get(position).getThumb());
            notifyDataSetChanged();
            if (LauncherApp.get_ServiceONOFF()) {
                try {
                    Intent intentaa = new Intent(activity, MAccessibilityService.class).putExtra("com.control.center.intent.MESSAGE", 1);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        activity.startForegroundService(intentaa);
                    } else {
                        activity.startService(intentaa);
                    }
                } catch (Throwable ignored) {
                }
            }
            if (Dialog_qkIconShape != null) {
                Dialog_qkIconShape.dismiss();
            }
        });
    }

    public int getItemCount() {
        return this.list.size();
    }
}