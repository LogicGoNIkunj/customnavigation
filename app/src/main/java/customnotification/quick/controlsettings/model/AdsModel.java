package customnotification.quick.controlsettings.model;

import com.google.gson.annotations.SerializedName;

public class AdsModel {
    boolean status;
    Data data;

    public boolean isStatus() {
        return status;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        int publisher_id;
        publishers publishers;

        public int getPublisher_id() {
            return publisher_id;
        }

        public Data.publishers getPublishers() {
            return publishers;
        }

        public class publishers {
            @SerializedName("admob")
            Admob admob;

            public Admob getAdmob() {
                return admob;
            }

            public class Admob {
                @SerializedName("banner")
                Admob_Banner banner;
                @SerializedName("interstitial")
                Admob_Interstitial admob_interstitial;
                @SerializedName("native")
                Admob_Native admob_native;
                @SerializedName("open")
                Admob_OpenAd admob_open;

                public Admob_OpenAd getAdmob_open() {
                    return admob_open;
                }

                public Admob_Banner getAdmob_banner() {
                    return banner;
                }

                public Admob_Interstitial getAdmob_interstitial() {
                    return admob_interstitial;
                }

                public Admob_Native getAdmob_native() {
                    return admob_native;
                }

                public class Admob_Banner {
                    @SerializedName("id")
                    String admob_banner_id;

                    public String getAdmob_banner_id() {
                        return admob_banner_id;
                    }
                }

                public class Admob_Interstitial {
                    @SerializedName("id")
                    String admob_interstitial_id;

                    public String getAdmob_interstitial_id() {
                        return admob_interstitial_id;
                    }
                }

                public class Admob_Native {
                    @SerializedName("id")
                    String admob_Native_id;

                    public String getAdmob_Native_id() {
                        return admob_Native_id;
                    }
                }
                public class Admob_OpenAd {
                    @SerializedName("id")
                    String admob_OpenAd_id;
                    @SerializedName("show_time")
                    int admob_OpenAd_show_time;

                    public String getAdmob_OpenAd_id() {
                        return admob_OpenAd_id;
                    }

                    public int getAdmob_OpenAd_show_time() {
                        return admob_OpenAd_show_time;
                    }
                }
            }
        }
    }
}