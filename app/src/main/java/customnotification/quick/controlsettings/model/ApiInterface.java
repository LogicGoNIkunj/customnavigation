package customnotification.quick.controlsettings.model;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    //    ads URL
    String adsAPI = "https://stage-ads.punchapp.in/api/";
//    String adsAPI = "https://smartadz.in/api/";

    @FormUrlEncoded
    @POST("get-ads-list")
    Call<AdsModel> getstorylist(@Field("app_id") int IntVal);



    //Game Url
    String GameAPI = "https://smartadz.in/api/";
    @GET("qureka-ad")
    Call<Model_Playdata> GetData();





    //Wallpaper URl
    String BASE_URL = "http://wallpaper.punchapp.in/";
    @GET("api/category")
    Call<CategoryListModel> getCategoryList(@Header("AuthorizationKey") String token);
    @FormUrlEncoded
    @POST("api/wallpapers/")
    Call<WallpaperModel> getWallpaperList(@Header("AuthorizationKey") String token, @Field("category_id") int category_id, @Query("page") int ID);




    //feedback
    @FormUrlEncoded
    @POST("feedback")
    Call<FeedbackModel> sendfeedback(@Field("app_name") String app_name, @Field("package_name") String package_name, @Field("title") String title, @Field("description") String description, @Field("device_name") String device_name, @Field("android_version") String android_version, @Field("version") String version);

}