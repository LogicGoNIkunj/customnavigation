package customnotification.quick.controlsettings.model;

import java.util.ArrayList;

public class CategoryListModel {
    private final ArrayList<Datum> data = new ArrayList<>();

    public ArrayList<Datum> getData() {
        return data;
    }

    public static class Datum {
        Integer id;
        String category_name;
        String category_icon;

        public Integer getId() {
            return id;
        }

        public String getCategoryName() {
            return category_name;
        }

        public String getCategoryIcon() {
            return category_icon;
        }
    }
}