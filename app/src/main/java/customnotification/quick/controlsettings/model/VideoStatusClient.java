package customnotification.quick.controlsettings.model;

import customnotification.quick.controlsettings.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VideoStatusClient {
    public static String BASE_URL_Feedback = "http://punchapp.in/api/";
    static Retrofit retrofitfeedback;

    public static Retrofit getClientfeedback() {
        if (retrofitfeedback == null) {
            retrofitfeedback = new Retrofit.Builder().baseUrl(BASE_URL_Feedback).client(new OkHttpClient.Builder().addInterceptor(chain -> chain.proceed(chain.request().newBuilder().addHeader("Authorization", BuildConfig.APPLICATION_ID).addHeader("content-type", "application/json").build())).connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build()).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofitfeedback;
    }
}