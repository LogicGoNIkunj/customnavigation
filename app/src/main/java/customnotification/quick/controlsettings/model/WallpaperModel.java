package customnotification.quick.controlsettings.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WallpaperModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getStatus() {
        return status;
    }

    public Data getData() {
        return data;
    }

    public static class Data {

        @SerializedName("data")
        @Expose
        private final List<Datum> data = null;
        @SerializedName("total")
        @Expose
        private Integer total;

        public List<Datum> getData() {
            return data;
        }

        public Integer getTotal() {
            return total;
        }

        public static class Datum {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("wallpaper_image")
            @Expose
            private String wallpaperImage;

            @SerializedName("thumb")
            @Expose
            private String wallpaper_thumb;

            public String getWallpaper_thumb() {
                return wallpaper_thumb;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getWallpaperImage() {
                return wallpaperImage;
            }
        }
    }
}