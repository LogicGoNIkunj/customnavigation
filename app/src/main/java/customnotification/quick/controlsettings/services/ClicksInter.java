package customnotification.quick.controlsettings.services;

public interface ClicksInter {
    void FlashClick();

    void SoundClick();

    void BluetoothClick();

    void BrightnessClick();

    void AutoRotateClick();

    void AirPlanClick();

    void HotSpotClick();

    void PowerSaverModeClick();

    void DoNotDisturbClick();
}