package customnotification.quick.controlsettings.services;

import android.accessibilityservice.AccessibilityService;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.app.NotificationCompat;
import androidx.core.view.ViewCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.List;
import java.util.Objects;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.utils.DHANVINE_Utils;
import customnotification.quick.controlsettings.utils.Utils;
import customnotification.quick.controlsettings.views.PanelView;

public class MAccessibilityService extends AccessibilityService {
    static int statusBarHeight;
    private LinearLayout controlPanelSubContainer, haderlayout, llbrightness;
    WindowManager.LayoutParams localLayoutParams;
    Context mContext;
    PanelView mPanelView;
    WindowManager manager;
    public final int minStatusBarHeight = 30;
    LinearLayout panelView;
    View statusBarParentView;
    View statusBarView;
    private BroadcastReceiver mReceiver;
    public Vibrator vibrator;
    float change;
    //    btm
    public static int btmMIN_THRESH_HOLD = 0;
    public static int btmX_THRESH_HOLD = 0;
    public static int btmY_THRESH_HOLD = 0;
    public static boolean btmlimited = false;
    public ImageView btmimNavigationBar;
    public View btmnavigationView;
    public WindowManager.LayoutParams btmparams;
    public WindowManager btmwindowManager;
    private FrameLayout.LayoutParams btmparamsaa;
    //    left
    public static long LeftLIMITED_MILESTONE = 0;
    public static int LeftMIN_THRESH_HOLD = 0;
    public static int LeftX_THRESH_HOLD = 0;
    public static int LeftY_THRESH_HOLD = 0;
    public static boolean Leftlimited = false;
    public static boolean LeftpointMoved = false;
    public ImageView LeftimNavigationBar;
    public View LeftnavigationView;
    public WindowManager.LayoutParams Leftparams;
    public WindowManager LeftwindowManager;
    private FrameLayout.LayoutParams Leftparamsaa;
    //    Right
    public static long RightLIMITED_MILESTONE = 0;
    public static int RightMIN_THRESH_HOLD = 0;
    public static int RightX_THRESH_HOLD = 0;
    public static int RightY_THRESH_HOLD = 0;
    public static boolean RightconfirmLongtap = false;
    public static boolean Rightlimited = false;
    public static boolean RightpointMoved = false;
    public ImageView RightimNavigationBar;
    public View RightnavigationView;
    public WindowManager.LayoutParams Rightparams;
    public WindowManager RightwindowManager;
    private FrameLayout.LayoutParams Rightparamsaa;


    //    touch event
    FrameLayout framede;
    int biglaymaxheight, frameheight;
    int frameheightaa;
    private float pretouchY = 0f;
    private int preheight = 0;


    final BroadcastReceiver powerMenuReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (!performGlobalAction(intent.getIntExtra("action", -1))) {
                Toast.makeText(context, "Not supported", Toast.LENGTH_SHORT).show();
            }
        }
    };

    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
    }

    public void onInterrupt() {
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
    }

    @SuppressLint("WrongConstant")
    public void onServiceConnected() {
        mContext = this;
        manager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        int i = Build.VERSION.SDK_INT;
        localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.type = i > 21 ? WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY : WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = 8913704;
        localLayoutParams.width = -1;
        localLayoutParams.height = (int) (((float) minStatusBarHeight) * getResources().getDisplayMetrics().scaledDensity);
        localLayoutParams.format = -3;
        statusBarView = LayoutInflater.from(this).inflate(R.layout.status_bar, null);
        statusBarParentView = statusBarView.findViewById(R.id.statusbar_parent);
        mPanelView = new PanelView(statusBarView, mContext, (z, bool) -> {
            MAccessibilityService.this.showHidePanel(z, bool);
            if (mPanelView.notificationList.size() != 0) {
                statusBarView.findViewById(R.id.txtclear).setVisibility(View.VISIBLE);
            } else {
                statusBarView.findViewById(R.id.txtclear).setVisibility(View.GONE);
            }
        });
        mPanelView.initView();
        panelView = statusBarView.findViewById(R.id.panelParentView);
        framede = statusBarView.findViewById(R.id.framede);
        controlPanelSubContainer = statusBarView.findViewById(R.id.controlPanelSubContainer);
        llbrightness = statusBarView.findViewById(R.id.llbrightness);
        haderlayout = statusBarView.findViewById(R.id.haderlayout);
        localLayoutParams.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE;
        if (i >= 28) {
            localLayoutParams.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        try {
            manager.addView(statusBarView, localLayoutParams);
        } catch (Exception unused) {
            Toast.makeText(this, "Unfortunately something didn't work. Please try again or contact the developer.", Toast.LENGTH_LONG).show();
        }
        getQSIconList();
        super.onServiceConnected();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.action.ACTION_UPDATE_NAVIGATION_SIZE");
        intentFilter.addAction("com.action.ACTION_UPDATE_NAVIGATION_COLOR");
        intentFilter.addAction("com.action.ACTION_SHOW_NAVIGATION_BAR");
        intentFilter.addAction("com.action.ACTION_HIDE_NAVIGATION_BAR");
        mReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                char c;
                String str = Objects.requireNonNull(intent.getAction());
                switch (str.hashCode()) {
                    case -1221829872:
                        if (str.equals("com.action.ACTION_UPDATE_NAVIGATION_COLOR")) {
                            c = 3;
                            break;
                        }
                    case -920821273:
                        if (str.equals("com.action.ACTION_HIDE_NAVIGATION_BAR")) {
                            c = 1;
                            break;
                        }
                    case -593131884:
                        if (str.equals("com.action.ACTION_UPDATE_NAVIGATION_SIZE")) {
                            c = 2;
                            break;
                        }
                    case 778819276:
                        if (str.equals("com.action.ACTION_SHOW_NAVIGATION_BAR")) {
                            c = 0;
                            break;
                        }
                    default:
                        c = 65535;
                        break;
                }
                switch (c) {
                    case 0:
                        if (LauncherApp.get_TrigerVal() == 0) {
                            if (LauncherApp.get_btmService()) {
                                btminitLayoutParams();
                                btmshowNavigationBar();
                                btminitAndDeclareNavigation(btmnavigationView, mContext);
                            }
                        } else if (LauncherApp.get_TrigerVal() == 1) {
                            if (LauncherApp.get_LeftService()) {
                                LeftinitLayoutParams();
                                LeftshowNavigationBar();
                                LeftinitAndDeclareNavigation(LeftnavigationView, mContext);
                            }
                        } else if (LauncherApp.get_TrigerVal() == 2) {
                            if (LauncherApp.get_RightService()) {
                                RightinitLayoutParams();
                                RightshowNavigationBar();
                                RightinitAndDeclareNavigation(RightnavigationView, mContext);
                            }
                        }
                        return;
                    case 1:
                        try {
                            if (LauncherApp.get_TrigerVal() == 0) {
                                if (LauncherApp.get_btmService()) {
                                    LauncherApp.set_btmService(false);
                                    btmwindowManager.removeViewImmediate(btmnavigationView);
                                }
                            } else if (LauncherApp.get_TrigerVal() == 1) {
                                if (LauncherApp.get_LeftService()) {
                                    LauncherApp.set_LeftService(false);
                                    LeftwindowManager.removeViewImmediate(LeftnavigationView);
                                }
                            } else if (LauncherApp.get_TrigerVal() == 2) {
                                if (LauncherApp.get_RightService()) {
                                    LauncherApp.set_RightService(false);
                                    RightwindowManager.removeViewImmediate(RightnavigationView);
                                }
                            }
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }
                    case 2:
                        try {
                            if (LauncherApp.get_TrigerVal() == 0) {
                                if (LauncherApp.get_btmService()) {
                                    btmimNavigationBar.getLayoutParams().width = LauncherApp.get_BarWith();
                                    btmimNavigationBar.getLayoutParams().height = LauncherApp.get_BarHeight();
                                    int midval = Resources.getSystem().getDisplayMetrics().widthPixels;
                                    btmparamsaa.setMarginEnd(LauncherApp.get_BarPosition());
                                    int finwid = LauncherApp.get_BarPosition() - midval;
                                    if (finwid == 0) {
                                        LauncherApp.set_BarMinvat(LauncherApp.get_BarPosition());
                                    }
                                    btmparamsaa.setMarginStart(finwid);
                                    btmimNavigationBar.setLayoutParams(btmparamsaa);
                                }
                            } else if (LauncherApp.get_TrigerVal() == 1) {
                                if (LauncherApp.get_LeftService()) {
                                    LeftimNavigationBar.getLayoutParams().width = LauncherApp.get_LeftBarWith();
                                    LeftimNavigationBar.getLayoutParams().height = 200;
                                    if (0 < LauncherApp.get_LeftBarPosition()) {
                                        Leftparamsaa.setMargins(0, LauncherApp.get_LeftBarPosition(), 0, 0);
                                    } else {
                                        Leftparamsaa.setMargins(0, 0, 0, Math.abs(LauncherApp.get_LeftBarPosition()));
                                    }
                                    LeftimNavigationBar.setLayoutParams(Leftparamsaa);
                                }
                            } else if (LauncherApp.get_TrigerVal() == 2) {
                                if (LauncherApp.get_RightService()) {
                                    RightimNavigationBar.getLayoutParams().width = LauncherApp.get_RightBarWith();
                                    RightimNavigationBar.getLayoutParams().height = 200;
                                    if (0 < LauncherApp.get_RightBarPosition()) {
                                        Rightparamsaa.setMargins(0, LauncherApp.get_RightBarPosition(), 0, 0);
                                    } else {
                                        Rightparamsaa.setMargins(0, 0, 0, Math.abs(LauncherApp.get_RightBarPosition()));
                                    }
                                    RightimNavigationBar.setLayoutParams(Rightparamsaa);
                                }
                            }
                            RightimNavigationBar.requestLayout();
                            return;
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            return;
                        }
                    case 3:
                        try {
                            if (LauncherApp.get_TrigerVal() == 0) {
                                if (LauncherApp.get_btmService()) {
                                    btmimNavigationBar.setColorFilter(LauncherApp.getbtmBarColorAlpha(), PorterDuff.Mode.SRC_IN);
                                    btmimNavigationBar.requestLayout();
                                }
                            } else if (LauncherApp.get_TrigerVal() == 1) {
                                if (LauncherApp.get_LeftService()) {
                                    LeftimNavigationBar.setColorFilter(LauncherApp.getLeftBarColorAlpha(), PorterDuff.Mode.SRC_IN);
                                    LeftimNavigationBar.requestLayout();
                                }
                            } else if (LauncherApp.get_TrigerVal() == 2) {
                                if (LauncherApp.get_RightService()) {
                                    RightimNavigationBar.setColorFilter(LauncherApp.getRightBarColorAlpha(), PorterDuff.Mode.SRC_IN);
                                    RightimNavigationBar.requestLayout();
                                }
                            }
                            return;
                        } catch (Exception e3) {
                            e3.printStackTrace();
                            return;
                        }
                    default:
                        return;
                }
            }
        };
        registerReceiver(mReceiver, intentFilter);

        statusBarView.findViewById(R.id.llbtm).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        preheight = framede.getHeight();
                        pretouchY = event.getRawY();
                        break;
                    }
                    case MotionEvent.ACTION_MOVE: {
                        float change = event.getRawY() - pretouchY;
                        float checkheight = (int) (preheight + change);
                        if (checkheight > haderlayout.getHeight() + 10 && checkheight < biglaymaxheight) {
                            if (checkheight > frameheight / 2) {
                                framede.setAlpha((float) 1);
                                haderlayout.setAlpha((float) 0);
                                framede.setVisibility(View.VISIBLE);
                                haderlayout.setVisibility(View.GONE);
                                ConstraintLayout constraintLayout = statusBarView.findViewById(R.id.viewallll);
                                ConstraintSet constraintSet = new ConstraintSet();
                                constraintSet.clone(constraintLayout);
                                constraintSet.connect(R.id.llbrightness, ConstraintSet.TOP, R.id.framede, ConstraintSet.BOTTOM, 0);
                                constraintSet.applyTo(constraintLayout);
                            } else if (checkheight > frameheight / 3) {
                                framede.setAlpha((float) 0.8);
                                haderlayout.setAlpha((float) 0.2);
                                framede.setVisibility(View.VISIBLE);
                                haderlayout.setVisibility(View.GONE);
                                ConstraintLayout constraintLayout = statusBarView.findViewById(R.id.viewallll);
                                ConstraintSet constraintSet = new ConstraintSet();
                                constraintSet.clone(constraintLayout);
                                constraintSet.connect(R.id.llbrightness, ConstraintSet.TOP, R.id.framede, ConstraintSet.BOTTOM, 0);
                                constraintSet.applyTo(constraintLayout);
                            } else if (checkheight > frameheight / 4) {
                                framede.setAlpha((float) 0.5);
                                haderlayout.setAlpha((float) 0.5);
                            } else if (checkheight > frameheight / 6) {
                                framede.setAlpha((float) 0.2);
                                haderlayout.setAlpha((float) 0.8);
                            } else if (checkheight > frameheight / 7) {
                                framede.setAlpha((float) 0);
                                haderlayout.setAlpha((float) 1);
                                haderlayout.setVisibility(View.VISIBLE);
                                framede.setVisibility(View.GONE);
                                ConstraintLayout constraintLayout = statusBarView.findViewById(R.id.viewallll);
                                ConstraintSet constraintSet = new ConstraintSet();
                                constraintSet.clone(constraintLayout);
                                constraintSet.connect(R.id.llbrightness, ConstraintSet.TOP, R.id.haderlayout, ConstraintSet.BOTTOM, 0);
                                constraintSet.applyTo(constraintLayout);
                            }
                            framede.getLayoutParams().height = (int) (preheight + change);
                            framede.requestLayout();
                        }
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        change = event.getRawY() - pretouchY;
                        float checkheight = (int) (preheight + change);
                        float maxuppos = biglaymaxheight / 2;
                        float maxdownpos = biglaymaxheight / 2;
                        if (maxuppos >= checkheight) {
                            Handler handler = new Handler();
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    if (framede.getLayoutParams().height > haderlayout.getHeight()) {
                                        handler.postDelayed(this, 1);
                                        framede.getLayoutParams().height = framede.getLayoutParams().height - 40;
                                        framede.requestLayout();
                                    } else {
                                        haderlayout.setAlpha((float) 1);
                                        framede.setVisibility(View.GONE);
                                        haderlayout.setVisibility(View.VISIBLE);
                                        ConstraintLayout constraintLayout = statusBarView.findViewById(R.id.viewallll);
                                        ConstraintSet constraintSet = new ConstraintSet();
                                        constraintSet.clone(constraintLayout);
                                        constraintSet.connect(R.id.llbrightness, ConstraintSet.TOP, R.id.haderlayout, ConstraintSet.BOTTOM, 0);
                                        constraintSet.applyTo(constraintLayout);
                                    }

                                }
                            };
                            handler.postDelayed(runnable, 1);
                            if (checkheight < 0) {
                                haderlayout.setVisibility(View.GONE);
                                panelView.setVisibility(View.INVISIBLE);
                                manager.removeView(statusBarParentView);
                                manager.addView(statusBarParentView, localLayoutParams);
                            }
                        }
                        if (maxdownpos <= checkheight) {
                            Handler handler = new Handler();
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    if (framede.getLayoutParams().height < frameheight) {
                                        handler.postDelayed(this, 1);
                                        framede.getLayoutParams().height = framede.getLayoutParams().height + 40;
                                        framede.requestLayout();
                                    }
                                }
                            };
                            handler.postDelayed(runnable, 1);
                            controlPanelSubContainer.setVisibility(View.VISIBLE);
                            llbrightness.setVisibility(View.VISIBLE);
                            mPanelView.updateButtonStates();
                        }
                        break;
                    }
                }
                return true;
            }
        });
    }

    private void getQSIconList() {
        PackageManager packageManager = mContext.getPackageManager();
        List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(new Intent("android.service.quicksettings.action.QS_TILE"), 0);
        String string = getResources().getString(R.string.quick_settings_tiles_default);
        for (ResolveInfo resolveInfo : queryIntentServices) {
            ComponentName componentName = new ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name);
            if (!string.contains(componentName.flattenToString())) {
                resolveInfo.serviceInfo.applicationInfo.loadLabel(packageManager);
                StringBuilder w = w("custom(");
                w.append(componentName.flattenToShortString());
                w.append(")");
                w.toString();
                ServiceInfo serviceInfo = resolveInfo.serviceInfo;
                if (serviceInfo.icon != 0 || serviceInfo.applicationInfo.icon != 0) {
                    Drawable loadIcon = serviceInfo.loadIcon(packageManager);
                    if ("android.permission.BIND_QUICK_SETTINGS_TILE".equals(resolveInfo.serviceInfo.permission) && loadIcon != null) {
                        loadIcon.setColorFilter(ViewCompat.MEASURED_STATE_MASK, PorterDuff.Mode.SRC_IN);
                        CharSequence loadLabel = resolveInfo.serviceInfo.loadLabel(packageManager);
                        if (loadLabel != null) {
                            loadLabel.toString();
                        }
                    }
                }
            }
        }
    }

    public static StringBuilder w(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        return sb;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent != null) {
            try {
                if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 0) {
                    try {
                        if (Build.VERSION.SDK_INT >= 24) {
                            disableSelf();
                        }
                        stopForeground(true);
                    } catch (Exception ignored) {
                    }
                } else if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 1) {
                    mPanelView.initCustTopView();
                } else if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 2) {
                    mPanelView.TimeSecondChange();
                } else if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 3) {
                    mPanelView.initCustTopBrightness();
                } else if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 4) {
                    mPanelView.SetColorFilter();
                } else if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 5) {
                    mPanelView.SetSmallCorner();
                } else if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 6) {
                    mPanelView.SetHeaderTiles();
                } else if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 7) {
                    mPanelView.ShowPowerbutton();
                } else if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 9) {
                    mPanelView.ClockSize();
                } else if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 10) {
                    mPanelView.SetGridTiles();
                } else if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 11) {
                    performGlobalAction(GLOBAL_ACTION_QUICK_SETTINGS);
                } else if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 12) {
                    Intent intentaa = new Intent("customnotification.quick.controlsettings.ACCESSIBILITY_ACTION");
                    intentaa.putExtra("action", 8);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intentaa);
                } else if (intent.getIntExtra("com.control.center.intent.MESSAGE", -1) == 13) {
                    performGlobalAction(2);
                }
                createNotification();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return START_STICKY;
    }

    private void showHidePanel(boolean z, boolean z2) {
        mPanelView.utils.setTileColor(LauncherApp.get_ActiveTileColor());
        try{
            ((LayerDrawable) ((SeekBar) statusBarParentView.findViewById(R.id.seek_small_bright)).getProgressDrawable()).getDrawable(1).setTint(Utils.getAlphaColor(LauncherApp.get_ActiveTileColor(), LauncherApp.get_ActiveTilealpha()));
        }catch (ClassCastException ignored){
        }
        ViewGroup.LayoutParams layoutParams = controlPanelSubContainer.getLayoutParams();
        if (!z || panelView.getVisibility() != View.VISIBLE) {
            if (z2 && framede.getVisibility() == View.VISIBLE) {
                layoutParams.height = mPanelView.utils.getHeight(mContext);
                statusBarHeight = minStatusBarHeight;
                localLayoutParams.height = (int) (((float) statusBarHeight) * getResources().getDisplayMetrics().scaledDensity);
                haderlayout.setVisibility(View.GONE);
                panelView.setVisibility(View.INVISIBLE);
                manager.removeView(statusBarParentView);
                manager.addView(statusBarParentView, localLayoutParams);


                LauncherApp.set_OpenFirst(false);
                layoutParams.height = mPanelView.utils.getHeight(mContext);
                panelView.setVisibility(View.INVISIBLE);
                int height = mPanelView.utils.getHeight(mContext);
                statusBarHeight = height;
                localLayoutParams.height = height;
                controlPanelSubContainer.setVisibility(View.GONE);
            }
            if (z2 || z || layoutParams.height != -2) {
                if (!LauncherApp.get_Panelheight()) {
                    LauncherApp.set_Panelheight(true);
                    biglaymaxheight = controlPanelSubContainer.getHeight() + 45;
                    frameheight = framede.getHeight();
                    LauncherApp.set_BigFramehei(framede.getHeight());
                }
                frameheightaa = LauncherApp.get_BigFramehei();
                if (z) {
                    layoutParams.height = mPanelView.utils.getHeight(mContext);
                    statusBarHeight = minStatusBarHeight;
                    panelView.setVisibility(View.INVISIBLE);
                    controlPanelSubContainer.setVisibility(View.GONE);
                    localLayoutParams.height = (int) (((float) statusBarHeight) * getResources().getDisplayMetrics().scaledDensity);

                    int height = mPanelView.utils.getHeight(mContext);
                    statusBarHeight = height;
                    localLayoutParams.height = height;
                    controlPanelSubContainer.setVisibility(View.GONE);
                    statusBarView.postDelayed(() -> {
                        if (!LauncherApp.get_OpenFirst()) {
                            panelView.setVisibility(View.VISIBLE);
                            framede.setVisibility(View.VISIBLE);
                            haderlayout.setVisibility(View.GONE);
                            framede.setAlpha((float) 1);
                            ConstraintLayout constraintLayoutaa = statusBarView.findViewById(R.id.viewallll);
                            ConstraintSet constraintSetaa = new ConstraintSet();
                            constraintSetaa.clone(constraintLayoutaa);
                            constraintSetaa.connect(R.id.llbrightness, ConstraintSet.TOP, R.id.framede, ConstraintSet.BOTTOM, 0);
                            constraintSetaa.applyTo(constraintLayoutaa);
                            Handler handler = new Handler();
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    if (framede.getLayoutParams().height < frameheight) {
                                        handler.postDelayed(this, 1);
                                        framede.getLayoutParams().height = framede.getLayoutParams().height + 40;
                                        framede.requestLayout();
                                    }
                                }
                            };
                            handler.postDelayed(runnable, 1);
                            controlPanelSubContainer.setVisibility(View.VISIBLE);
                            llbrightness.setVisibility(View.VISIBLE);
                            mPanelView.updateButtonStates();
                            LauncherApp.set_OpenFirst(true);
                        } else {
                            panelView.setVisibility(View.VISIBLE);
                            controlPanelSubContainer.setVisibility(View.GONE);
                            haderlayout.setVisibility(View.VISIBLE);
                            ConstraintLayout constraintLayout = statusBarView.findViewById(R.id.viewallll);
                            ConstraintSet constraintSet = new ConstraintSet();
                            constraintSet.clone(constraintLayout);
                            constraintSet.connect(R.id.llbrightness, ConstraintSet.TOP, R.id.haderlayout, ConstraintSet.BOTTOM, 0);
                            constraintSet.applyTo(constraintLayout);
                            mPanelView.updateButtonStates();
                            mPanelView.startPanelUpperAnimations(statusBarView);
                            Animation animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slidedown);
                            panelView.startAnimation(animSlideDown);
                        }
                    }, 100);

                    manager.removeView(statusBarParentView);
                    manager.addView(statusBarParentView, localLayoutParams);
                } else {
                    if (framede.getVisibility() == View.VISIBLE) {
                        Handler handler = new Handler();
                        Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                if (framede.getLayoutParams().height > haderlayout.getHeight()) {
                                    handler.postDelayed(this, 1);
                                    framede.getLayoutParams().height = framede.getLayoutParams().height - 40;
                                    framede.requestLayout();
                                } else {
                                    haderlayout.setAlpha((float) 1);
                                    framede.setVisibility(View.GONE);
                                    haderlayout.setVisibility(View.VISIBLE);
                                    ConstraintLayout constraintLayout = statusBarView.findViewById(R.id.viewallll);
                                    ConstraintSet constraintSet = new ConstraintSet();
                                    constraintSet.clone(constraintLayout);
                                    constraintSet.connect(R.id.llbrightness, ConstraintSet.TOP, R.id.haderlayout, ConstraintSet.BOTTOM, 0);
                                    constraintSet.applyTo(constraintLayout);
                                }

                            }
                        };
                        handler.postDelayed(runnable, 1);
                    } else if (haderlayout.getVisibility() == View.VISIBLE) {
                        layoutParams.height = mPanelView.utils.getHeight(mContext);
                        statusBarHeight = minStatusBarHeight;
                        localLayoutParams.height = (int) (((float) statusBarHeight) * getResources().getDisplayMetrics().scaledDensity);

                        haderlayout.setVisibility(View.GONE);
                        panelView.setVisibility(View.INVISIBLE);
                        manager.removeView(statusBarParentView);
                        manager.addView(statusBarParentView, localLayoutParams);
                    }
                }
            }
        }
        if (haderlayout.getVisibility() == View.VISIBLE) {
            layoutParams.height = mPanelView.utils.getHeight(mContext);
            statusBarHeight = minStatusBarHeight;
            panelView.setVisibility(View.VISIBLE);
            framede.setVisibility(View.VISIBLE);
            framede.setAlpha((float) 1);
            haderlayout.setVisibility(View.GONE);
            ConstraintLayout constraintLayout = statusBarView.findViewById(R.id.viewallll);
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(constraintLayout);
            constraintSet.connect(R.id.llbrightness, ConstraintSet.TOP, R.id.framede, ConstraintSet.BOTTOM, 0);
            constraintSet.applyTo(constraintLayout);
            Handler handler = new Handler();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (framede.getLayoutParams().height < frameheight) {
                        handler.postDelayed(this, 1);
                        framede.getLayoutParams().height = framede.getLayoutParams().height + 40;
                        framede.requestLayout();
                    }
                }
            };
            handler.postDelayed(runnable, 1);
            controlPanelSubContainer.setVisibility(View.VISIBLE);
            llbrightness.setVisibility(View.VISIBLE);
            mPanelView.updateButtonStates();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        updateFontSize(configuration);
    }

    public final void updateFontSize(Configuration configuration) {
        if (configuration.fontScale > 1.3f) {
            configuration.fontScale = 1.3f;
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
            displayMetrics.scaledDensity = configuration.fontScale * displayMetrics.density;
            getResources().updateConfiguration(configuration, displayMetrics);
        }
    }

    public void onDestroy() {
        if (manager != null) {
            manager.removeView(statusBarParentView);
        }
        if (mPanelView != null) {
            mPanelView.doCleanUp();
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(powerMenuReceiver);
        super.onDestroy();
    }

    public void onCreate() {
        super.onCreate();
        LocalBroadcastManager.getInstance(this).registerReceiver(powerMenuReceiver, new IntentFilter("customnotification.quick.controlsettings.ACCESSIBILITY_ACTION"));
        mContext = getBaseContext();
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        btmwindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        btmY_THRESH_HOLD = DHANVINE_Utils.convertDpToPixel(25, mContext);
        btmX_THRESH_HOLD = DHANVINE_Utils.convertDpToPixel(25, mContext);
        btmMIN_THRESH_HOLD = DHANVINE_Utils.convertDpToPixel(5, mContext);
        LeftwindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LeftY_THRESH_HOLD = DHANVINE_Utils.convertDpToPixel(25, mContext);
        LeftX_THRESH_HOLD = DHANVINE_Utils.convertDpToPixel(25, mContext);
        LeftMIN_THRESH_HOLD = DHANVINE_Utils.convertDpToPixel(5, mContext);
        RightwindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        RightY_THRESH_HOLD = DHANVINE_Utils.convertDpToPixel(25, mContext);
        RightX_THRESH_HOLD = DHANVINE_Utils.convertDpToPixel(25, mContext);
        RightMIN_THRESH_HOLD = DHANVINE_Utils.convertDpToPixel(5, mContext);
    }

    @SuppressLint("WrongConstant")
    public void btminitLayoutParams() {
        btmparams = new WindowManager.LayoutParams();
        if (Build.VERSION.SDK_INT >= 26) {
            btmparams.type = 2038;
        } else {
            btmparams.type = 2002;
        }
        btmparams.flags = 262440;
        btmparams.format = -3;
        btmparams.width = -2;
        btmparams.height = -2;
        btmparams.x = 0;
        btmparams.y = 0;
        btmparams.gravity = 81;
    }

    public void btmshowNavigationBar() {
        if (btmnavigationView == null) {
            btmnavigationView = LayoutInflater.from(getBaseContext()).inflate(R.layout.dhanvine_navigation_view, (ViewGroup) null);
        }
        try {
            btmwindowManager.removeView(btmnavigationView);
        } catch (Exception ignored) {
        }
        btmwindowManager.addView(btmnavigationView, btmparams);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void btminitAndDeclareNavigation(View viewaa, Context context) {
        btmimNavigationBar = viewaa.findViewById(R.id.im_navigation_bar);
        btmimNavigationBar.setImageResource(R.drawable.img_btm_service);
        btmparamsaa = new FrameLayout.LayoutParams(LauncherApp.get_BarWith(), 25);
        btmimNavigationBar.setColorFilter(LauncherApp.getbtmBarColorAlpha(), PorterDuff.Mode.SRC_IN);
        btmimNavigationBar.getLayoutParams().width = LauncherApp.get_BarWith();
        btmimNavigationBar.getLayoutParams().height = LauncherApp.get_BarHeight();
        int midval = Resources.getSystem().getDisplayMetrics().widthPixels;
        btmparamsaa.setMarginEnd(LauncherApp.get_BarPosition());
        int finwid = LauncherApp.get_BarPosition() - midval;
        if (finwid == 0) {
            LauncherApp.set_BarMinvat(LauncherApp.get_BarPosition());
        }
        btmparamsaa.setMarginStart(finwid);
        btmimNavigationBar.setLayoutParams(btmparamsaa);
        btmimNavigationBar.requestLayout();
        btmimNavigationBar.setOnTouchListener(new View.OnTouchListener() {
            float initialTouchX, initialTouchY;
            int initialY;
            int xPrec = 0;
            int yPrec = 0;

            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                        if (LauncherApp.get_Vibrateonoff()) {
                            vibrator.vibrate(30);
                        }
                        initialY = btmparams.y;
                        initialTouchX = motionEvent.getRawX();
                        initialTouchY = motionEvent.getRawY();
                        return true;
                    case 1:
                        if (btmparams.y != 0) {
                            showHidePanel(true, false);
                        } else if (btmparams.x < 0) {
                            showHidePanel(true, false);
                        }
                        btmparams.x = 0;
                        btmparams.y = 0;
                        btmwindowManager.updateViewLayout(viewaa, btmparams);
                        btmlimited = false;
                        return true;
                    case 2:
                        yPrec = -(initialY + ((int) (motionEvent.getRawY() - initialTouchY)));
                        if (Math.abs(xPrec) < Math.abs(yPrec)) {
                            if (Math.abs(yPrec) <= btmY_THRESH_HOLD) {
                                btmparams.y = yPrec;
                            } else {
                                btmparams.y = btmY_THRESH_HOLD;
                            }
                            btmparams.x = 0;
                            btmwindowManager.updateViewLayout(viewaa, btmparams);
                            if (btmparams.y == btmY_THRESH_HOLD && !btmlimited) {
                                btmlimited = true;
                                if (LauncherApp.get_Vibrateonoff()) {
                                    vibrator.vibrate(30);
                                }
                            }
                        } else {
                            if (Math.abs(xPrec) <= btmX_THRESH_HOLD) {
                                btmparams.x = xPrec;
                            } else if (xPrec < 0) {
                                btmparams.x = -btmX_THRESH_HOLD;
                            } else {
                                btmparams.x = btmX_THRESH_HOLD;
                            }
                            btmparams.y = 0;
                            btmwindowManager.updateViewLayout(viewaa, btmparams);
                            if (Math.abs(btmparams.x) == btmX_THRESH_HOLD && !btmlimited) {
                                btmlimited = true;
                                if (LauncherApp.get_Vibrateonoff()) {
                                    vibrator.vibrate(30);
                                }
                            }
                        }
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    //    Left
    @SuppressLint("WrongConstant")
    public void LeftinitLayoutParams() {
        Leftparams = new WindowManager.LayoutParams();
        if (Build.VERSION.SDK_INT >= 26) {
            Leftparams.type = 2038;
        } else {
            Leftparams.type = 2002;
        }
        Leftparams.flags = 262440;
        Leftparams.format = -3;
        Leftparams.width = -2;
        Leftparams.height = -2;
        Leftparams.x = 0;
        Leftparams.y = 0;
        Leftparams.gravity = Gravity.LEFT | Gravity.CENTER;
    }

    public void LeftshowNavigationBar() {
        if (LeftnavigationView == null) {
            LeftnavigationView = LayoutInflater.from(getBaseContext()).inflate(R.layout.dhanvine_navigation_view, (ViewGroup) null);
        }
        try {
            LeftwindowManager.removeView(LeftnavigationView);
        } catch (Exception ignored) {
        }
        LeftwindowManager.addView(LeftnavigationView, Leftparams);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void LeftinitAndDeclareNavigation(View viewaaa, Context context) {
        LeftimNavigationBar = viewaaa.findViewById(R.id.im_navigation_bar);
        LeftimNavigationBar.setImageResource(R.drawable.img_tl_service);
        Leftparamsaa = new FrameLayout.LayoutParams(25, 200);
        LeftimNavigationBar.setColorFilter(LauncherApp.getLeftBarColorAlpha(), PorterDuff.Mode.SRC_IN);
        LeftimNavigationBar.getLayoutParams().width = LauncherApp.get_LeftBarWith();
        LeftimNavigationBar.getLayoutParams().height = 200;
        if (0 < LauncherApp.get_LeftBarPosition()) {
            Leftparamsaa.setMargins(0, LauncherApp.get_LeftBarPosition(), 0, 0);
        } else {
            Leftparamsaa.setMargins(0, 0, 0, Math.abs(LauncherApp.get_LeftBarPosition()));
        }
        LeftimNavigationBar.setLayoutParams(Leftparamsaa);
        LeftimNavigationBar.requestLayout();
        LeftimNavigationBar.setOnTouchListener(new View.OnTouchListener() {
            private float initialTouchX;
            private int initialX;

            public boolean onTouch(View view, MotionEvent motionEvent) {
                int yPrec = 0;
                switch (motionEvent.getAction()) {
                    case 0:
                        if (LauncherApp.get_Vibrateonoff()) {
                            vibrator.vibrate(30);
                        }
                        initialX = Leftparams.x;
                        initialTouchX = motionEvent.getRawX();
                        return true;
                    case 1:
                        LeftpointMoved = false;
                        int i = Leftparams.x;
                        int i2 = Leftparams.y;
                        if (i2 != 0) {
                            showHidePanel(true, false);
                        } else if (i < 0) {
                            showHidePanel(true, false);
                        } else {
                            showHidePanel(true, false);
                        }
                        LeftLIMITED_MILESTONE = 0;
                        Leftparams.x = 0;
                        Leftparams.y = 0;
                        LeftwindowManager.updateViewLayout(viewaaa, Leftparams);
                        Leftlimited = false;
                        return true;
                    case 2:
                        int xPrec = initialX + ((int) (motionEvent.getRawX() - initialTouchX));
                        if (Math.abs(xPrec) <= LeftMIN_THRESH_HOLD && Math.abs(yPrec) <= LeftMIN_THRESH_HOLD && !LeftpointMoved) {
                            LeftpointMoved = true;
                        }
                        if (Math.abs(xPrec) < Math.abs(yPrec)) {
                            if (Math.abs(yPrec) <= LeftY_THRESH_HOLD) {
                                Leftparams.y = yPrec;
                            } else {
                                Leftparams.y = LeftY_THRESH_HOLD;
                            }
                            Leftparams.x = 0;
                            LeftwindowManager.updateViewLayout(viewaaa, Leftparams);
                            if (Leftparams.y == LeftY_THRESH_HOLD && !Leftlimited) {
                                Leftlimited = true;
                                if (LauncherApp.get_Vibrateonoff()) {
                                    vibrator.vibrate(30);
                                }
                                LeftLIMITED_MILESTONE = System.currentTimeMillis();
                            }
                        } else {
                            if (Math.abs(xPrec) <= btmX_THRESH_HOLD) {
                                Leftparams.x = xPrec;
                            } else if (xPrec < 0) {
                                Leftparams.x = -LeftX_THRESH_HOLD;
                            } else {
                                Leftparams.x = LeftX_THRESH_HOLD;
                            }
                            Leftparams.y = 0;
                            LeftwindowManager.updateViewLayout(viewaaa, Leftparams);
                            if (Math.abs(Leftparams.x) == LeftX_THRESH_HOLD && !Leftlimited) {
                                Leftlimited = true;
                                if (LauncherApp.get_Vibrateonoff()) {
                                    vibrator.vibrate(30);
                                }
                                LeftLIMITED_MILESTONE = System.currentTimeMillis();
                            }
                        }
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    //    Right
    @SuppressLint("WrongConstant")
    public void RightinitLayoutParams() {
        Rightparams = new WindowManager.LayoutParams();
        if (Build.VERSION.SDK_INT >= 26) {
            Rightparams.type = 2038;
        } else {
            Rightparams.type = 2002;
        }
        Rightparams.flags = 262440;
        Rightparams.format = -3;
        Rightparams.width = -2;
        Rightparams.height = -2;
        Rightparams.x = 0;
        Rightparams.y = 0;
        Rightparams.gravity = Gravity.RIGHT | Gravity.CENTER;
    }

    public void RightshowNavigationBar() {
        if (RightnavigationView == null) {
            RightnavigationView = LayoutInflater.from(getBaseContext()).inflate(R.layout.dhanvine_navigation_view, (ViewGroup) null);
        }
        try {
            RightwindowManager.removeView(RightnavigationView);
        } catch (Exception ignored) {
        }
        RightwindowManager.addView(RightnavigationView, Rightparams);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void RightinitAndDeclareNavigation(View viewaaa, Context context) {
        RightimNavigationBar = viewaaa.findViewById(R.id.im_navigation_bar);
        RightimNavigationBar.setImageResource(R.drawable.img_right_service);
        Rightparamsaa = new FrameLayout.LayoutParams(25, 200);
        RightimNavigationBar.setColorFilter(LauncherApp.getRightBarColorAlpha(), PorterDuff.Mode.SRC_IN);
        RightimNavigationBar.getLayoutParams().width = LauncherApp.get_RightBarWith();
        RightimNavigationBar.getLayoutParams().height = 200;
        if (0 < LauncherApp.get_RightBarPosition()) {
            Rightparamsaa.setMargins(0, LauncherApp.get_RightBarPosition(), 0, 0);
        } else {
            Rightparamsaa.setMargins(0, 0, 0, Math.abs(LauncherApp.get_RightBarPosition()));
        }
        RightimNavigationBar.setLayoutParams(Rightparamsaa);
        RightimNavigationBar.requestLayout();
        RightimNavigationBar.setOnTouchListener(new View.OnTouchListener() {
            private float initialTouchX;
            private int initialX;

            public boolean onTouch(View view, MotionEvent motionEvent) {
                int yPrec = 0;
                switch (motionEvent.getAction()) {
                    case 0:
                        RightconfirmLongtap = false;
                        if (LauncherApp.get_Vibrateonoff()) {
                            vibrator.vibrate(30);
                        }
                        initialX = Rightparams.x;
                        int initialY = Rightparams.y;
                        initialTouchX = motionEvent.getRawX();
                        return true;
                    case 1:
                        RightpointMoved = false;
                        int i = Rightparams.x;
                        int i2 = Rightparams.y;
                        if (!RightconfirmLongtap) {
                            if (i2 != 0) {
                                showHidePanel(true, false);
                            } else if (i < 0) {
                                showHidePanel(true, false);
                            } else {
                                showHidePanel(true, false);
                            }
                        }
                        RightLIMITED_MILESTONE = 0;
                        Rightparams.x = 0;
                        Rightparams.y = 0;
                        RightwindowManager.updateViewLayout(viewaaa, Rightparams);
                        Rightlimited = false;
                        return true;
                    case 2:
                        int xPrec = initialX + ((int) (motionEvent.getRawX() - initialTouchX));
                        if (Math.abs(xPrec) <= RightMIN_THRESH_HOLD && Math.abs(yPrec) <= RightMIN_THRESH_HOLD && !RightpointMoved) {
                            RightpointMoved = true;
                        }
                        if (Math.abs(xPrec) < Math.abs(yPrec)) {
                            if (Math.abs(yPrec) <= RightY_THRESH_HOLD) {
                                Rightparams.y = yPrec;
                            } else {
                                Rightparams.y = RightY_THRESH_HOLD;
                            }
                            Rightparams.x = 0;
                            RightwindowManager.updateViewLayout(viewaaa, Rightparams);
                            if (Rightparams.y == RightY_THRESH_HOLD && !Rightlimited) {
                                Rightlimited = true;
                                if (LauncherApp.get_Vibrateonoff()) {
                                    vibrator.vibrate(30);
                                }
                                RightLIMITED_MILESTONE = System.currentTimeMillis();
                            }
                        } else {
                            if (Math.abs(xPrec) <= RightX_THRESH_HOLD) {
                                Rightparams.x = xPrec;
                            } else if (xPrec < 0) {
                                Rightparams.x = -RightX_THRESH_HOLD;
                            } else {
                                Rightparams.x = RightX_THRESH_HOLD;
                            }
                            Rightparams.y = 0;
                            RightwindowManager.updateViewLayout(viewaaa, Rightparams);
                            if (Math.abs(Rightparams.x) == RightX_THRESH_HOLD && !Rightlimited) {
                                Rightlimited = true;
                                if (LauncherApp.get_Vibrateonoff()) {
                                    vibrator.vibrate(30);
                                }
                                RightLIMITED_MILESTONE = System.currentTimeMillis();
                            }
                        }
                        return true;
                    default:
                        return false;
                }
            }
        });
    }


    private void createNotification() {
        createNotificationChannel();
        Intent intent = new Intent(this, MAccessibilityService.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        } else {
            pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        Notification notification = new NotificationCompat.Builder(this, "1")
                .setContentTitle(getString(R.string.app_name))
                .setVibrate(null)
                .setContentText(getString(R.string.app_name))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .build();
        startForeground(1, notification);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel("1", "Foreground Service Channel", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}