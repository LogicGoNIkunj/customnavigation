package customnotification.quick.controlsettings.services;

import customnotification.quick.controlsettings.utils.Notification;

public interface NotificationListener {
    void onItemClicked(Notification notification);
}