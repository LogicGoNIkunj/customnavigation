package customnotification.quick.controlsettings.services;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.utils.Constants;
import customnotification.quick.controlsettings.utils.Utils;

import java.io.ByteArrayOutputStream;

public class NotificationService extends NotificationListenerService {
    Context context;
    final Handler handler = new Handler();

    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public void onNotificationPosted(final StatusBarNotification statusBarNotification) {
        handler.postDelayed(() -> sendNotification(statusBarNotification, true), 1000);
    }

    public void onNotificationRemoved(final StatusBarNotification statusBarNotification) {
        handler.postDelayed(() -> sendNotification(statusBarNotification, false), 1000);
    }

    private void sendNotification(StatusBarNotification statusBarNotification, boolean z) {
        String str;
        String str2;
        String str3;
        Bitmap bitmap;
        if (statusBarNotification.getNotification() != null) {
            String packageName = statusBarNotification.getPackageName();
            String charSequence = statusBarNotification.getNotification().tickerText != null ? statusBarNotification.getNotification().tickerText.toString() : null;
            Bitmap drawableToBmp = statusBarNotification.getNotification().getLargeIcon() == null ? null : drawableToBmp(context, statusBarNotification.getNotification().getLargeIcon().loadDrawable(context), 50);
            if (statusBarNotification.getNotification().extras == null) {
                str2 = null;
                str = null;
            } else {
                Bundle bundle = statusBarNotification.getNotification().extras;
                str = bundle.getString(NotificationCompat.EXTRA_TITLE);
                str2 = bundle.getCharSequence(NotificationCompat.EXTRA_TEXT) != null ? bundle.getCharSequence(NotificationCompat.EXTRA_TEXT).toString() : null;
            }
            str3 = String.valueOf(statusBarNotification.getId());
            try {
                Drawable drawable = ContextCompat.getDrawable(createPackageContext(packageName, 0), statusBarNotification.getNotification().icon);
                if (drawable != null) {
                    bitmap = drawableToBmp(null, drawable, 20);
                    Intent intent = new Intent(Utils.FROM_NOTIFICATION_SERVICE);
                    intent.putExtra("id", str3);
                    intent.putExtra("package", packageName);
                    intent.putExtra("ticker", charSequence);
                    intent.putExtra("title", str);
                    intent.putExtra("isAdded", z);
                    intent.putExtra("postTime", statusBarNotification.getPostTime());
                    intent.putExtra("text", str2);
                    intent.putExtra("actions", statusBarNotification.getNotification().actions);
                    intent.putExtra("largeIcon", getByteArrayFromBitmap(drawableToBmp));
                    if (bitmap != null) {
                        intent.putExtra("icon", getByteArrayFromBitmap(drawableToBmp(null, ContextCompat.getDrawable(context, R.drawable.android_icon), 20)));
                    } else {
                        intent.putExtra("icon", getByteArrayFromBitmap(bitmap));
                    }
                    intent.putExtra("pendingIntent", statusBarNotification.getNotification().contentIntent);
                    context.sendBroadcast(intent);
                }
            } catch (Exception | OutOfMemoryError e) {
                e.printStackTrace();
            }
            Intent intent2 = new Intent(Utils.FROM_NOTIFICATION_SERVICE);
            intent2.putExtra("id", str3);
            intent2.putExtra("package", packageName);
            intent2.putExtra("ticker", charSequence);
            intent2.putExtra("title", str);
            intent2.putExtra("isAdded", z);
            intent2.putExtra("postTime", statusBarNotification.getPostTime());
            intent2.putExtra("text", str2);
            intent2.putExtra("actions", statusBarNotification.getNotification().actions);
            intent2.putExtra("largeIcon", getByteArrayFromBitmap(drawableToBmp));
            intent2.putExtra("pendingIntent", statusBarNotification.getNotification().contentIntent);
            context.sendBroadcast(intent2);
        }
    }

    private byte[] getByteArrayFromBitmap(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private Bitmap scaleDownImage(Bitmap bitmap, int i, int i2) {
        try {
            float width = (float) bitmap.getWidth();
            float height = (float) bitmap.getHeight();
            float f = (float) i;
            if (width <= f) {
                return bitmap;
            }
            float f2 = width / height;
            float f3 = (float) i2;
            float f4 = f / f3;
            if (height <= width) {
                if (f4 < 1.0f) {
                } else {
                    f = (float) ((int) (f3 / f2));
                }
                f3 = f;
                f = f3;
            } else if (f4 > 1.0f) {
                f = (float) ((int) (f2 * f3));
            } else {
                f3 = (float) ((int) (f / f2));
            }
            return Bitmap.createScaledBitmap(bitmap, (int) f, (int) f3, true);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    private Bitmap getCroppedBitmap(Context context2, Drawable drawable) {
        Display defaultDisplay;
        int i = 0;
        int i2 = 0;
        try {
            defaultDisplay = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getRealMetrics(displayMetrics);
            i = displayMetrics.widthPixels;
            i2 = displayMetrics.heightPixels;
        } catch (Exception | OutOfMemoryError ignored) {
        }
        Bitmap createBitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        if (createBitmap.getWidth() <= i || createBitmap.getHeight() <= i2) {
            return scaleUpImage(createBitmap, i, i2);
        }
        return scaleDownImage(createBitmap, i, i2);
    }

    private Bitmap scaleUpImage(Bitmap bitmap, int i, int i2) {
        Bitmap bitmap2 = null;
        try {
            if (bitmap.getWidth() > i) {
                bitmap.recycle();
            } else {
                if (bitmap.getHeight() > i2) {
                    bitmap.recycle();
                }
                return null;
            }
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
        return bitmap2;
    }

    public Bitmap drawableToBmp(Context context2, Drawable drawable, int i) {
        Bitmap bitmap;
        if (drawable == null) {
            return null;
        }
        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_4444);
        } else if (i <= 0) {
            return getCroppedBitmap(context2, drawable);
        } else {
            int convertDpToPixel = (int) Constants.convertDpToPixel((float) i, context2);
            bitmap = Bitmap.createBitmap(convertDpToPixel, convertDpToPixel, Bitmap.Config.ARGB_4444);
        }
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}