package customnotification.quick.controlsettings.services;

public interface OnPanelItemClickListner {
    void onItemClicked(boolean z, Boolean bool);
}