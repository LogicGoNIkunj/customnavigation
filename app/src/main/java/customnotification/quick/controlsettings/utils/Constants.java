package customnotification.quick.controlsettings.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;

import customnotification.quick.controlsettings.services.MAccessibilityService;
import pub.devrel.easypermissions.EasyPermissions;

public class Constants {
    public static final String CAMERA_PKG = "CAMERA_PKG";
    public static final String NOTIFICATION = "NOTIFICATION";
    public static final String[] PHONE_STATE_PERMISSION = {"android.permission.CAMERA"};

    public static SharedPreferences setNotif(Context context, boolean z) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        defaultSharedPreferences.edit().putBoolean(NOTIFICATION, z).apply();
        return defaultSharedPreferences;
    }

    public static boolean getNotif(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(NOTIFICATION, false);
    }

    public static void setAutoOrientationEnabled(Context context, boolean z) {
        Settings.System.putInt(context.getContentResolver(), "accelerometer_rotation", z ? 1 : 0);
    }

    public static void setCameraPkg(Context context, String str) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(CAMERA_PKG, str).apply();
    }

    public static void setPhonePkg(Context context, String str) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("PHONE_PKG", str).apply();
    }

    public static boolean isRotationOn(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "accelerometer_rotation", 0) == 1;
    }

    public static String getCameraPkg(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(CAMERA_PKG, "");
    }

    public static boolean checkSystemWritePermission(Context context) {
        return Settings.System.canWrite(context);
    }

    public static float convertDpToPixel(float f, Context context) {
        Resources resources;
        if (context == null) {
            resources = Resources.getSystem();
        } else {
            resources = context.getResources();
        }
        return f * (((float) resources.getDisplayMetrics().densityDpi) / 160.0f);
    }

    public static boolean hasPermissions(Context context, String... strArr) {
        return EasyPermissions.hasPermissions(context, strArr);
    }

    public static boolean checkAccessibilityEnabled(Context context) {
        String string = Settings.Secure.getString(context.getContentResolver(), "enabled_accessibility_services");
        if (string == null) {
            return false;
        }
        TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(':');
        simpleStringSplitter.setString(string);
        while (simpleStringSplitter.hasNext()) {
            String next = simpleStringSplitter.next();
            if (next.equalsIgnoreCase(context.getPackageName() + "/" + MAccessibilityService.class.getName())) {
                return true;
            }
        }
        return false;
    }
}