package customnotification.quick.controlsettings.utils;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;

import androidx.core.content.ContextCompat;

import customnotification.quick.controlsettings.services.MAccessibilityService;

public class DHANVINE_Utils {
    public static boolean isAccessibilityServiceEnabled(Context context) {
        int i;
        String string;
        String str = context.getPackageName() + "/" + MAccessibilityService.class.getCanonicalName();
        try {
            i = Settings.Secure.getInt(context.getContentResolver(), "accessibility_enabled");
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            i = 0;
        }
        TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(':');
        if (i == 1 && (string = Settings.Secure.getString(context.getContentResolver(), "enabled_accessibility_services")) != null) {
            simpleStringSplitter.setString(string);
            while (simpleStringSplitter.hasNext()) {
                if (simpleStringSplitter.next().equalsIgnoreCase(str)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static int checkPermission(String[] strArr, Context context) {
        int i = 0;
        for (String str : strArr) {
            i += ContextCompat.checkSelfPermission(context, str);
        }
        return i;
    }

    public static int convertDpToPixel(int i, Context context) {
        return (int) (((float) i) * (((float) context.getResources().getDisplayMetrics().densityDpi) / 160.0f));
    }
}