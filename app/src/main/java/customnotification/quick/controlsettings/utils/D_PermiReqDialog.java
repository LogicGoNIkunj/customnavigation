package customnotification.quick.controlsettings.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;

import customnotification.quick.controlsettings.R;

public class D_PermiReqDialog extends AlertDialog {
    public final D_PermiReqListener listener;

    public D_PermiReqDialog(Context context, D_PermiReqListener dHANVINE_PermissionRequestListener) {
        super(context);
        this.listener = dHANVINE_PermissionRequestListener;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.dhanvine_dialog_permission_request);
        findViewById(R.id.tv_sure).setOnClickListener(view -> {
            dismiss();
            listener.sureClicked();
        });
        findViewById(R.id.tv_retry).setOnClickListener(view -> {
            dismiss();
            listener.goToInfo();
        });
    }
}