package customnotification.quick.controlsettings.utils;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.LinkedList;

import customnotification.quick.controlsettings.model.WallpaperModel;

public class FavPreference {
    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;

    public FavPreference(@NonNull Context context) {
        preferences = context.getSharedPreferences("favImageTable", Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void addToFavorite(WallpaperModel.Data.Datum imageUrl) {
        ArrayList<WallpaperModel.Data.Datum> imageList = getFavoriteList();
        imageList.add(imageUrl);
        setImageList(imageList);
    }

    public void deleteFromFavorite(WallpaperModel.Data.Datum imageUrl) {
        LinkedList<WallpaperModel.Data.Datum> imageList = new LinkedList<>(getFavoriteList());
        for (int i = 0; i < imageList.size(); i++) {
            if (imageList.get(i).getId().equals(imageUrl.getId())) {
                imageList.remove(i);
            }
        }
        setImageList(new ArrayList<>(imageList));
    }

    public void setImageList(ArrayList<WallpaperModel.Data.Datum> imageList) {
        editor.putString("imageList", new Gson().toJson(imageList));
        editor.commit();
    }

    public ArrayList<WallpaperModel.Data.Datum> getFavoriteList() {
        String imagesString = preferences.getString("imageList", "");
        if (!imagesString.equalsIgnoreCase("")) {
            return new Gson().fromJson(imagesString, new TypeToken<ArrayList<WallpaperModel.Data.Datum>>() {
            }.getType());
        }
        return new ArrayList<>();
    }
}