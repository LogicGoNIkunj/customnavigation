package customnotification.quick.controlsettings.utils;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HorizontalMarginItemDecoration extends RecyclerView.ItemDecoration {

    private final int horizontalMarginInPx;

    public void getItemOffsets(Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        outRect.right = this.horizontalMarginInPx;
        outRect.left = this.horizontalMarginInPx;
    }

    public HorizontalMarginItemDecoration(Context context, @DimenRes int horizontalMarginInDp) {
        this.horizontalMarginInPx = (int) context.getResources().getDimension(horizontalMarginInDp);
    }
}