package customnotification.quick.controlsettings.utils;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import customnotification.quick.controlsettings.adaptar.CustomNotificationAdapter;

public class RecyclerItemTouchHelper extends ItemTouchHelper.SimpleCallback {
    private final RecyclerItemTouchHelperListener listener;

    public interface RecyclerItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int i, int i2);
    }

    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
        return true;
    }

    public RecyclerItemTouchHelper(int i, int i2, RecyclerItemTouchHelperListener recyclerItemTouchHelperListener) {
        super(i, i2);
        this.listener = recyclerItemTouchHelperListener;
    }

    public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        String obj = ((CustomNotificationAdapter.ViewHolder) viewHolder).tv_title.getTag().toString();
        if (obj.equals("android") || obj.equals("com.android.systemui") || obj.equals("com.android.settings")) {
            return 0;
        }
        return super.getSwipeDirs(recyclerView, viewHolder);
    }

    public void onSwiped(RecyclerView.ViewHolder viewHolder, int i) {
        this.listener.onSwiped(viewHolder, i, viewHolder.getAdapterPosition());
    }

    public int convertToAbsoluteDirection(int i, int i2) {
        return super.convertToAbsoluteDirection(i, i2);
    }
}