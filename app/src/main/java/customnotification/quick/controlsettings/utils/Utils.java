package customnotification.quick.controlsettings.utils;

import static customnotification.quick.controlsettings.views.PanelView.longItemsAdapter1;
import static customnotification.quick.controlsettings.views.PanelView.longItemsAdapter2;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;

import java.lang.reflect.Method;
import java.util.ArrayList;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.largetile.LongItems;

public class Utils {
    public static final String FROM_NOTIFICATION_SERVICE = "from_notification_service";
    public final Context context;
    private int tile_color;

    public Utils(Context context2) {
        this.context = context2;
        this.tile_color = LauncherApp.get_ActiveTileColor();
    }

    public void setTileColor(int i) {
        this.tile_color = i;
    }

    public int getHeight(Context context2) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context2.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRealMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public int getBrightMode(Context context2) {
        try {
            return Settings.System.getInt(context2.getContentResolver(), "screen_brightness_mode");
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean isHotspotOn(Context context2) {
        WifiManager wifiManager = (WifiManager) context2.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        try {
            Method declaredMethod = Class.forName(wifiManager.getClass().getName()).getDeclaredMethod("getWifiApState", new Class[0]);
            declaredMethod.setAccessible(true);
            int intValue = (Integer) declaredMethod.invoke(wifiManager, null);
            return intValue == 13;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void getBrightness(SeekBar seekBar, Context context2) {
        float f;
        try {
            f = (float) Settings.System.getInt(context2.getContentResolver(), "screen_brightness");
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            f = 0.0f;
        }
        seekBar.setProgress((int) f);
    }

    public void setBrightness(int i, Context context2) {
        if (getBrightMode(context2) == 1) {
            Settings.System.putInt(context2.getContentResolver(), "screen_brightness_mode", 0);
        }
        Settings.System.putInt(context2.getContentResolver(), "screen_brightness", i);
    }

    public void isAirplaneModeOn(Context context2, ArrayList<LongItems> stringArrayList) {
        int i = 1;
        if (Settings.Global.getInt(context2.getContentResolver(), "airplane_mode_on", 0) == 0) {
            i = 0;
        }
        stringArrayList.get(7).setActiveornot(i != 0);
    }

    public void gpsstate(Context context2, ArrayList<LongItems> stringArrayList) {
        stringArrayList.get(8).setActiveornot(((LocationManager) context2.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled("gps"));
        if (longItemsAdapter1 != null) {
            longItemsAdapter1.notifyDataSetChanged();
        }
        if (longItemsAdapter2 != null) {
            longItemsAdapter2.notifyDataSetChanged();
        }
    }

    public void isAutoRotateOn(Context context2, ArrayList<LongItems> stringArrayList) {
        if (!(Settings.System.getInt(context2.getContentResolver(), "accelerometer_rotation", 0) == 1)) {
            Constants.setAutoOrientationEnabled(context2, true);
            stringArrayList.get(6).setActiveornot(false);
            stringArrayList.get(6).setThumb(R.drawable.ic_qs_auto_rotate);
        } else {
            Constants.setAutoOrientationEnabled(context2, false);
            stringArrayList.get(6).setActiveornot(true);
            stringArrayList.get(6).setThumb(R.drawable.ic_qs_portrait);
        }
        if (longItemsAdapter1 != null) {
            longItemsAdapter1.notifyDataSetChanged();
        }
        if (longItemsAdapter2 != null) {
            longItemsAdapter2.notifyDataSetChanged();
        }
    }

    public void isWifiOn(Intent intent, ImageView imageView, ArrayList<LongItems> stringArrayList) {
        imageView.setImageResource(R.drawable.ic_qs_wifi_4);
        stringArrayList.get(0).setActiveornot(((NetworkInfo) intent.getParcelableExtra("networkInfo")).isConnected());
        setImageViewState(imageView, ((NetworkInfo) intent.getParcelableExtra("networkInfo")).isConnected());
    }

    public void mobilecheack(Context context2, ImageView imageView, ArrayList<LongItems> stringArrayList) {
        stringArrayList.get(1).setActiveornot(isMobileDataEnable(context2));
        setImageViewState(imageView, isMobileDataEnable(context2));
    }

    public boolean isMobileDataEnable(Context context2) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context2.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Method declaredMethod = Class.forName(connectivityManager.getClass().getName()).getDeclaredMethod("getMobileDataEnabled", new Class[0]);
            declaredMethod.setAccessible(true);
            return (Boolean) declaredMethod.invoke(connectivityManager, new Object[0]);
        } catch (Exception unused) {
            return false;
        }
    }

    public void isBluetoothOn(ImageView imageView, ArrayList<LongItems> stringArrayList) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null) {
            stringArrayList.get(4).setActiveornot(BluetoothAdapter.getDefaultAdapter().isEnabled());
            setImageViewState(imageView, BluetoothAdapter.getDefaultAdapter().isEnabled());
        } else {
            stringArrayList.get(4).setActiveornot(false);
            setImageViewState(imageView, false);
        }
    }

    public void changeSoundMode(Context context2, ImageView imageView, ArrayList<LongItems> stringArrayList, boolean changeon) {
        AudioManager audioManager = (AudioManager) context2.getSystemService(Context.AUDIO_SERVICE);
        int ringerMode = audioManager != null ? audioManager.getRingerMode() : 0;
        Vibrator vibrator = (Vibrator) context2.getSystemService(Context.VIBRATOR_SERVICE);
        boolean hasVibrator = vibrator != null && vibrator.hasVibrator();
        if (audioManager == null) {
            return;
        }
        if (hasVibrator) {
            if (changeon) {
                if (ringerMode == 2) {
                    audioManager.setRingerMode(0);
                    imageView.setImageResource(R.drawable.ic_volume_ringer_mute);
                    stringArrayList.get(3).setActiveornot(false);
                    stringArrayList.get(3).setThumb(R.drawable.ic_volume_ringer_mute);
                    stringArrayList.get(3).setName("Mute");
                    setImageViewState(imageView, false);
                } else if (ringerMode == 0) {
                    audioManager.setRingerMode(1);
                    imageView.setImageResource(R.drawable.ic_volume_ringer_vibrate);
                    stringArrayList.get(3).setActiveornot(false);
                    stringArrayList.get(3).setThumb(R.drawable.ic_volume_ringer_vibrate);
                    stringArrayList.get(3).setName("Vibrate");
                    setImageViewState(imageView, false);
                } else {
                    audioManager.setRingerMode(2);
                    imageView.setImageResource(R.drawable.ic_qs_volume_high);
                    stringArrayList.get(3).setActiveornot(true);
                    stringArrayList.get(3).setThumb(R.drawable.ic_qs_volume_high);
                    stringArrayList.get(3).setName("Sound");
                    setImageViewState(imageView, true);
                }
            } else {
                if (ringerMode == 2) {
                    imageView.setImageResource(R.drawable.ic_qs_volume_high);
                    stringArrayList.get(3).setActiveornot(true);
                    stringArrayList.get(3).setThumb(R.drawable.ic_qs_volume_high);
                    setImageViewState(imageView, true);
                } else if (ringerMode == 0) {
                    imageView.setImageResource(R.drawable.ic_volume_ringer_mute);
                    stringArrayList.get(3).setActiveornot(false);
                    stringArrayList.get(3).setThumb(R.drawable.ic_volume_ringer_mute);
                    setImageViewState(imageView, false);
                } else {
                    imageView.setImageResource(R.drawable.ic_volume_ringer_vibrate);
                    stringArrayList.get(3).setActiveornot(false);
                    stringArrayList.get(3).setThumb(R.drawable.ic_volume_ringer_vibrate);
                    setImageViewState(imageView, false);
                }
            }
        }
    }

    public void setImageViewState(ImageView imageView, boolean z) {
        if (z) {
            try {
                imageView.getDrawable().setTint(context.getResources().getColor(R.color.on_button));
                imageView.setBackgroundDrawable(context.getResources().getDrawable(LauncherApp.get_IconShapethumb()));
                imageView.getBackground().setTint(Utils.getAlphaColor(tile_color, LauncherApp.get_ActiveTilealpha()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            imageView.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_TxtColor(), LauncherApp.get_TxtColoralpha()));
            imageView.setBackgroundDrawable(context.getResources().getDrawable(LauncherApp.get_IconShapethumb()));
            imageView.getBackground().setTint(Color.parseColor("#48FFFFFF"));
        }
        if (longItemsAdapter1 != null) {
            longItemsAdapter1.notifyDataSetChanged();
        }
        if (longItemsAdapter2 != null) {
            longItemsAdapter2.notifyDataSetChanged();
        }
    }

    public static int getAlphaColor(int barColor, int AlphaVal) {
        return Color.argb(AlphaVal, Color.red(barColor), Color.green(barColor), Color.blue(barColor));
    }
}