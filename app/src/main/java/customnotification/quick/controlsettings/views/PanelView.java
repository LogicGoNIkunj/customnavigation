package customnotification.quick.controlsettings.views;

import static android.content.Context.POWER_SERVICE;

import android.animation.ObjectAnimator;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import customnotification.quick.controlsettings.LauncherApp;
import customnotification.quick.controlsettings.R;
import customnotification.quick.controlsettings.adaptar.CustomNotificationAdapter;
import customnotification.quick.controlsettings.largetile.LargePagerAdapter;
import customnotification.quick.controlsettings.largetile.LongItems;
import customnotification.quick.controlsettings.largetile.LongItemsAdapter;
import customnotification.quick.controlsettings.largetile.ScrollDisabledRecyclerView;
import customnotification.quick.controlsettings.services.ClicksInter;
import customnotification.quick.controlsettings.services.MAccessibilityService;
import customnotification.quick.controlsettings.services.OnPanelItemClickListner;
import customnotification.quick.controlsettings.utils.Constants;
import customnotification.quick.controlsettings.utils.ItemOffsetDecoration2;
import customnotification.quick.controlsettings.utils.Notification;
import customnotification.quick.controlsettings.utils.RecyclerItemTouchHelper;
import customnotification.quick.controlsettings.utils.Utils;
import customnotification.quick.controlsettings.activites.W_WallpaperActivity;

public class PanelView implements SurfaceHolder.Callback, ClicksInter {
    final Context context;
    final Animation fadeinAndScaleUp, fadeinAnim;
    boolean isSingleClick;
    boolean isFlashOn = false;
    CameraManager mCameraManager;
    String mCameraId;
    SurfaceHolder surfaceHolder;
    Camera.Parameters params;
    Camera cam;
    CameraManager.TorchCallback torchCallback;
    CustomNotificationAdapter notificationAdapter;
    public final ArrayList<Notification> notificationList = new ArrayList<>();
    public static OnPanelItemClickListner onPanelItemClickListner;
    float preY;
    final View statusBarView;
    SurfaceView surfaceView;
    TextClock time_control_center_tv;
    TextView txt_battryper;
    ImageView img_charge;
    public final Utils utils;
    Intent wifiIntent;
    int deviceStatus;
    int level;
    ImageView img_small_data, img_small_touch, img_small_volume, img_small_bluetooth, img_small_brightness, btn_powermenu;
    SeekBar seek_small_bright;
    LinearLayout llbrightness, header_item4, header_item5, header_item6;
    final ArrayList<LongItems> stringArrayList = new ArrayList<>();
    List<LongItems> sub1list = new ArrayList<>();
    List<LongItems> sub2list = new ArrayList<>();
    ScrollDisabledRecyclerView rv_page1, rv_page2;
    public static LongItemsAdapter longItemsAdapter1, longItemsAdapter2;
    ViewPager viewPager;
    int Horivalval;
    CardView topView, clearll;
    ImageView icon_brightness, img_small_wifi;
    final BroadcastReceiver mInfoReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().matches("android.net.wifi.STATE_CHANGE")) {
                utils.isWifiOn(intent, img_small_wifi, stringArrayList);
                wifiIntent = intent;
            }
            if (intent.getAction().matches("android.intent.action.AIRPLANE_MODE")) {
                utils.isAirplaneModeOn(context, stringArrayList);
            }
            if (intent.getAction().matches("android.bluetooth.adapter.action.STATE_CHANGED")) {
                utils.isBluetoothOn(img_small_bluetooth, stringArrayList);
            }
            if (intent.getAction().matches("android.intent.action.CONFIGURATION_CHANGED") && intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                utils.gpsstate(context, stringArrayList);
            }
            String action = intent.getAction();
            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            if (action.equals("android.intent.action.USER_PRESENT") || action.equals("android.intent.action.SCREEN_OFF") || action.equals("android.intent.action.SCREEN_ON")) {
                if (keyguardManager.inKeyguardRestrictedInputMode()) {
                    System.out.println("Screen off LOCKED");
                } else {
                    System.out.println("Screen off UNLOCKED");
                }
            }
            if (action.equals(Utils.FROM_NOTIFICATION_SERVICE)) {
                updateNotificationList(intent);
            }
        }
    };
    final BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            deviceStatus = intent.getIntExtra("plugged", 0);
            level = intent.getIntExtra("level", 0);
            if (deviceStatus == 1) {
                img_charge.setImageResource(R.drawable.ic_charge_running);
            } else {
                img_charge.setImageResource(R.drawable.battery);
            }
            txt_battryper.setText("".concat(String.valueOf(level)).concat("%"));
        }

    };

    public void surfaceDestroyed(SurfaceHolder surfaceHolder2) {
    }

    public PanelView(View view, Context context2, OnPanelItemClickListner onPanelItemClickListner2) {
        statusBarView = view;
        context = context2;
        utils = new Utils(context2);
        onPanelItemClickListner = onPanelItemClickListner2;
        fadeinAnim = AnimationUtils.loadAnimation(context2, R.anim.fade_in);
        fadeinAndScaleUp = AnimationUtils.loadAnimation(context2, R.anim.fade_in_scale_up);
        if (hasCameraFlash(context2)) {
            mCameraManager = (CameraManager) context2.getSystemService(Context.CAMERA_SERVICE);
            try {
                mCameraId = mCameraManager.getCameraIdList()[0];
            } catch (Exception e) {
                e.printStackTrace();
            }
            torchCallback = new CameraManager.TorchCallback() {
                public void onTorchModeChanged(String str, boolean z) {
                    super.onTorchModeChanged(str, z);
                    isFlashOn = z;
                }
            };
            mCameraManager.registerTorchCallback(torchCallback, null);
        }
    }

    private void initializeGesture(View view) {
        view.setOnTouchListener((view1, motionEvent) -> {
            int action = motionEvent.getAction();
            if (action == 0) {
                isSingleClick = true;
                new CountDownTimer(200, 1000) {
                    public void onTick(long j) {
                    }

                    public void onFinish() {
                        isSingleClick = false;
                    }
                }.start();
                preY = motionEvent.getY();
            } else if (action == 1) {
                float y = motionEvent.getY() - preY;
                if (Math.abs(y) > 100.0f) {
                    onPanelItemClickListner.onItemClicked(y > 0.0f, false);
                } else if (isSingleClick) {
                    view1.performClick();
                }
            }
            return true;
        });
    }

    public void initView() {
        setTouchListeners();
        initNotifications();
        InItbigTiles();
        llbrightness = statusBarView.findViewById(R.id.llbrightness);
        header_item4 = statusBarView.findViewById(R.id.header_item4);
        header_item5 = statusBarView.findViewById(R.id.header_item5);
        header_item6 = statusBarView.findViewById(R.id.header_item6);
        seek_small_bright = statusBarView.findViewById(R.id.seek_small_bright);
        time_control_center_tv = statusBarView.findViewById(R.id.time_control_center_tv);
        Typeface createFromAsset = Typeface.createFromAsset(context.getAssets(), "roboto_light.ttf");
        time_control_center_tv.setTypeface(createFromAsset);
        ((TextClock) statusBarView.findViewById(R.id.tv_control_center_date)).setTypeface(createFromAsset);
        txt_battryper = statusBarView.findViewById(R.id.txt_battryper);
        img_charge = statusBarView.findViewById(R.id.img_charge);
        img_small_volume = statusBarView.findViewById(R.id.img_small_volume);
        img_small_wifi = statusBarView.findViewById(R.id.img_small_wifi);
        img_small_data = statusBarView.findViewById(R.id.img_small_data);
        img_small_touch = statusBarView.findViewById(R.id.img_small_touch);
        img_small_bluetooth = statusBarView.findViewById(R.id.img_small_bluetooth);
        img_small_brightness = statusBarView.findViewById(R.id.img_small_brightness);
        btn_powermenu = statusBarView.findViewById(R.id.btn_powermenu);
        topView = statusBarView.findViewById(R.id.topView);
        clearll = statusBarView.findViewById(R.id.clearll);
        SetGridTiles();
        initCustTopView();
        SetSmallCorner();
        SetHeaderTiles();
        ClockSize();
        TimeSecondChange();
        initCustTopBrightness();
        ShowPowerbutton();
        Clicks();
        SetColorFilter();
        utils.setImageViewState(img_small_touch, false);
        surfaceView = statusBarView.findViewById(R.id.surfaceView);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.RSSI_CHANGED");
        intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
        intentFilter.addAction("android.net.wifi.STATE_CHANGE");
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        intentFilter.addAction("android.media.RINGER_MODE_CHANGED");
        intentFilter.addAction("android.location.PROVIDERS_CHANGED");
        intentFilter.addAction("android.net.wifi.WIFI_AP_STATE_CHANGED");
        intentFilter.addAction(Utils.FROM_NOTIFICATION_SERVICE);
        intentFilter.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        context.registerReceiver(mInfoReceiver, intentFilter);
        context.registerReceiver(this.mBatInfoReceiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        updateButtonStates();
    }

    public void updateButtonStates() {
        UpdateOriantation(context.getResources().getConfiguration().orientation);
        statusBarView.findViewById(R.id.error_text).setVisibility(View.GONE);
        if (utils.getBrightMode(context) == 1) {
            stringArrayList.get(5).setActiveornot(true);
            utils.setImageViewState(img_small_brightness, true);
        } else {
            stringArrayList.get(5).setActiveornot(false);
            utils.setImageViewState(img_small_brightness, false);
        }
        stringArrayList.get(12).setActiveornot(utils.isHotspotOn(context));
        Intent intent = wifiIntent;
        if (intent != null) {
            utils.isWifiOn(intent, img_small_wifi, stringArrayList);
        }
        utils.isAirplaneModeOn(context, stringArrayList);
        utils.isBluetoothOn(img_small_bluetooth, stringArrayList);
        utils.mobilecheack(context, img_small_data, stringArrayList);
        utils.gpsstate(context, stringArrayList);
        SoundManage(false);
        isFlashOn();
        ModeDayNight();
        if (Constants.checkSystemWritePermission(context)) {

            try {
                setBrightness(Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS));
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (!Constants.checkSystemWritePermission(context)) {
            return;
        }
        if (Constants.isRotationOn(context)) {
            stringArrayList.get(6).setActiveornot(false);
            stringArrayList.get(6).setThumb(R.drawable.ic_qs_auto_rotate);
        } else {
            stringArrayList.get(6).setActiveornot(true);
            stringArrayList.get(6).setThumb(R.drawable.ic_qs_portrait);
        }
        stringArrayList.get(10).setActiveornot(battrySaverONOff());
        stringArrayList.get(14).setActiveornot(DNDTruefalse());
    }

    public void UpdateOriantation(int orientation) {
        if (orientation == 1) {
            statusBarView.findViewById(R.id.viewallll).setPadding(0, 30, 0, 0);
            ViewGroup.LayoutParams params = viewPager.getLayoutParams();
            params.height = (int) (context.getResources().getDisplayMetrics().heightPixels / 2.3);
            viewPager.setLayoutParams(params);
            Horivalval = Integer.parseInt(StringUtils.substringBefore(LauncherApp.get_GridTiles().trim(), "*").trim());
            if (Horivalval == 3) {
                sub1list = stringArrayList.subList(0, 9);
                sub2list = stringArrayList.subList(9, stringArrayList.size());
            } else if (Horivalval == 4) {
                sub1list = stringArrayList.subList(0, 12);
                sub2list = stringArrayList.subList(12, stringArrayList.size());
            } else if (Horivalval == 5) {
                sub1list = stringArrayList.subList(0, 15);
                sub2list = stringArrayList.subList(15, stringArrayList.size());
            }
            rv_page1.setLayoutManager(new GridLayoutManager(context, Horivalval));
            rv_page2.setLayoutManager(new GridLayoutManager(context, Horivalval));
            longItemsAdapter1 = new LongItemsAdapter(context, sub1list, this);
            longItemsAdapter2 = new LongItemsAdapter(context, sub2list, this);
            rv_page1.setAdapter(longItemsAdapter1);
        } else {
            statusBarView.findViewById(R.id.viewallll).setPadding(0, 0, 0, 0);


            ViewGroup.LayoutParams params = viewPager.getLayoutParams();
            params.height = (int) (context.getResources().getDisplayMetrics().heightPixels / 2);
            viewPager.setLayoutParams(params);

            sub1list = stringArrayList.subList(0, 8);
            sub2list = stringArrayList.subList(8, stringArrayList.size());
            rv_page1.setLayoutManager(new GridLayoutManager(context, 4));
            rv_page2.setLayoutManager(new GridLayoutManager(context, 4));
            longItemsAdapter1 = new LongItemsAdapter(context, sub1list, this);
            longItemsAdapter2 = new LongItemsAdapter(context, sub2list, this);
            rv_page1.setAdapter(longItemsAdapter1);
        }
    }

    private void initNotifications() {
        if (notificationList.size() != 0) {
            statusBarView.findViewById(R.id.txtclear).setVisibility(View.VISIBLE);
        } else {
            statusBarView.findViewById(R.id.txtclear).setVisibility(View.GONE);
        }
        RecyclerView recyclerView = statusBarView.findViewById(R.id.rv_notification_list);
        notificationAdapter = new CustomNotificationAdapter(context, notificationList, notification -> onPanelItemClickListner.onItemClicked(false, false));
        recyclerView.setAdapter(notificationAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new ItemOffsetDecoration2(context, R.dimen.small_margin));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        new ItemTouchHelper(new RecyclerItemTouchHelper(0, 12, (viewHolder, i, i2) -> {
            if (i2 >= 0 && i2 < notificationList.size()) {
                notificationList.remove(i2);
                notificationAdapter.notifyItemRemoved(i2);
                recyclerView.requestLayout();
                if (notificationList.size() != 0) {
                    statusBarView.findViewById(R.id.txtclear).setVisibility(View.VISIBLE);
                } else {
                    statusBarView.findViewById(R.id.txtclear).setVisibility(View.GONE);
                }
            }
        })).attachToRecyclerView(recyclerView);
        statusBarView.findViewById(R.id.txtclear).setOnClickListener(view -> {
            notificationList.clear();
            notificationAdapter.notifyDataSetChanged();
            recyclerView.requestLayout();
            statusBarView.findViewById(R.id.txtclear).setVisibility(View.GONE);
        });
    }

    private Bitmap getBitmapFromByteArray(byte[] bArr) {
        if (bArr != null) {
            return BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
        }
        return null;
    }

    private void updateNotificationList(Intent intent) {
        String stringExtra = intent.getStringExtra("id");
        String stringExtra2 = intent.getStringExtra("package");
        String stringExtra3 = intent.getStringExtra("title");
        String stringExtra4 = intent.getStringExtra("text");
        boolean booleanExtra = intent.getBooleanExtra("isAdded", true);
        long longExtra = intent.getLongExtra("postTime", Calendar.getInstance().getTime().getTime());
        Bitmap bitmapFromByteArray = getBitmapFromByteArray(intent.getByteArrayExtra("icon"));
        Bitmap bitmapFromByteArray2 = getBitmapFromByteArray(intent.getByteArrayExtra("largeIcon"));
        PendingIntent pendingIntent = intent.getParcelableExtra("pendingIntent");
        if (!stringExtra.equalsIgnoreCase("0") || !stringExtra2.equalsIgnoreCase("com.google.android.gm")) {
            int i = 0;
            if (booleanExtra) {
                Notification notification = new Notification(stringExtra, bitmapFromByteArray, bitmapFromByteArray2, stringExtra3, stringExtra4, 1, stringExtra2, longExtra, pendingIntent);
                int i2 = -1;
                while (i < notificationList.size()) {
                    if (notificationList.get(i).id.equals(stringExtra)) {
                        i2 = i;
                    }
                    i++;
                }
                if (i2 != -1) {
                    if (notification.senderIcon != null) {
                        notificationList.get(i2).senderIcon = notification.senderIcon;
                    }
                    if (notification.icon != null) {
                        notificationList.get(i2).icon = notification.icon;
                    }
                    notificationList.get(i2).tv_title = notification.tv_title;
                    notificationList.get(i2).tv_text = notification.tv_text;
                    notificationList.get(i2).pack = notification.pack;
                    notificationList.get(i2).postTime = notification.postTime;
                    notificationList.get(i2).count = notification.count;
                    if (notification.id != null) {
                        notificationList.get(i2).id = notification.id;
                    }
                    if (notification.pendingIntent != null) {
                        notificationList.get(i2).pendingIntent = notification.pendingIntent;
                    }
                } else {
                    notificationList.add(notification);
                    TextView txtclear = statusBarView.findViewById(R.id.txtclear);
                    if (notificationList.size() != 0) {
                        txtclear.setVisibility(View.VISIBLE);
                    } else {
                        txtclear.setVisibility(View.GONE);
                    }
                }
            } else {
                while (true) {
                    if (i >= notificationList.size()) {
                        break;
                    } else if (notificationList.get(i).id.equals(stringExtra)) {
                        notificationList.remove(i);
                        break;
                    } else {
                        i++;
                    }
                }
            }
            notificationAdapter.notifyDataSetChanged();
        }
    }

    public void doCleanUp() {
        Camera camera = cam;
        if (camera != null) {
            camera.stopPreview();
            cam.setPreviewCallback(null);
            cam.release();
            cam = null;
        }
        try {
            context.unregisterReceiver(mInfoReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder2) {
        surfaceHolder = surfaceHolder2;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder2, int i, int i2, int i3) {
        releaseCamera();
    }

    private void acquireCamera(SurfaceHolder surfaceHolder2) {
        if (cam == null) {
            try {
                Camera open = Camera.open();
                cam = open;
                params = open.getParameters();
                cam.setPreviewDisplay(surfaceHolder2);
            } catch (Exception unused) {
                Camera camera = cam;
                if (camera == null) {
                    Toast.makeText(context, "Phone restart required for this feature.!", Toast.LENGTH_SHORT).show();
                    return;
                }
                camera.release();
                cam = null;
            }
        }
    }

    private void releaseCamera() {
        Camera camera = cam;
        if (camera != null) {
            camera.stopPreview();
            cam.setPreviewCallback(null);
            cam.release();
            cam = null;
        }
    }

    public void turnFlashOnOff() {
        try {
            if (!isFlashOn) {
                mCameraManager.setTorchMode(mCameraId, true);
                isFlashOn = true;
            } else {
                mCameraManager.setTorchMode(mCameraId, false);
                isFlashOn = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        isFlashOn();
    }

    private void isFlashOn() {
        if (isFlashOn) {
            img_small_touch.setImageResource(R.drawable.img_flashon);
            stringArrayList.get(2).setActiveornot(isFlashOn);
            stringArrayList.get(2).setThumb(R.drawable.img_flashon);
            utils.setImageViewState(img_small_touch, true);
        } else {
            img_small_touch.setImageResource(R.drawable.img_flashoff);
            stringArrayList.get(2).setActiveornot(isFlashOn);
            stringArrayList.get(2).setThumb(R.drawable.img_flashoff);
            utils.setImageViewState(img_small_touch, false);
        }
    }

    public void SoundManage(boolean sondedit) {
        try {
            utils.changeSoundMode(context, img_small_volume, stringArrayList, sondedit);
        } catch (Exception unused) {
            statusBarView.findViewById(R.id.error_text).setVisibility(View.VISIBLE);
        }
    }

    public void BluetoothManage() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter != null) {
            if (defaultAdapter.isEnabled()) {
                defaultAdapter.disable();
            } else {
                defaultAdapter.enable();
            }
        }
    }


    public void BrightnessManager() {
        try {
            if (utils.getBrightMode(context) == 1) {
                stringArrayList.get(5).setActiveornot(false);
                utils.setImageViewState(img_small_brightness, false);
                Settings.System.putInt(context.getContentResolver(), "screen_brightness_mode", 0);
                return;
            }
            Settings.System.putInt(context.getContentResolver(), "screen_brightness_mode", 1);
            stringArrayList.get(5).setActiveornot(true);
            utils.setImageViewState(img_small_brightness, true);
            new Handler().postDelayed(() -> {
                try {
                    ObjectAnimator animation = ObjectAnimator.ofInt(((SeekBar) statusBarView.findViewById(R.id.seek_small_bright)), "progress", Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS));
                    animation.setDuration(500); // 0.5 second
                    animation.setInterpolator(new DecelerateInterpolator());
                    animation.start();
                } catch (Settings.SettingNotFoundException e) {
                    e.printStackTrace();
                }
            }, 50);
        } catch (Exception unused) {
            statusBarView.findViewById(R.id.error_text).setVisibility(View.VISIBLE);
        }
    }


    public void ModeDayNight() {
        switch (context.getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) {
            case Configuration.UI_MODE_NIGHT_YES:
                stringArrayList.get(13).setActiveornot(true);
                longItemsAdapter2.notifyDataSetChanged();
                break;
            case Configuration.UI_MODE_NIGHT_NO:
                stringArrayList.get(13).setActiveornot(false);
                longItemsAdapter2.notifyDataSetChanged();
                break;
        }
    }

    public static boolean hasCameraFlash(Context context2) {
        return context2.getPackageManager().hasSystemFeature("android.hardware.camera.flash");
    }

    private Camera.Size getSmallestPreviewSize(Camera.Parameters parameters) {
        List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        if (supportedPreviewSizes.size() > 0) {
            return supportedPreviewSizes.get(supportedPreviewSizes.size() - 1);
        }
        return null;
    }

    private Camera.Size getSmallestPictureSize(Camera.Parameters parameters) {
        Camera.Size size = null;
        for (Camera.Size size2 : parameters.getSupportedPictureSizes()) {
            if (size == null || size2.width * size2.height < size.width * size.height) {
                size = size2;
            }
        }
        return size;
    }

    private void setTouchListeners() {
        initializeGesture(statusBarView.findViewById(R.id.statusbar_parent));
    }

    public void startPanelUpperAnimations(View view) {
        statusBarView.startAnimation(fadeinAnim);
    }


    public void setBrightness(int brightness) {
        seek_small_bright.setProgress(brightness);
    }

    public void Clicks() {
        if (Constants.checkSystemWritePermission(context)) {
            utils.getBrightness(seek_small_bright, context);
        }
        float curBrightnessValue = 0;
        try {
            curBrightnessValue = android.provider.Settings.System.getInt(context.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        seek_small_bright.setProgress((int) curBrightnessValue);
        seek_small_bright.setMax(getMaxBrightness(context, 255));

        seek_small_bright.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                try {
                    if (Constants.checkSystemWritePermission(context)) {
                        utils.setBrightness(i, context);
                    } else {
                        statusBarView.findViewById(R.id.error_text).setVisibility(View.VISIBLE);
                    }
                } catch (Exception ignored) {
                }
            }
        });

        img_small_wifi.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT < 29) {
                WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                wifiManager.setWifiEnabled(!wifiManager.isWifiEnabled());
            } else {
                Intent intent = new Intent("android.settings.panel.action.WIFI");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                onPanelItemClickListner.onItemClicked(false, true);
            }
        });
        img_small_data.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT < 29) {
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
                context.startActivity(intent);
                onPanelItemClickListner.onItemClicked(false, true);
                context.startActivity(new Intent("android.settings.DATA_ROAMING_SETTINGS"));
                onPanelItemClickListner.onItemClicked(false, true);
                return;
            }
            Intent intent2 = new Intent("android.settings.panel.action.INTERNET_CONNECTIVITY");
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent2);
            onPanelItemClickListner.onItemClicked(false, true);
        });
        img_small_touch.setOnClickListener(view -> turnFlashOnOff());
        img_small_volume.setOnClickListener(view -> SoundManage(true));
        img_small_bluetooth.setOnClickListener(view -> BluetoothManage());
        img_small_brightness.setOnClickListener(view -> BrightnessManager());
        statusBarView.findViewById(R.id.settings_iv).setOnClickListener(view -> {
            try {
                Intent intent = new Intent("android.settings.SETTINGS");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
            onPanelItemClickListner.onItemClicked(false, true);
        });
        statusBarView.findViewById(R.id.background_iv).setOnClickListener(view -> {
            try {
                Intent intent = new Intent(context, W_WallpaperActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
            onPanelItemClickListner.onItemClicked(false, true);
        });
        btn_powermenu.setOnClickListener(view -> {
            if (!isAccessibilitySettingsOn(context)) {
                context.startActivity(new Intent("android.settings.ACCESSIBILITY_SETTINGS"));
            } else {
                poweronOff();
            }
            onPanelItemClickListner.onItemClicked(false, true);
        });
    }

    public void poweronOff() {
        context.getPackageManager().setComponentEnabledSetting(new ComponentName(context, MAccessibilityService.class), PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
        if (!isAccessibilitySettingsOn(context)) {
            context.startActivity(new Intent("android.settings.ACCESSIBILITY_SETTINGS"));
            return;
        }
        Intent intent = new Intent("customnotification.quick.controlsettings.ACCESSIBILITY_ACTION");
        intent.putExtra("action", 6);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    boolean isAccessibilitySettingsOn(Context context) {
        int i;
        String string;
        String str = context.getPackageName() + "/" + MAccessibilityService.class.getCanonicalName();
        try {
            i = Settings.Secure.getInt(context.getApplicationContext().getContentResolver(), "accessibility_enabled");
        } catch (Settings.SettingNotFoundException unused) {
            i = 0;
        }
        TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(':');
        if (i == 1 && (string = Settings.Secure.getString(context.getApplicationContext().getContentResolver(), "enabled_accessibility_services")) != null) {
            simpleStringSplitter.setString(string);
            while (simpleStringSplitter.hasNext()) {
                if (simpleStringSplitter.next().equalsIgnoreCase(str)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void TimeSecondChange() {
        if (LauncherApp.get_ClockSecondshow()) {
            time_control_center_tv.setFormat24Hour(context.getResources().getString(R.string.time_format_withsec));
            time_control_center_tv.setFormat12Hour(context.getResources().getString(R.string.time_format_withsec));
        } else {
            time_control_center_tv.setFormat24Hour(context.getResources().getString(R.string.time_format));
            time_control_center_tv.setFormat12Hour(context.getResources().getString(R.string.time_format));
        }
    }

    public void initCustTopView() {
        isFlashOn();
        SoundManage(false);
        ModeDayNight();
        if (Constants.checkSystemWritePermission(context)) {
            try {
                setBrightness(Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS));
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
        }
        img_small_wifi.setBackground(context.getDrawable(LauncherApp.get_IconShapethumb()));
        img_small_data.setBackground(context.getDrawable(LauncherApp.get_IconShapethumb()));
        img_small_touch.setBackground(context.getDrawable(LauncherApp.get_IconShapethumb()));
        img_small_volume.setBackground(context.getDrawable(LauncherApp.get_IconShapethumb()));
        img_small_bluetooth.setBackground(context.getDrawable(LauncherApp.get_IconShapethumb()));
        img_small_brightness.setBackground(context.getDrawable(LauncherApp.get_IconShapethumb()));
        if (longItemsAdapter1 != null) {
            longItemsAdapter1.notifyDataSetChanged();
        }
        if (longItemsAdapter2 != null) {
            longItemsAdapter2.notifyDataSetChanged();
        }
    }

    public void SetColorFilter() {
        topView.setBackgroundTintList(ColorStateList.valueOf(LauncherApp.get_MainBgColor()).withAlpha(LauncherApp.get_MainBgalpha()));
        icon_brightness = statusBarView.findViewById(R.id.icon_brightness);
        icon_brightness.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_brightnesscolor(), LauncherApp.get_brightcoloralpha()));
        seek_small_bright.setProgressTintList(ColorStateList.valueOf(LauncherApp.get_brightnesscolor()));
        int SeekColor = Utils.getAlphaColor(LauncherApp.get_brightnesscolor(), LauncherApp.get_brightcoloralpha());
        seek_small_bright.setThumbTintList(ColorStateList.valueOf(SeekColor));
        int color = Utils.getAlphaColor(LauncherApp.get_TxtColor(), LauncherApp.get_TxtColoralpha());
        time_control_center_tv.setTextColor(ColorStateList.valueOf(color));
        txt_battryper.setTextColor(ColorStateList.valueOf(color));
        ((TextClock) statusBarView.findViewById(R.id.tv_control_center_date)).setTextColor(ColorStateList.valueOf(color));
        ((ImageView) statusBarView.findViewById(R.id.background_iv)).getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_TxtColor(), LauncherApp.get_TxtColoralpha()));
        ((ImageView) statusBarView.findViewById(R.id.settings_iv)).getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_TxtColor(), LauncherApp.get_TxtColoralpha()));
        btn_powermenu.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_TxtColor(), LauncherApp.get_TxtColoralpha()));
        img_charge.getDrawable().setTint(Utils.getAlphaColor(LauncherApp.get_TxtColor(), LauncherApp.get_TxtColoralpha()));
        img_small_wifi.getDrawable().setTint(color);
        img_small_data.getDrawable().setTint(color);
        img_small_touch.getDrawable().setTint(color);
        img_small_volume.getDrawable().setTint(color);
        img_small_bluetooth.getDrawable().setTint(color);
        img_small_brightness.getDrawable().setTint(color);
        ((WormDotsIndicator) statusBarView.findViewById(R.id.dots_indicator)).setDotsColor(color);
        ((WormDotsIndicator) statusBarView.findViewById(R.id.dots_indicator)).setDotIndicatorColor(color);
        ((WormDotsIndicator) statusBarView.findViewById(R.id.dots_indicator)).setStrokeDotsIndicatorColor(LauncherApp.get_brightnesscolor());
        statusBarView.findViewById(R.id.img_btm).getBackground().setTint(color);
        if (longItemsAdapter1 != null) {
            longItemsAdapter1.notifyDataSetChanged();
        }
        if (longItemsAdapter2 != null) {
            longItemsAdapter2.notifyDataSetChanged();
        }
    }

    public void SetSmallCorner() {
        if (LauncherApp.get_SmallCorner()) {
            topView.setRadius(30);
            clearll.setRadius(30);
        } else {
            topView.setRadius(60);
            clearll.setRadius(60);
        }
    }

    public void initCustTopBrightness() {
        if (LauncherApp.get_brigtnesshader()) {
            llbrightness.setVisibility(View.VISIBLE);
        } else {
            llbrightness.setVisibility(View.GONE);
        }
    }

    public void ShowPowerbutton() {
        if (LauncherApp.get_powerbtn()) {
            btn_powermenu.setVisibility(View.VISIBLE);
        } else {
            btn_powermenu.setVisibility(View.GONE);
        }
    }

    public void SetHeaderTiles() {
        if (LauncherApp.get_headertiles() == 3) {
            header_item4.setVisibility(View.GONE);
            header_item5.setVisibility(View.GONE);
            header_item6.setVisibility(View.GONE);
        } else if (LauncherApp.get_headertiles() == 4) {
            header_item4.setVisibility(View.VISIBLE);
            header_item5.setVisibility(View.GONE);
            header_item6.setVisibility(View.GONE);
        } else if (LauncherApp.get_headertiles() == 5) {
            header_item4.setVisibility(View.VISIBLE);
            header_item5.setVisibility(View.VISIBLE);
            header_item6.setVisibility(View.GONE);
        } else if (LauncherApp.get_headertiles() == 6) {
            header_item4.setVisibility(View.VISIBLE);
            header_item5.setVisibility(View.VISIBLE);
            header_item6.setVisibility(View.VISIBLE);
        }
    }

    public void ClockSize() {
        int screenori = context.getResources().getConfiguration().orientation;
        if (screenori == 1) {
            ((TextClock) statusBarView.findViewById(R.id.tv_control_center_date)).setTextSize(LauncherApp.get_Clocksize() / 3);
            time_control_center_tv.setTextSize(LauncherApp.get_Clocksize() / 4);
        }
    }

    public void InItbigTiles() {
        viewPager = statusBarView.findViewById(R.id.viewpager);
        rv_page1 = statusBarView.findViewById(R.id.rv_page1);
        rv_page2 = statusBarView.findViewById(R.id.rv_page2);
        stringArrayList.add(new LongItems(R.drawable.ic_qs_wifi_4, "Wi-fi", false));
        stringArrayList.add(new LongItems(R.drawable.ic_swap_vert, "Mobile Data", false));
        stringArrayList.add(new LongItems(R.drawable.img_flashoff, "Flashlight", false));
        stringArrayList.add(new LongItems(R.drawable.ic_qs_volume_high, "Sound", true));
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null) {
            stringArrayList.add(new LongItems(R.drawable.ic_qs_bluetooth_on, "Bluetooth", bluetoothAdapter.isEnabled()));
        } else {
            stringArrayList.add(new LongItems(R.drawable.ic_qs_bluetooth_on, "Bluetooth", false));
        }
        stringArrayList.add(new LongItems(R.drawable.ic_brightness_auto, "Brightness", false));
        stringArrayList.add(new LongItems(R.drawable.ic_qs_auto_rotate, "Auto-rotate", false));
        stringArrayList.add(new LongItems(R.drawable.ic_signal_airplane, "Airplane mode", false));
        stringArrayList.add(new LongItems(R.drawable.ic_signal_location, "Location", false));
        stringArrayList.add(new LongItems(R.drawable.ic_type_image, "Home", false));
        stringArrayList.add(new LongItems(R.drawable.ic_qs_battery_saver, "Battery Saver", false));
        stringArrayList.add(new LongItems(R.drawable.ic_screen_lock, "Lock Screen", false));
        stringArrayList.add(new LongItems(R.drawable.ic_wifi_tethering_black_24dp, "Hotspot", false));
        stringArrayList.add(new LongItems(R.drawable.ic_ui_mode_dark, "Dark Theme", false));
        stringArrayList.add(new LongItems(R.drawable.ic_do_not_disturb_on_24dp, "Do Not Disturb", false));
        stringArrayList.add(new LongItems(R.drawable.ic_chevron_down, "Open System", false));
        new Handler().postDelayed(() -> {
            LargePagerAdapter adapter = new LargePagerAdapter();
            adapter.insertViewId(R.id.page_one);
            adapter.insertViewId(R.id.page_two);
            viewPager.setAdapter(adapter);
            ((WormDotsIndicator) statusBarView.findViewById(R.id.dots_indicator)).setViewPager(viewPager);
        }, 500);
    }

    public void SetGridTiles() {
        int screenori = context.getResources().getConfiguration().orientation;
        if (screenori == 1) {
            Horivalval = Integer.parseInt(StringUtils.substringBefore(LauncherApp.get_GridTiles().trim(), "*").trim());
            if (Horivalval == 3) {
                sub1list = stringArrayList.subList(0, 9);
                sub2list = stringArrayList.subList(9, stringArrayList.size());
            } else if (Horivalval == 4) {
                sub1list = stringArrayList.subList(0, 12);
                sub2list = stringArrayList.subList(12, stringArrayList.size());
            } else if (Horivalval == 5) {
                sub1list = stringArrayList.subList(0, 15);
                sub2list = stringArrayList.subList(15, stringArrayList.size());
            }
            rv_page1.setLayoutManager(new GridLayoutManager(context, Horivalval));
            rv_page2.setLayoutManager(new GridLayoutManager(context, Horivalval));
            longItemsAdapter1 = new LongItemsAdapter(context, sub1list, this);
            longItemsAdapter2 = new LongItemsAdapter(context, sub2list, this);
            rv_page1.setAdapter(longItemsAdapter1);
        }
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    rv_page1.setAdapter(longItemsAdapter1);
                } else if (position == 1) {
                    rv_page2.setAdapter(longItemsAdapter2);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void FlashClick() {
        turnFlashOnOff();
    }

    @Override
    public void SoundClick() {
        SoundManage(true);
    }

    @Override
    public void BluetoothClick() {
        BluetoothManage();
    }

    @Override
    public void BrightnessClick() {
        BrightnessManager();
    }

    @Override
    public void AutoRotateClick() {
        if (Constants.checkSystemWritePermission(context)) {
            utils.isAutoRotateOn(context, stringArrayList);
        } else {
            statusBarView.findViewById(R.id.error_text).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void AirPlanClick() {
        Intent intent = new Intent("android.settings.AIRPLANE_MODE_SETTINGS");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        onPanelItemClickListner.onItemClicked(false, true);
    }


    public void PowerSaverModeClick() {
        try {
            Intent batterySaverIntent = new Intent(Intent.ACTION_POWER_USAGE_SUMMARY);
            batterySaverIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(batterySaverIntent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
        onPanelItemClickListner.onItemClicked(false, true);
    }

    public boolean battrySaverONOff() {
        try {
            return Settings.Global.getInt(this.context.getContentResolver(), "low_power") == 1 || Settings.System.getInt(this.context.getContentResolver(), "POWER_SAVE_MODE_OPEN") == 1;
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void DoNotDisturbClick() {
        try {
            Intent intentaa = new Intent("android.settings.ZEN_MODE_SETTINGS");
            intentaa.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentaa);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
        onPanelItemClickListner.onItemClicked(false, true);
    }

    public boolean DNDTruefalse() {
        try {
            return Settings.Global.getInt(this.context.getContentResolver(), "zen_mode") != 0;
        } catch (Settings.SettingNotFoundException unused) {
            return false;
        }
    }

    @Override
    public void HotSpotClick() {
        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.TetherSettings"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        onPanelItemClickListner.onItemClicked(false, true);
    }

    public int getMaxBrightness(Context context, int i) {
        PowerManager powerManager = (PowerManager) context.getSystemService(POWER_SERVICE);
        if (powerManager != null) {
            Field[] declaredFields = powerManager.getClass().getDeclaredFields();
            for (Field field : declaredFields) {
                if (field.getName().equals("BRIGHTNESS_ON")) {
                    field.setAccessible(true);
                    try {
                        return (Integer) field.get(powerManager);
                    } catch (IllegalAccessException unused) {
                        return i;
                    }
                }
            }
        }
        return i;
    }
}